Admin and API install requirement
- Laravel ver.7.x
- PHP and PHP-FPM ver. >= 7.3

Project Installation
- Install docker, please visit https://docs.docker.com/docker-for-mac/install/
- Check the project path in Dockerfile, nginx.conf, start.sh and check php version to use ver.7.3
- Install composer, use command(osx) 
   - php composer-setup.php --install-dir=bin --filename=composer
   - mv composer.phar /usr/local/bin/composer
- Install Laravel framwork, use command: 
    - composer create-project laravel/laravel admin
- After install Laravel framwork use command:
    - compose install
- Then use the command for generate app key
    - php artisan key:generate

Docker command
- git clone {url}
- git pull origin {branch_name}
- git branch  <= show all branch
- git branch {branch_name} <= create new branch
- git checkout {branch_name} <= switch branch
- git add -A
- git commit -m "a message"
- git push -u origin {branch_name}