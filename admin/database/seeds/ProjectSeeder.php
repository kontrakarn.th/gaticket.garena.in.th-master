<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = ['id', 'project_name', 'server_test', 'domain_name', 'region', 'ip_server_test', 'status_domain', 'expired_ssl', 'status_ssl', 'live_date', 'end_date', 'db', 'last_ip', 'last_login', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by'];
        $projects =   [
            [
                '1',
                'tv.codm.garena.in.th',
                'test.tv.codm.garena.in.th',
                'tv.codm.garena.in.th', 
                1, 
                '112.121.158.198', 
                '1', 
                '2020-10-01 00:00:00', 
                '1', 
                '2020-07-07 00:00:00', 
                '2020-07-07 00:00:00', 
                'our_server', 
                null, 
                null, 
                '2020-07-07 16:25:47', 
                null, 
                '2020-07-09 16:54:11', 
                null, 
                null, 
                null
            ],
            ['2', 'cdnupload.seathailand.com','test.cdnupload.seathailand.com', 'cdnupload.seathailand.com', 2, '203.116.23.218', '1', '2020-09-16 00:00:00', '1', '2020-07-01 00:00:00', '2020-07-07 00:00:00', 'gcloud', null, null, '2020-07-07 16:27:41', null, '2020-07-08 14:58:32', null, null, null],
            ['3', 'emailsendingtools.garena.in.th','test.emailsendingtools.garena.in.th', 'emailsendingtools.garena.in.th', 1, '112.121.158.198', '1', '2020-09-08 00:00:00', '1', '2020-07-01 00:00:00', '2020-07-07 17:12:27', 'gcloud', null, null, '2020-07-07 17:12:27', null, '2020-07-08 15:24:08', null, null, null],
            ['4', 'feedingporo.lol.garena.in.th', 'test.feedingporo.lol.garena.in.th', 'feedingporo.lol.garena.in.th', 1, '112.121.158.198', '1', '2020-08-27 00:00:00', '1', '2020-06-24 00:00:00', '2020-12-31 00:00:00', 'gcloud', null, null, '2020-07-07 17:13:18', null, '2020-07-08 15:14:14', null, null, null],
            ['5', 'policy.seathailand.com', 'test.policy.seathailand.com', 'policy.seathailand.com', 2, '203.116.23.218', '1', '2020-10-07 00:00:00', '1', '2020-07-10 00:00:00', '2020-07-31 00:00:00', 'gcloud', null, null, '2020-07-10 10:29:03', null, '2020-07-10 10:29:03', null, null, null],
            ['6', 'api.policy.seathailand.com','test.api.policy.seathailand.com', 'api.policy.seathailand.com', 1, '112.121.158.198', '1', '2020-10-12 00:00:00', '1', '2020-07-17 00:00:00', '2020-07-31 00:00:00', 'gcloud', null, null, '2020-07-17 15:07:27', null, '2020-07-17 15:07:54', null, null, null],
            ['7', 'spx.policy.seathailand.com', 'test.spx.policy.seathailand.com', 'spx.policy.seathailand.com', 1, '112.121.158.198', '1', '2020-10-08 00:00:00', '1', '2020-07-17 00:00:00', '2020-07-31 00:00:00', 'gcloud', null, null, '2020-07-17 15:08:26', null, '2020-07-17 15:09:57', null, null, null],
            ['8', 'api.spx.policy.seathailand.com', 'test.api.spx.policy.seathailand.com', 'api.spx.policy.seathailand.com', 1, '112.121.158.198', '1', '2020-10-12 00:00:00', '1', '2020-07-17 00:00:00', '2020-07-31 00:00:00', 'gcloud', null, null, '2020-07-17 15:08:53', null, '2020-07-17 15:09:38', null, null, null],
        ];
        foreach ($projects as $project) {
            $new_project = new Project;
            foreach ($fields as $field) {
                $new_project[$field] = $project[array_search($field, $fields)];
            }
            $new_project->save();
        }  
    }
}
