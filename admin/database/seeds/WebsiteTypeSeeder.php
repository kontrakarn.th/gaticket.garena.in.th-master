<?php

use Illuminate\Database\Seeder;
use App\WebsiteType;

class WebsiteTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'Coporate';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'HR';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'BNS';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'AoV';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'FO4';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'FreeFire';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'CoDM';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'SpeedDrifter';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'LoL';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'HoN';
        $new_user_group->save();
        $new_user_group = new WebsiteType;
        $new_user_group['website_type'] = 'Game Support';
        $new_user_group->save();
    }
}
