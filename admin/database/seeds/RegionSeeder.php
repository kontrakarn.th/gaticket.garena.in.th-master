<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_ticket = new Region;
        $new_ticket['region'] = 'Thailand';
        $new_ticket['ip_test'] = "112.121.158.198";
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Singapore';
        $new_ticket['ip_test'] = "203.116.23.218";
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Taiwan';
        $new_ticket['ip_test'] = "124.108.163.19";
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Vietnam';
        $new_ticket['ip_test'] = "137.59.117.69";
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Indonesia';
        $new_ticket['ip_test'] = "202.73.18.14";
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Malaysia';
        $new_ticket['ip_test'] = null;
        $new_ticket->save();
        $new_ticket = new Region;
        $new_ticket['region'] = 'Philippines';
        $new_ticket['ip_test'] = "119.28.115.18";
        $new_ticket->save();
    }
}
