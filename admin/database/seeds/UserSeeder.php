<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_user = new User;
        $new_user['name'] = 'Neo Super Admin';
        $new_user["email"] = 'sirapat.a@seagroup.com';
        $new_user["role"] = 'super_admin';
        $new_user->save();
        $new_user = new User;
        $new_user['name'] = 'Pete Super Admin';
        $new_user["email"] = 'pasan.s@seagroup.com';
        $new_user["role"] = 'super_admin';
        $new_user->save();
        $new_user = new User;
        $new_user['name'] = 'Pete User';
        $new_user["email"] = 'petepasan@gmail.com';
        $new_user["role"] = 'user';
        $new_user["group"] = '1';
        $new_user->save();
        $new_user = new User;
        $new_user['name'] = 'Art Super Admin';
        $new_user["email"] = 'phongsun.w@garena.co.th';
        $new_user["role"] = 'super_admin';
        $new_user = new User;
        $new_user['name'] = 'Art User';
        $new_user["email"] = 'phongsun.art@gmail.com';
        $new_user["role"] = 'user';
        $new_user["group"] = '1';
        $new_user->save();
        $new_user = new User;
        $new_user['name'] = 'Neo Admin';
        $new_user["email"] = 'sirapat.asa@gmail.com';
        $new_user["role"] = 'admin';
        $new_user->save();
        $new_user = new User;
        $new_user['name'] = 'Neo User';
        $new_user["email"] = 'sirapatheblader@gmail.com';
        $new_user["role"] = 'user';
        $new_user["group"] = '1';
        $new_user->save();
    }
}
