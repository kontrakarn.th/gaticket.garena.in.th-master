<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            TicketSeeder::class,
            DTicketSeeder::class,
            UserGroupSeeder::class,
            RegionSeeder::class,
            WebsiteTypeSeeder::class,
            ProjectSeeder::class,
        ]);
    }
}
