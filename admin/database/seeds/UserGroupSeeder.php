<?php

use Illuminate\Database\Seeder;
use App\UserGroup;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_user_group = new Usergroup;
        $new_user_group['user_group'] = 'LOL';
        $new_user_group->save();
        $new_user_group = new Usergroup;
        $new_user_group['user_group'] = 'BNS';
        $new_user_group->save();
        $new_user_group = new Usergroup;
        $new_user_group['user_group'] = 'PUBGM';
        $new_user_group->save();
        $new_user_group = new Usergroup;
        $new_user_group['user_group'] = 'CODM';
        $new_user_group->save();
    }
}
