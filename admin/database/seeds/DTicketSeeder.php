<?php

use Illuminate\Database\Seeder;
use App\DTicket;

class DTicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_ticket = new DTicket;
        $new_ticket['requestor_id'] = "3";
        $new_ticket['domain_name'] = "domain_name1";
        $new_ticket->save();
        $new_ticket = new DTicket;
        $new_ticket['requestor_id'] = "3";
        $new_ticket['assignee_id'] = "1";
        $new_ticket['domain_name'] = "domain_name2";
        $new_ticket['status'] = "Processing";
        $new_ticket->save();
        $new_ticket = new DTicket;
        $new_ticket['requestor_id'] = "3";
        $new_ticket['assignee_id'] = "1";
        $new_ticket['domain_name'] = "domain_name3";
        $new_ticket['status'] = "Closed";
        $new_ticket->save();
    }
}
