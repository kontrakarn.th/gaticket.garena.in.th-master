<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('ticket_id');
            $table->integer('requestor_id');
            $table->integer('assignee_id')->nullable();
            $table->text('emails_for_permission')->nullable();
            $table->string('website_url');
            $table->string('website_type');
            $table->string('website_region');
            $table->text('remarks')->nullable();
            $table->string('status')->default('Unassigned');
            $table->string('gacode')->nullable();
            $table->text('gatag')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('requestor_id');
            $table->index('website_url');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
