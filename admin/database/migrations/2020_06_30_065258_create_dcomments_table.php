<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dcomments', function (Blueprint $table) {
            $table->bigIncrements('dcomment_id');
            $table->integer('user_id');
            $table->integer('dticket_id');
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
            $table->index('dticket_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dcomments');
    }
}
