<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDticketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dtickets', function (Blueprint $table) {
            $table->bigIncrements('dticket_id');
            $table->integer('requestor_id');
            $table->integer('assignee_id')->nullable();
            $table->string('domain_name');
            $table->text('remarks')->nullable();
            $table->string('status')->default('Unassigned');
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('requestor_id');
            $table->index('domain_name');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dticket');
    }
}
