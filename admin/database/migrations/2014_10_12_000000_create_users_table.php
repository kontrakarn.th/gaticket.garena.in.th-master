<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('role');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('avatar')->nullable();
            $table->integer('group')->nullable();
            $table->string('user_status')->default('Active');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->index('role');
            $table->index('name');
            $table->index('email');
            $table->index('user_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
