<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->integer('dticket_id')->nullable();
            $table->string('project_name');
            $table->string('server_test');
            $table->string('domain_name');
            $table->integer('region')->nullable();
            $table->string('ip_server_test')->nullable();
            $table->integer('status_domain')->default('0');
            $table->dateTime('expired_ssl')->nullable();
            $table->integer('status_ssl')->default('0');
            $table->dateTime('live_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('db')->nullable();
            $table->string('last_ip')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
