<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  
  <title>Manage Users · GA Ticket Admin</title>

  <!-- General CSS Files -->
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  
  
  <link rel="icon" 
      type="image/png" 
      href="{{ Config::get('app.url').'/img/favicon.png' }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/bootstrap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/common.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/style.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/components.css' }}">
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/jquery.dataTables.css' }}">
  </link>
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/dataTables.bootstrap4.css' }}">
  </link>
</head>

<?php
  $csrf = csrf_token();
?>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" style="text-decoration:none" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <?php
              if ($user['avatar'] === null) {
                $user['avatar'] = Config::get('app.url').'/img/default_avatar3.png';
              }
            ?>
            <img alt="image" src="{{$user['avatar']}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{$user['name']}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
            <div style="font-size:10px;word-wrap:break-word;text-align:center"class="dropdown-title">
                    {{$user['email']}}
                </div>  
              <a href="/api/logout" class="dropdown-item has-icon text-danger" style="text-decoration:none">
                <i class="fas fa-sign-out-alt"></i> Log out
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="/admin/dashboard">GA Ticket Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="/admin/dashboard">GA</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Home</li>
              <li><a class="nav-link" href="{{ env('APP_URL') }}/admin/dashboard"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
              <li class="menu-header">Manage Tickets</li>
              <li><a class="nav-link" href="/admin/ga-tickets"><i class="fas fa-columns"></i> <span>Manage GA Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/domain-tickets"><i class="fas fa-columns"></i> <span>Manage Domain Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/gcloud-list"><i class="fas fa-columns"></i> <span>GCloud Lists</span></a></li>
              <?php
                if ($user['role'] == "super_admin") {
                  echo "<li class=\"menu-header\">For Super Admins</li>";
                  echo "<li class=\"active\"><a class=\"nav-link\" href=\"/admin/users\"><i class=\"far fa-user\"></i> <span>Manage Users</span></a></li>";
                }
              ?>
              <li class="menu-header">Create a Ticket</li>
              <li><a class="nav-link" href="/admin/redirect-to-user-page"><i class="fas fa-ellipsis-h"></i> <span>Go to User Page</span></a></li>
              
              
            </ul>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        @if (session()->has('message'))
        <div class="alert {{ session('alert') ?? 'alert-info' }} alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    {{ session('message') }}
                </div>
        </div>
        @endif
        <section class="section">
          <div class="section-header">
            <h1>Manage Users</h1>
          </div>

          <div class="section-body">
            <div class="card">
              <div class="card-body" style="overflow-x:overlay">

                <a style="text-decoration:none;margin-bottom:10px;color:white
                ;float:right"class="btn btn-primary" href="/admin/users/new-user">
                    New User
                </a>
                <div class="main_content">
                <table id="myTable" class="row-border hover stripe" style="width:100%">
                    <thead>
                        <tr>
                                <td class="dont_sort"></td>
                                <td>
                                    Name
                                </td>
                            @foreach ($table_columns as $key => $table_column)
                                
                                <td>
                                    {{$table_column[1]}}
                                </td>
                            @endforeach
                                <td class="dont_sort">
                                  
                                </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
  </script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
  </script>
  <?php
    use Firebase\JWT\JWT;
    $key = env("JWT_ENCRYPTION_KEY");
    $jwt = JWT::encode(session("Authorization"), $key);
  ?>
  <script type="text/javascript" charset="utf8">
    $(document).ready( function () {
      $('#myTable').DataTable({
        "order": [
          6,
          "desc"
        ],
        "serverSide": true,
        "ajax": {
          url: "{{ env('APP_URL') }}/api/users",
          headers: {
            Authorization: "{{ $jwt }}",
          }
        },
        "columns": [
          { 
            "defaultContent": "",
            "render": function ( data, type, row, meta ) {
              if (row['avatar'] != null) {
                return `<img width="30px" class="rounded-circle mr-1" 
                              src="${row['avatar']}">
                              </img>`
              } else {
                return `<img width="30px" class="rounded-circle mr-1" 
                              src="<?php echo Config::get('app.url') . '/img/default_avatar3.png'; ?>">
                              </img>`
              }
            }
          },
          { "data": "name",
            "render": function ( data, type, row, meta ) {
              return `<a href='/admin/users/${row['user_id']}'>${data}</a>`
            }
          },
          { 
            "data": "email",
          },
          { 
            "data": "role",
            "render": function ( data, type, row, meta ) {
              if (data == 'admin') {
                return 'Admin'
              } else if (data == 'super_admin') {
                return 'Super Admin'
              } else if (data == 'user') {
                return 'User'
              }
            }
          },
          { 
            "data": "user_group.user_group",
            "defaultContent": ""
          },
          { 
            "data": "user_status",
            "defaultContent": '',
            "render": function ( data, type, row, meta ) {
              if (data == 'Active') {
                return '<span class="badge badge-primary">Active</span>'
              } else if (data == 'Inactive') {
                return '<span class="badge badge-secondary">Inactive</span>'
              }
            } 
          },
          { 
            "data": 'created_at',
            "render": function ( data, type, row, meta ) {
              var formatter= 'YYYY-MM-DD HH:mm:ss';
              var d = new Date(data)
              var data = moment(d).format(formatter);
              return data.length > 10 ?
                data.substr( 0, 10 ) + '...':
                data;
            }
          },
          { 
            "data": "deleted_at",
            "defaultContent": '',
            "render": function ( data, type, row, meta ) {
              if (row['role'] == 'super_admin') {
                return '';
              } else if (data == null) {
                return `
                            <form method="POST" action="/admin/users/${row['user_id']}">
                              <input type="hidden" name="_method" value="DELETE"> 
                              <input type="hidden" name="_token" value="{{ $csrf }}">
                              <input type="submit" user_name="${row['name']}" class="btn btn-danger delete-user" value="Deactivate">
                            </form>
                        `
              } else {
                return `
                            <form method="POST" action="/admin/users/${row['user_id']}">
                              <input type="hidden" name="_token" value="{{ $csrf }}">
                              <input type="submit" user_name="${row['name']}" class="btn btn-success" value="Reactivate">
                            </form>
                        `
              }
            }
          },
        ]
      })
    });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ Config::get('app.url').'/assets/js/stisla.js' }}"></script>

  <!-- JS Libraies -->
  <script src="{{ Config::get('app.url').'/node_modules/jquery-sparkline/jquery.sparkline.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/chart.js/dist/Chart.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/owl.carousel.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.js' }}"></script>

  <!-- Template JS File -->
  <script src="{{ Config::get('app.url').'/assets/js/scripts.js' }}"></script>
  <script src="{{ Config::get('app.url').'/assets/js/custom.js' }}"></script>
  <script src="{{ Config::get('app.url').'/js/notify.js' }}"></script>
  
  <script type="text/javascript">
    

    $(()=>{
      $('body').on('click', '.delete-user', function(e){
          e.preventDefault() // Don't post the form, unless confirmed
          if (confirm('Are sure you want to deactivate user ' + $(this).attr('user_name') + '?')) {
              // Post the form
              $(e.target).closest('form').submit() // Post the surrounding form
          }
      });
    })
    
  </script>
  <!-- Page Specific JS File -->
</body>
</html>