
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>View User · GA Ticket Admin</title>

  <link rel="icon" 
      type="image/png" 
      href="{{ Config::get('app.url').'/img/favicon.png' }}">

  <!-- General CSS Files -->
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/jqvmap/dist/jqvmap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.carousel.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css' }}">
  
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/bootstrap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/common.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/style.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/components.css' }}">
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/dataTables.bootstrap4.css' }}">
  </link>
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/jquery.dataTables.css' }}">
  </link>

  <style>
        .container {
        width: 300px;
        }
        

        .container .field {
        width: 70%;
        float:right;
        }

        .container select {
        width: 70%;
        float:right;
        }


  </style>
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" style="text-decoration:none" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <?php
              if ($user['avatar'] === null) {
                $user['avatar'] = Config::get('app.url').'/img/default_avatar3.png';
              }
            ?>
            <img alt="image" src="{{$user['avatar']}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{$user['name']}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
            <div style="font-size:10px;word-wrap:break-word;text-align:center"class="dropdown-title">
                    {{$user['email']}}
                </div>
              <a href="/api/logout" class="dropdown-item has-icon text-danger" style="text-decoration:none">
                <i class="fas fa-sign-out-alt"></i> Log out
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="/admin/dashboard">GA Ticket Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="/admin/dashboard">GA</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Home</li>
              <li><a class="nav-link" href="{{ env('APP_URL') }}/admin/dashboard"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
              <li class="menu-header">Manage Tickets</li>
              <li><a class="nav-link" href="/admin/ga-tickets"><i class="fas fa-columns"></i> <span>Manage GA Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/domain-tickets"><i class="fas fa-columns"></i> <span>Manage Domain Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/gcloud-list"><i class="fas fa-columns"></i> <span>GCloud Lists</span></a></li>
              <?php
                if ($user['role'] == "super_admin") {
                  echo "<li class=\"menu-header\">For Super Admins</li>";
                  echo "<li class=\"active\"><a class=\"nav-link\" href=\"/admin/users\"><i class=\"far fa-user\"></i> <span>Manage Users</span></a></li>";
                }
              ?>
              <li class="menu-header">Create a Ticket</li>
              <li><a class="nav-link" href="/admin/redirect-to-user-page"><i class="fas fa-ellipsis-h"></i> <span>Go to User Page</span></a></li>
              
            </ul>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
      @if ($errors->any())
        @foreach ($errors->all() as $error)
          <div class="alert {{ 'alert-danger' }} alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ $error }}
            </div>
          </div>
        @endforeach
      @endif
        <section class="section">
          <div class="section-header">
            <h1>Manage Users - View User</h1>
          </div>

          <div class="section-body">
            <div class="card">
              <div class="card-body">
                <div style="text-align:center" class="container">
                  <div>
                    @if ($listed_user['deleted_at'] == null)
                      <span class="badge badge-primary">Active</span>
                    @else
                      <span class="badge badge-secondary">Inactive</span>                         
                    @endif
                  </div><br>
                  <?php
                    if ($listed_user['avatar'] === null) {
                      $listed_user['avatar'] = Config::get('app.url').'/img/default_avatar3.png';
                    }
                  ?>
                  <img alt="image" style="margin-bottom:20px;width:60%;" src="{{$listed_user['avatar']}}" class="rounded-circle">
                  
                  <form method="POST" action="/admin/users/{{$listed_user['user_id']}}">
                    <?php
                      $fields = ['name']
                    ?>
                    @method('PUT')
                    <?php $role = $listed_user['role']; ?>
                    @csrf
                    <label >Name</label>
                    <input class="field" type="text" name="name" value="{{$listed_user['name']}}" <?php if ($role =="super_admin") {echo "readonly";} ?> required>
                    <div>
                      <label>Email</label>
                      <input class="field" type="email" <?php if ($role =="super_admin") {echo "readonly";} ?> name="email" value="{{$listed_user['email']}}" required>
                    </div>
                    <div>
                      <label>Role</label>
                      <?php 
                        if ($role =="super_admin") {
                            echo "<input class=\"field\" name=\"role\" readonly value=\"Super Admin\" required>";
                        } else {
                            echo "<select name=\"role\" required>";
                            echo "<option value=\"\">Please select an option</option>";
                                if ($role == "user") {
                                    echo "<option value=\"admin\">Admin</option>";
                                    echo "<option selected=\"selected\"value=\"user\">User</option>";
                                } else if ($role == "admin") {
                                    echo "<option selected=\"selected\" value=\"admin\">Admin</option>";
                                    echo "<option value=\"user\">User</option>";
                                }
                            echo "</select>";
                        }
                      ?>
                    </div>
                    <div>
                      
                      @if ($role === "admin" or $role === "user")
                      <label>Group</label>
                        <select name="group">
                          <option value="">No group</option>
                          @foreach ($user_groups as $key=>$user_group)
                            @if ($listed_user['group'] === $key)
                              <option selected="selected" value="{{$key}}">{{$user_group}}</option>
                            @endif
                            @if ($listed_user['group'] !== $key)
                              <option value="{{$key}}">{{$user_group}}</option>
                            @endif
                          @endforeach
                        </select>
                      @endif
                    </div>
                    <div>
                      <label>Created At</label>
                      <input class="field" readonly name="created_at" value="{{$listed_user['created_at']}}" required></input>
                    </div>   
                      @if ($role == "user" or $role == "admin")
                        <div>
                          <input class="btn btn-primary" style="margin-top:5px;margin-bottom:5px"type="submit" value="Save Changes">
                        </div>
                      @endif
                      <input class="btn btn-danger" style="margin-top:5px;" onclick="goBack()" type="button" value="Back">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
  </script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ Config::get('app.url').'/assets/js/stisla.js' }}"></script>

  <!-- JS Libraies -->
  <script src="{{ Config::get('app.url').'/node_modules/jquery-sparkline/jquery.sparkline.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/chart.js/dist/Chart.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/owl.carousel.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.js' }}"></script>
  <!-- Template JS File -->
  <script src="{{ Config::get('app.url').'/assets/js/scripts.js' }}"></script>
  <script src="{{ Config::get('app.url').'/assets/js/custom.js' }}"></script>

    <script type="text/javascript">
        function goBack() {
          window.location.href = "/admin/users"
        }
    </script>
  <!-- Page Specific JS File -->
</body>
</html>