@component('mail::message')
Hi {{ $markdown_info["name"] }},

Admin **{{ $markdown_info['admin_name'] }}** changed the status of your {{ $markdown_info['domain_or_ga'] }} ticket.

@component('mail::panel')
**{{ $markdown_info['url_or_domain'] }}:** {{ $markdown_info['website_url'] }}

**New Status:** {{ $markdown_info['status'] }}
@endcomponent

@component('mail::button', ['url' => $markdown_info['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
