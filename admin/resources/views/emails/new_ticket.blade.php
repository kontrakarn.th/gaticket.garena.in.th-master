@component('mail::message')
Hi Admins,

**{{ $markdown_info['name'] }}** submitted a new {{ $markdown_info['domain_or_ga'] }} Ticket.

@component('mail::panel')
**{{ $markdown_info['url_or_domain'] }}:** {{ $markdown_info['website_url'] }}
@endcomponent

@component('mail::button', ['url' => $markdown_info['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
