@component('mail::message')
Hi {{ $markdown_info['name'] }},

Admin **{{ $markdown_info['admin_name'] }}** submitted the GA Code for your GA Ticket.

@component('mail::panel')
**URL:** {{ $markdown_info['website_url'] }}

**New Status:** Closed
@endcomponent

@if ($markdown_info['gatag'])
GA Code:
@component('mail::panel')
{{ $markdown_info['gacode'] }}
@endcomponent
@endif

@if ($markdown_info['gatag'])
GA Tag:

@component('mail::panel')
{{ $markdown_info['gatag'] }}
@endcomponent
@endif

@component('mail::button', ['url' => $markdown_info['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
