@component('mail::message')
Hi {{ $markdown_info['name'] }},

Admin **{{ $markdown_info['admin_name'] }}** created a GATicket account for your email.

Please log-in to the website using your Gmail account.

@component('mail::button', ['url' => $markdown_info['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
