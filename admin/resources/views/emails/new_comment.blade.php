@component('mail::message')
Hi {{ $markdown_info['name'] }},

Admin **{{ $markdown_info['admin_name'] }}** commented on your {{ $markdown_info['domain_or_ga'] }} Ticket.

@component('mail::panel')
**{{ $markdown_info['url_or_domain'] }}:** {{ $markdown_info['website_url'] }}
@endcomponent

**Comment:**

@component('mail::panel')
{{ $markdown_info['comment_msg'] }}
@endcomponent

@component('mail::button', ['url' => $markdown_info['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
