@component('mail::message')
Hi Super Admins,

Admin **{{ $markdown_info2['admin_name'] }}** has self-assigned a {{ $markdown_info2['domain_or_ga'] }} ticket.

@component('mail::panel')
**{{ $markdown_info2['url_or_domain'] }}:** {{ $markdown_info2['website_url'] }}
@endcomponent

@component('mail::button', ['url' => $markdown_info2['button']])
Visit GATicket
@endcomponent

Thanks,<br>
GA Ticket Admin
@endcomponent
