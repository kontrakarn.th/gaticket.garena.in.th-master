<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Domain Ticket - ID: {{ $dticket->dticket_id }} · GA Ticket Admin</title>

  <link rel="icon" 
      type="image/png" 
      href="{{ Config::get('app.url').'/img/favicon.png' }}">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/jqvmap/dist/jqvmap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.carousel.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css' }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/common.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/style.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/components.css' }}">
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/dataTables.bootstrap4.css' }}">
  </link>
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/jquery.dataTables.css' }}">
  </link>
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" style="text-decoration:none" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <?php
              if ($user['avatar'] === null) {
                $user['avatar'] = Config::get('app.url').'/img/default_avatar3.png';
              }
            ?>
            <img alt="image" src="{{$user['avatar']}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{$user['name']}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
                <div style="font-size:10px;word-wrap:break-word;text-align:center"class="dropdown-title">
                    {{$user['email']}}
                </div>
              <a href="/api/logout" class="dropdown-item has-icon text-danger" style="text-decoration:none">
                <i class="fas fa-sign-out-alt"></i> Log out
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="/admin/dashboard">GA Ticket​ Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="/admin/dashboard">GA</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Home</li>
              <li><a class="nav-link" href="{{ env('APP_URL') }}/admin/dashboard"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
              <li class="menu-header">Manage Tickets</li>
              <li><a class="nav-link" href="{{ env('APP_URL') }}/admin/ga-tickets"><i class="fas fa-columns"></i> <span>Manage GA Tickets</span></a></li>
              <li class="active"><a class="nav-link" href="{{ env('APP_URL') }}/admin/domain-tickets"><i class="fas fa-columns"></i> <span>Manage Domain Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/gcloud-list"><i class="fas fa-columns"></i> <span>GCloud Lists</span></a></li>
              @if ($user['role'] === "super_admin") 
                  <li class="menu-header">For Super Admins</li>
                  <li><a class="nav-link" href="/admin/users"><i class="far fa-user"></i> <span>Manage Users</span></a></li>
              @endif
              <li class="menu-header">Create a Ticket</li>
              <li><a class="nav-link" href="/admin/redirect-to-user-page"><i class="fas fa-ellipsis-h"></i> <span>Go to User Page</span></a></li>
              
            </ul>
        </aside>
      </div>

      <!-- Main Content -->
    <div class="main-content">
        @if(session()->has('message'))
            <div class="alert {{ session('alert') ?? 'alert-info' }} alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    {{ session('message') }}
                </div>
            </div>
        @endif
        @error('body')
        @foreach ($errors->all() as $error)
          <div class="alert {{ 'alert-danger' }} alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ $error }}
            </div>
          </div>
        @endforeach
        @enderror
        <section class="section">
            <div class="section-header">
                <h1>Manage Domain Tickets - ID: {{ $dticket->dticket_id }}</h1>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12" style="max-width:100%">
                    <div class="card">
                        <div class="card-header">
                            <h4>Requestor Info</h4>
                        </div>
                        <div class="card-body">             
                            <ul class="list-unstyled list-unstyled-border">
                                <li class="media">
                                    <?php
                                        if ($avatar === null) {
                                            $avatar = Config::get('app.url') . '/img/default_avatar3.png';
                                        }
                                    ?>
                                    <img style="word-wrap:normal;margin-top:5px" class="mr-3 rounded-circle" width="50" src="{{ $avatar }}" alt="avatar">
                                    <div class="media-body">
                                        <div class="media-title">{{ $dticket->name }}</div>
                                        <span class="text-small text-muted">{{ $dticket->requestor_email }}</span>
                                        <br>
                                        <span class="text-small text-muted">{{ $group }}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if (! $assignee)
                    @if ($user['role'] === "super_admin")
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12" style="max-width:100%">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Assign</h4>
                                    <form class="card-header-form">
                                        <input type="text" class="form-control"id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                                    </form>
                                </div>
                                <div class="card-body" tabindex="2" style="height:125px; overflow-x:hidden;overflow-y:overlay; outline: none;">
                                    <ul class="list-unstyled list-unstyled-border" id="myUL">
                                        @foreach ($admins as $admin)
                                            <li class="media">
                                                <?php
                                                    if ($admin->avatar === null) {
                                                        $admin->avatar = Config::get('app.url') . '/img/default_avatar3.png';
                                                    }
                                                ?>
                                                <img style="word-wrap:normal" class="mr-2 rounded-circle" width="30" src="{{ $admin->avatar }}" alt="avatar">
                                                <div class="media-body">
                                                    <div class="media-title"><a style="font-size:14px; cursor:default">{{ $admin->name }}</a></div>
                                                    <div style="white-space:pre-line; font-size:12px" class="media-description text-muted">{{ $admin->email }}</div>
                                                </div>
                                                <form method="POST" action="/admin/domain-tickets/{{ $dticket->dticket_id }}/assign-admin">
                                                    @csrf
                                                    @method('PUT')
                                                    <input name="assignee_id" value="{{ $admin->user_id }}" hidden>
                                                    <button class="btn btn-sm btn-warning">Assign</button>
                                                </form>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @else
                        <div>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Assign to Myself</h4>
                                </div>
                                <div class="card-body" style="">
                                    <form method="POST" action="/admin/domain-tickets/{{ $dticket->dticket_id }}/assign-admin">
                                        @csrf
                                        @method('PUT')
                                        <input name="assignee_id" value="{{ $user->user_id }}" hidden>
                                        <button style="float:center;text-align:center;font-size:13px;font-weight:600"class=" btn btn-sm btn-warning">Take on request</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12" style="max-width:100%">
                        <div class="card">
                            <div class="card-header">
                                <h4>Assignee Info</h4>
                            </div>
                            <div class="card-body">             
                                <ul class="list-unstyled list-unstyled-border">
                                    <li class="media">
                                        <img style="word-wrap:normal" class="mr-3 rounded-circle" width="50" src="{{ $assignee->avatar }}" alt="avatar">
                                        <div class="media-body">
                                            <div class="media-title">{{ $assignee->name }}</div>
                                            <span class="text-small text-muted">{{ $assignee->email }}</span>
                                            <br>
                                            <span class="text-small text-muted">{{ $assignee->group }}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                    @if ($dticket->remarks)    
                        <div class="card">
                            <a class="btn btn-primary collapsed" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                View Remarks
                            </a>
                            <div class="collapse" id="collapseExample" style="">
                                <div class="card-body">
                                    <span style="white-space:pre-line">{{ $dticket->remarks }}</span>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h4>Ticket Info</h4>
                            @if ($dticket->status === "Unassigned")
                                <div class="badge badge-pill badge-danger mb-1 float-right" style="margin-left:3px; margin-right:3px">Unassigned</div>
                            @endif
                            @if ($dticket->status === "Open")
                                <div class="badge badge-pill badge-primary mb-1 float-right" style="margin-left:3px; margin-right:3px">Open</div>
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Processing</div>
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Closed</div>
                            @endif
                            @if ($dticket->status === "Processing")
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Open</div>
                                <div class="badge badge-pill badge-primary mb-1 float-right" style="margin-left:3px; margin-right:3px">Processing</div>
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Closed</div>
                            @endif
                            @if ($dticket->status === "Closed")
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Open</div>
                                <div class="badge badge-pill badge-light mb-1 float-right" style="opacity:0.5;margin-left:3px; margin-right:3px">Processing</div>
                                <div class="badge badge-pill badge-primary mb-1 float-right" style="margin-left:3px; margin-right:3px">Closed</div>
                            @endif
                            @if ($dticket->status === "Rejected")
                                <div class="badge badge-pill badge-dark mb-1 float-right" style="margin-left:3px; margin-right:3px">Rejected</div>
                            @endif
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th>Field</th>
                                            <th>Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>                         
                                        <tr>
                                            <th>Domain Name</th>
                                            <td>
                                                {{ $dticket->domain_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Created At</th>
                                            <td>
                                                {{ $dticket->created_at }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if ($dticket->status === "Open")
                                <div class="card-footer">
                                    <form style="display:inline-block;margin-right:10px" method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/update-status">
                                        @csrf
                                        @method('PUT')
                                        <button name="status" value="Processing" class="btn btn-icon icon-left btn-warning"><i class="fas fa-check"></i> Mark as Processing</button>
                                    </form>
                                    <form style="display:inline-block" method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/update-status">
                                        @csrf
                                        @method('PUT')
                                        <input hidden value="Rejected" name="status">
                                        <input id="reject_msg" hidden value="" name="reject_msg">
                                        <button name="status" id="reject" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Reject</button>
                                    </form>
                                </div>
                            @endif
                            @if ($dticket->status === "Unassigned")
                                <div class="card-footer">
                                    <form style="display:inline-block" method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/update-status">
                                        @csrf
                                        @method('PUT')
                                        <input hidden value="Rejected" name="status">
                                        <input id="reject_msg" hidden value="" name="reject_msg">
                                        <button name="status" id="reject" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Reject</button>
                                    </form>
                                </div>
                            @endif
                            @if ($dticket->status === "Processing")
                                <div class="card-footer">
                                    <form style="display:inline-block;margin-right:10px" method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/update-status">
                                        @csrf
                                        @method('PUT')
                                        <button name="status" value="Closed" class="btn btn-icon icon-left btn-primary"><i class="fas fa-check"></i> Mark as Closed</button>
                                    </form>
                                    <form style="display:inline-block" method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/update-status">
                                        @csrf
                                        @method('PUT')
                                        <input hidden value="Rejected" name="status">
                                        <input id="reject_msg" hidden value="" name="reject_msg">
                                        <button name="status" id="reject" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Reject</button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Comments</h4>
                        </div>
                        <div class="card-body" tabindex="2" style="height:315px;overflow-x:hidden;overflow-y:overlay; outline: none;">             
                            <ul class="list-unstyled list-unstyled-border">
                                @foreach ($dcomments as $comment)
                                    <li style="display:block"class="media">

                                        <div class="media-body">
                                            <img style="word-wrap:normal" class="mr-3 rounded-circle" width="40" src="{{ $comment->avatar }}" alt="avatar">

                                            <span class="float-right" >
                                                <span class="dont-weight-600 text-muted text-small">{{ $comment->created_at }}
                                                </span>
                                            </span>
                                            <span class="media-title">{{ $comment->name }}
                                            </span>
                                            
                                            
                                        </div>
                                        <span style="margin-left:60px;overflow:auto;white-space:pre-line;display:block;width: -webkit-fill-available" class="media-description text-muted">{{ $comment->body }}
                                            </span>


                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="form-group">
                                <label style="margin-top:10px">Add Comment:</label>
                                <form method="POST" action="{{ env('APP_URL') }}/admin/domain-tickets/{{ $dticket->dticket_id }}/create-comment">
                                    @csrf
                                    <textarea style="resize: none" required maxlength="1000" class="form-control" name="body"></textarea>
                                    <br>
                                    <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
  </script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ Config::get('app.url').'/assets/js/stisla.js' }}"></script>

  <!-- JS Libraies -->
  <script src="{{ Config::get('app.url').'/node_modules/jquery-sparkline/jquery.sparkline.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/chart.js/dist/Chart.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/owl.carousel.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.js' }}"></script>
  <script>
    function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
        } else {
        li[i].style.display = "none";
        }
    }
    }
  </script>
  <!-- Template JS File -->
  <script src="{{ Config::get('app.url').'/assets/js/scripts.js' }}"></script>
  <script src="{{ Config::get('app.url').'/assets/js/custom.js' }}"></script>
  <script type="text/javascript">
    $(()=>{
      $('#reject').click(function(e) {
          e.preventDefault() // Don't post the form, unless confirmed
          var reject_msg = window.prompt("Note: This will delete this domain from g-cloud list.\n\nPlease provide a reason for rejection:","").trim();
          if (reject_msg !== "") {
              // Post the form
              $('#reject_msg').attr("value", "Ticket rejected. Reason for rejection: " + reject_msg)
              //$('#comment_body').closest('form').submit()
              $(e.target).closest('form').submit() // Post the surrounding form
          }
      });
    })
  </script>
  <!-- Page Specific JS File -->
</body>
</html>