<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>GCloud List · GA Ticket Admin</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/jqvmap/dist/jqvmap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.carousel.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css' }}">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/5.3.5/sweetalert2.min.css">
  
  <link rel="icon" type="image/png" href="{{ Config::get('app.url').'/img/favicon.png' }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/bootstrap.min.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/css/common.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/style.css' }}">
  <link rel="stylesheet" href="{{ Config::get('app.url').'/assets/css/components.css' }}">
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/dataTables.bootstrap4.css' }}">
  <link rel="stylesheet" type="text/css" href="{{ Config::get('app.url').'/css/jquery.dataTables.css' }}">
  <style>
    body {
      padding: 0 !important;
    }
  </style>
  <style>
    /* Tooltip container */
    .tooltip {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
    }

    /* Tooltip text */
    .tooltip .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      padding: 5px 0;
      border-radius: 6px;
    
      /* Position the tooltip text - see examples below! */
      position: absolute;
      z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip:hover .tooltiptext {
      visibility: visible;
    }
  </style>

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"></script> -->
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" style="text-decoration:none" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <?php
              if ($user['avatar'] === null) {
                $user['avatar'] = Config::get('app.url').'/img/default_avatar3.png';
              }
            ?>
            <img alt="image" src="{{$user['avatar']}}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{$user['name']}}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div style="font-size:10px;word-wrap:break-word;text-align:center"class="dropdown-title">
              {{$user['email']}}
              </div>
              <a href="/api/logout" class="dropdown-item has-icon text-danger" style="text-decoration:none">
                <i class="fas fa-sign-out-alt"></i> Log out
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="/dashboard">GA Ticket Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="/dashboard">GA</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Home</li>
              <li><a class="nav-link" href="{{ env('APP_URL') }}/admin/dashboard"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
              <li class="menu-header">Manage Tickets</li>
              <li><a class="nav-link" href="/admin/ga-tickets"><i class="fas fa-columns"></i> <span>Manage GA Tickets</span></a></li>
              <li><a class="nav-link" href="/admin/domain-tickets"><i class="fas fa-columns"></i> <span>Manage Domain Tickets</span></a></li>
              <li class="active"><a class="nav-link" href="/admin/gcloud-list"><i class="fas fa-columns"></i> <span>GCloud Lists</span></a></li>
              @if ($user['role'] === "super_admin") 
              <li class="menu-header">For Super Admins</li>
              <li><a class="nav-link" href="/admin/users"><i class="far fa-user"></i> <span>Manage Users</span></a></li>
              @endif
              <li class="menu-header">Create a Ticket</li>
              <li><a class="nav-link" href="/admin/redirect-to-user-page"><i class="fas fa-ellipsis-h"></i> <span>Go to User Page</span></a></li>
            </ul>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        @if(session()->has('message'))
          <div class="alert {{ session('alert') ?? 'alert-info' }} alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                  <span>×</span>
              </button>
              {{ session('message') }}
            </div>
          </div>
        @endif
        <section class="section">
          <div class="section-header">
            <h1>GCloud List</h1>
          </div>
          <div class="section-body">
            <!-- Modal -->
            <div class="modal fade" id="addProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <form id="form-data" name="form-data" class="needs-validation" onSubmit="sendAPi(event)" novalidate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body" id="form-modal-body">
                          <div class="form-group">
                            <label for="project_name">Project Name</label>
                            <input type="text" class="form-control" name="project_name" id="project_name" required pattern="^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+$">
                            <div class="invalid-feedback">
                              Invalid Domain
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="region" >Region</label>
                            <select name="region" id="region" class="form-control" required>
                              <option value="" disabled selected>- Choose Region -</option>
                              @foreach($regions as $region)
                                  <option value="{{ $region->id }}" data-id="{{ $region->ip_test }}">{{ $region->region }}</option>
                              @endforeach
                            </select>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="ip_server_test" >IP Server test</label>
                            <input type="text" class="form-control" name="ip_server_test" id="ip_server_test" pattern="^((\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$" nullable>
                            <div class="invalid-feedback">
                              Invalid IP Address
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="status_domain" >Status Domain</label>
                            <select name="status_domain" id="status_domain" class="form-control" required>
                                <option value="" disabled selected>- Choose Status Domain -</option>
                                <option value="done">Done</option>
                                <option value="not_done">Not Done</option>
                            </select>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="expired_ssl" >Expired SSL</label>
                            <input type="date" class="form-control" name="expired_ssl" id="expired_ssl" required>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="status_ssl" >Status SSL</label>
                            <select name="status_ssl" id="status_ssl" class="form-control" required>
                                <option value="" disabled selected>- Choose Status SSL -</option>
                                <option value="done">Done</option>
                                <option value="not_done">Not Done</option>
                            </select>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="live_date" >Live Date</label>
                            <input type="date" class="form-control" name="live_date" id="live_date" required>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="end_date" >End Date</label>
                            <input type="date" class="form-control" name="end_date" id="end_date" required>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="db" >DB</label>
                            <select name="db" id="db" class="form-control" required>
                                <option value="" disabled selected>- Choose DB -</option>
                                <option value="our_server">OUR SERVER</option>
                                <option value="gcloud">GCLOUD</option>
                            </select>
                            <div class="invalid-feedback">
                              Required
                            </div>
                          </div>
                          <div class="form-group" id="remark_form_group">
                            <label for="remarks">Remarks</label>
                            <textarea maxlength="10000"style="resize: none;height:150px" class="form-control" name="remarks" id="remarks" cols="30" rows="10"></textarea>
                          </div>
                          <p class="text-danger" style="margin-left:10px">Note: Domain ticket will be created automatically</p>
                        </div>
                        <div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button id="addProject_button" type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>         
            <div class="card">
              <div class="card-header">
                <div class="card-header-action">
                  <button href="#addProject" data-toggle="modal" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>Add</button>
                </div>
              </div>
              <div class="card-body" style="overflow-x:overlay; overflow-y:hidden">
                <p class="text-danger">* ถ้าแก้ไขชื่อ Project จะทำการ Gen Server Test และ Domain ให้อัตโนมัติ</p>
                <p class="text-danger">* ถ้าแก้ไข Region จะ Auto IP Address</p>
                <p class="text-danger">* กด Domain Name เพิ่อดู domain ticket</p>
                <table id="myTable" class="row-border" style="width:100% ;text-align:center">
                  <thead>
                    <tr>
                      <td>Project Name</td>
                      <td>Server Test</td>
                      <td>Domain Name</td>
                      <td><i class="far fa-edit"></i>  Region</td>
                      <td>IP Test</td>
                      <td><i class="far fa-edit"></i>  Status Domain</td>
                      <td><i class="far fa-edit"></i>  Expired SSL</td>
                      <td><i class="far fa-edit"></i>  Status SSL</td>
                      <td><i class="far fa-edit"></i>  Live Date</td>
                      <td><i class="far fa-edit"></i>  End Date</td>
                      <td><i class="far fa-edit"></i>  DB</td>
                      <td></i>  Delete</td>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                @foreach ($projects as $project)   
                  <!-- Modal Edit Region -->
                  <div class="modal fade" id="editRegion{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <form name="form-data" class="needs-validation" onSubmit="saveEditRegion({{ $project->id }}, event)" novalidate>
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Region</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                <label for="region{{$project->id}}" >Region</label>
                                <select name="region{{$project->id}}" id="region{{$project->id}}" class="form-control" required>
                                    <option value="" disabled selected>- Choose Region -</option>
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}" data-id="{{ $region->ip_test }}" {{ $project->region == $region->id ? 'selected' : "" }}>{{ $region->region }}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_region_button{{$project->id}}" type="submit" class="btn btn-primary">Save changes</button>
                              </div>
                            </form>
                          </div>
                      </div>
                  </div>


                  <!-- Modal Edit Status Domain -->
                  <div class="modal fade" id="editStatusDomain{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <form name="form-data" class="needs-validation" onSubmit="saveEditStatusDomain({{ $project->id }}, event)" novalidate>
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Status Domain</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="status_domain{{$project->id}}" >Status Domain</label>
                                  <select name="status_domain{{$project->id}}" id="status_domain{{$project->id}}" class="form-control" required>
                                      <option value="" disabled selected>- Choose Status Domain -</option>
                                      <option value="done" {{ $project->status_domain == 1 ? "selected" : "" }}>Done</option>
                                      <option value="not_done" {{ $project->status_domain == 0 ? "selected" : "" }}>Not Done</option>
                                  </select>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_status_domain_button{{$project->id}}" type="submit" class="btn btn-primary">Save changes</button>
                              </div>
                          </div>
                        </form>
                      </div>
                  </div>

                  <!-- Modal Edit Expired SSL -->
                  <div class="modal fade" id="editExpiredSSL{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Expired SSL</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="expired_ssl{{$project->id}}" >Expired SSL</label>
                                  <input type="date" class="form-control" name="expired_ssl{{$project->id}}" id="expired_ssl{{$project->id}}" value="{{ \Illuminate\Support\Carbon::parse($project->expired_ssl)->format('Y-m-d') }}">
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_expired_ssl_button{{$project->id}}" type="button" class="btn btn-primary" onclick="saveEditExpiredSSL({{ $project->id }}, event)">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>

                  <!-- Modal Edit Status SSL -->
                  <div class="modal fade" id="editStatusSSL{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Status SSL</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="status_ssl{{$project->id}}" >Status SSL</label>
                                  <select name="status_ssl{{$project->id}}" id="status_ssl{{$project->id}}" class="form-control">
                                      <option disabled selected>- Choose Status SSL -</option>
                                      <option value="done" {{ $project->status_ssl == 1 ? "selected" : "" }}>Done</option>
                                      <option value="not_done" {{ $project->status_ssl == 0 ? "selected" : "" }}>Not Done</option>
                                  </select>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_status_ssl_button{{$project->id}}" type="button" class="btn btn-primary" onclick="saveEditStatusSSL({{ $project->id }}, event)">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>


                  <!-- Modal Edit Live Date -->
                  <div class="modal fade" id="editLiveDate{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Live Date</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="live_date{{$project->id}}" >Live Date</label>
                                  <input type="date" class="form-control" name="live_date{{$project->id}}" id="live_date{{$project->id}}" value="{{ \Illuminate\Support\Carbon::parse($project->live_date)->format('Y-m-d') }}">
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_live_date_button{{$project->id}}" type="button" class="btn btn-primary" onclick="saveEditLiveDate({{ $project->id }}, event)">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>


                  <!-- Modal Edit End Date -->
                  <div class="modal fade" id="editEndDate{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit End Date</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="end_date{{$project->id}}" >End Date</label>
                                  <input type="date" class="form-control" name="end_date{{$project->id}}" id="end_date{{$project->id}}" value="{{ \Illuminate\Support\Carbon::parse($project->end_date)->format('Y-m-d') }}">
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_end_date_button{{$project->id}}" type="button" class="btn btn-primary" onclick="saveEditEndDate({{ $project->id }}, event)">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>


                  <!-- Modal Edit Server DB -->
                  <div class="modal fade" id="editServerDB{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Server DB</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <label for="DB{{$project->id}}" >DB</label>
                                  <select name="DB{{$project->id}}" id="DB{{$project->id}}" class="form-control" required>
                                      <option value="" disabled selected>- Choose DB -</option>
                                      <option value="our_server" {{ $project->db == "our_server" ? "selected" : "" }}>OUR SERVER</option>
                                      <option value="gcloud" {{ $project->db == "gcloud" ? "selected" : "" }}>GCLOUD</option>
                                  </select>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button id="edit_db_button{{$project->id}}" type="button" class="btn btn-primary" onclick="saveEditServerDB({{ $project->id }}, event)">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
  </script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>

  <!--new-->
  <!-- <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
  <script>
    $(document).ready( function () {
      console.log($('#project23'));
      $('#project23').tooltip();
    });
  </script> -->
  
  <?php
    use Firebase\JWT\JWT;
    $key = env("JWT_ENCRYPTION_KEY");
    $jwt = JWT::encode(session("Authorization"), $key);
  ?>
  <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  </script>

  <script type="text/javascript" charset="utf8">
    $(document).ready( function () {
      $('#myTable').DataTable({
        "order": [
          11,
          "desc"
        ],
        "serverSide": true,
        "ajax": {
          url: "{{ env('APP_URL') }}/admin/gcloud-list/table",
          headers: {
            Authorization: "{{ $jwt }}",
          }
        },
        "columns": [
          { 
            "data": "project_name",
            "render": function (data, type, row, meta ) {
              if (data.length > 25) {
                let val = data.substr(0, 25)+"...";
                return '<div id="project'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'"><a href="https://'+data+'" target="_blank">'+val+'</a></div>'
              } else {
                return '<div id="project'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'"><a href="https://'+data+'" target="_blank">'+data+'</a></div>'
              }
            }
          },
          { 
            "data": "server_test",
            "render": function(data, type, row, meta) {
              if (data == null) {
                return '<div onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" id="serverTest'+row.id+'"><a href="http://'+data+'" target="_blank" ></a></div>'
              }
              if (data.length > 15) {
                let val = data.substr(0, 15)+"...";
                return '<div onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" id="serverTest'+row.id+'"><a href="http://'+data+'" target="_blank" >'+val+'</a></div>'
              } else {
                return '<div onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" id="serverTest'+row.id+'"><a href="http://'+data+'" target="_blank" >'+data+'</a></div>'
              }
            },
          },
          { 
            "data": "domain_name",
            "render": function(data, type, row, meta) {
              if (data.length > 15) {
                let val = data.substr(0, 15)+"...";
                return '<div onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" id="domainName'+row.id+'"><a href="{{env('APP_URL')}}/admin/domain-tickets/'+row.dticket_id+'">'+val+'</a></div>'
              } else {
                return '<div onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" id="domainName'+row.id+'"><a href="{{env('APP_URL')}}/admin/domain-tickets/'+row.dticket_id+'">'+data+'</a></div>'
              }
            },
          },
          { "data": "region",
            "render": function(data, type, row, meta) {
              if (data == 1) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#cbe2b0\'" style="background-color: #cbe2b0; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 2) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f1d1d1\'" style="background-color: #f1d1d1; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 3) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f3e1e1\'" style="background-color: #f3e1e1; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 4) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#faf2f2\'" style="background-color: #faf2f2; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 5) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f6d186\'" style="background-color: #f6d186; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 6) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f19292\'" style="background-color: #f19292; cursor: pointer" >'+row.get_region.region+'</div>'
              } else if (data == 7) {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ccffff\'" style="background-color: #ccffff; cursor: pointer" >'+row.get_region.region+'</div>'
              } else {
                return '<div id="dataRegion'+row.id+'" data-toggle="modal" data-target="#editRegion'+row.id+'" onmouseover="this.style.backgroundColor=\'#ffffff\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="background-color: #ffffff; cursor: pointer" >-</div>'
              }
            },
          },
          { "data": "ip_server_test",
            "render": function (data, type, row, meta) {
              if (data) {
                return '<div id="ipServerTest'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" >'+data+'</div>'
              } else {
                return '<div id="ipServerTest'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" ></div>'
              }
            }
          },
          { 
            "data": "status_domain",
            "render": function ( data, type, row, meta ) {
              if (data == 1) {
                return '<div id="statusDomain'+row.id+'" data-toggle="modal" data-target="#editStatusDomain'+row.id+'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#4CAF50\'" style="cursor: pointer" class="bg-success">Done</div>'
              } else {
                return '<div id="statusDomain'+row.id+'" data-toggle="modal" data-target="#editStatusDomain'+row.id+'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" style="cursor: pointer" class="bg-danger">Not Done</div>'
              }
            }
          },
          { 
            "data": "expired_ssl",
            "render": function ( data, type, row, meta ) {
              if (data) {
                if (data < '{{\Carbon\Carbon::now() }}') {
                  return '<div id="dataExpiredSSL'+row.id+'" data-toggle="modal" data-target="#editExpiredSSL'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" style="background-color: #F44336; cursor: pointer">'+moment(data).format('D MMM YY')+'</div>';
                } else {
                  return '<div id="dataExpiredSSL'+row.id+'" data-toggle="modal" data-target="#editExpiredSSL'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">'+moment(data).format('D MMM YY')+'</div>';
                }
              } else {
                return '<div id="dataExpiredSSL'+row.id+'" data-toggle="modal" data-target="#editExpiredSSL'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">-</div>';
              }
              
            }
          },
          { 
            "data": "status_ssl",
            "render": function ( data, type, row, meta ) {
              if (data == 1) {
                return '<div id="dataStatusSSL'+row.id+'" data-toggle="modal" data-target="#editStatusSSL'+row.id+'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#4CAF50\'" style="cursor: pointer" class="bg-success">Done</div>'
              } else {
                return '<div id="dataStatusSSL'+row.id+'" data-toggle="modal" data-target="#editStatusSSL'+row.id+'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" style="cursor: pointer" class="bg-danger">Not Done</div>'
              }
            }
          },
          { 
            "data": "live_date",
            "render": function ( data, type, row, meta ) {
              if (data) {
                return '<div id="dataLiveDate'+row.id+'" data-toggle="modal" data-target="#editLiveDate'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">'+moment(data).format('D MMM YY')+'</div>';
              } else {
                return '<div id="dataLiveDate'+row.id+'" data-toggle="modal" data-target="#editLiveDate'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">-</div>';
              }
            }
          },
          { 
            "data": "end_date",
            "render": function ( data, type, row, meta ) {
              if (data) {
                return '<div id="dataEndDate'+row.id+'" data-toggle="modal" data-target="#editEndDate'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">'+moment(data).format('D MMM YY')+'</div>';
              } else {
                return '<div id="dataEndDate'+row.id+'" data-toggle="modal" data-target="#editEndDate'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor: pointer">-</div>';
              }
            }
          },
          { 
            "data": "db",
            "render": function ( data, type, row, meta ) {
              if (data == 'our_server') {
                return '<div id="dataDB'+row.id+'" data-toggle="modal" data-target="#editServerDB'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'rgb(223,157,155)\'" style="cursor: pointer; background-color: rgb(223,157,155)">OUR SERVER</div>'
              } else if (data == 'gcloud') {
                return '<div id="dataDB'+row.id+'" data-toggle="modal" data-target="#editServerDB'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'rgb(177,168,211)\'" style="cursor: pointer; background-color: rgb(177,168,211)">GCLOUD</div>'
              } else {
                return '<div id="dataDB'+row.id+'" data-toggle="modal" data-target="#editServerDB'+row.id+'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="background-color: #ffffff; cursor: pointer">-</div>'
              }
            }
          },
          {
            "render": function (data, type, row, meta) {
              return '<form onSubmit="deleteProject('+row.id+', event)"><button type="submit" id="delete_project_button'+row.id+'" class="btn-sm btn-danger" onclick="return confirm(\'Are you sure?\')">Delete</button></form>'
            }
          },
        ]
      })
    });
  </script>
  <script>
    $(document).on("change","#region",function () {
        let ip = $(this).find(':selected').attr('data-id') ;
        $('#ip_server_test').val(ip);
    })

    function sendAPi(event) {
      event.preventDefault();
      var formData = new FormData($('#form-data')[0]);
      $.ajax({
        type: 'POST',
        url: "{{ env('APP_URL') }}/admin/sendApi",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('#addProject_button').attr('disabled', 'disabled');
        },
        success: function (data) {
          if(data.status){
            $('#addProject').modal('toggle');
            $('#myTable').find('tbody').prepend(data.new_project_html);
            $(data.modal_html).insertAfter('#myTable');
            $('#addProject_button').attr('disabled', false);

            let form = $('#form-data');
            form.attr('class', 'needs-validation')
            form.empty();
            form.append(data.form);

            swal(
              'Good job!',
              data.message,
              'success'
            ).then(function () {

            })
          } else {
            $('#addProject_button').attr('disabled', false);
            swal(
              'Something Wrong!',
              data.message,
              'error'
            ).then(function () {
              
            });
          }
        },
        error:function (feedback) {
          $('#addProject_button').attr('disabled', false);
          swal(
            'Something Wrong!',
            'Error',
            'error'
          ).then(function() {
            
          });
        }
      });
    }
    function deleteProject(id, event) {
      event.preventDefault();
      $.ajax({
        type: 'DELETE',
        url: "{{ env('APP_URL') }}/admin/delete-project",
        data: {
          idProject: id
        },
        dataType: 'json',
        beforeSend: function(){
            $('#delete_project_button'+id).attr('disabled', 'disabled');
        },
        success: function (data) {
          if(data.status){
            $('#project'+id).parent().parent().remove();
            $('#editProjectName'+id).remove();
            $('#editRegion'+id).remove();
            $('#editStatusDomain'+id).remove();
            $('#editExpiredSSL'+id).remove();
            $('#editStatusSSL'+id).remove();
            $('#editLiveDate'+id).remove();
            $('#editEndDate'+id).remove();
            $('#editServerDB'+id).remove();
            swal(
              'Good job!',
              data.message,
              'success'
            ).then(function () {

            })
          } else {
            $('#delete_project_button'+id).attr('disabled', false);
            swal(
              'Something Wrong!',
              data.message,
              'error'
            ).then(function () {
              
            });
          }
        },
        error:function (feedback) {
          $('#delete_project_button'+id).attr('disabled', false);
          swal(
            'Something Wrong!',
            'Error',
            'error'
          ).then(function() {
            
          });
        }
      });
    }
    
    function saveEditRegion(id, event) {
      event.preventDefault();
      let new_region = $('#region'+id).val();

      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editRegion",
          data: {
              idProject : id,
              newRegion : new_region
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_region_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
              if(data.status){
                {{--location.href = "{{ env('APP_URL') }}/admin/gcloud-list";--}}
                $('#editRegion'+id).modal('toggle');
                let region = $("#dataRegion"+id).parent() ;
                let ipserverTest  = $("#ipServerTest"+id) ;
                let domain = $("#domainName"+id).parent() ;
                
                region.remove();
                ipserverTest.empty();
                
                $(data.region_html).insertAfter(domain);
                ipserverTest.append(data.ip_test);
                $('#edit_region_button'+id).attr('disabled', false);

                swal(
                    'Good job!',
                    data.message,
                    'success'
                ).then(function () {
                    
                });
              } else {
                $('#edit_region_button'+id).attr('disabled', false);
                swal(
                    'Something Wrong!',
                    data.message,
                    'error'
                ).then(function () {

                });
              }
          },
          error: function (xhr, type) {
            $('#edit_region_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
        });
    }


    function saveEditStatusDomain(id, event) {
      event.preventDefault();
      let new_status_domain = $('#status_domain'+id).val();
      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editStatusDomain",
          data: {
              idProject : id,
              newStatusDomain : new_status_domain
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_status_domain_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
            if(data.status){
              $('#editStatusDomain'+id).modal('toggle');
              let status_domain = $('#statusDomain'+id).parent();
              let ipServerTest = $('#ipServerTest'+id).parent();

              status_domain.remove();
              $(data.status_domain_html).insertAfter(ipServerTest);
              $('#edit_status_domain_button'+id).attr('disabled', false);
              swal(
                  'Good job!',
                  data.message,
                  'success'
              ).then(function () {

              });
            }else{
              $('#edit_status_domain_button'+id).attr('disabled', false);
              swal(
                  'Something Wrong!',
                  data.message,
                  'error'
              ).then(function () {

              });
            }
          },
          error: function (xhr, type) {
            $('#edit_status_domain_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
        });
    }

    function saveEditExpiredSSL(id, event) {
      event.preventDefault();
      let new_expired_date = $('#expired_ssl'+id).val();

      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editExpireDateSSL",
          data: {
              idProject : id,
              newExpiredDateSSL : new_expired_date
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_expired_ssl_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
              if(data.status){
                $('#editExpiredSSL'+id).modal('toggle');
                $('#dataExpiredSSL'+id).parent().remove();
                let status_domain = $('#statusDomain'+id).parent();
                $(data.expired_ssl_html).insertAfter(status_domain);
                $('#edit_expired_ssl_button'+id).attr('disabled', false);
                swal(
                  'Good job!',
                  data.message,
                  'success'
                ).then(function () {

                });
              }else{
                $('#edit_expired_ssl_button'+id).attr('disabled', false);
                swal(
                    'Something Wrong!',
                    data.message,
                    'error'
                ).then(function () {

                });
              }
          },
          error: function (xhr, type) {
            $('#edit_expired_ssl_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
      });
    }


    function saveEditStatusSSL(id, event) {
      event.preventDefault();
        let new_status_ssl = $('#status_ssl'+id).val();
        $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editStatusSSL",
          data: {
              idProject : id,
              newStatusSSL : new_status_ssl
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_status_ssl_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
              if(data.status){
                $('#editStatusSSL'+id).modal('toggle');
                $('#dataStatusSSL'+id).parent().remove();

                let expire_ssl = $('#dataExpiredSSL'+id).parent();
                $(data.status_ssl_html).insertAfter(expire_ssl);

                $('#edit_status_ssl_button'+id).attr('disabled', false);
                swal(
                    'Good job!',
                    data.message,
                    'success'
                ).then(function () {

                });
              }else{
                $('#edit_status_ssl_button'+id).attr('disabled', false);
                swal(
                    'Something Wrong!',
                    data.message,
                    'error'
                ).then(function () {

                });
              }
            },
            error: function (xhr, type) {
              $('#edit_status_ssl_button'+id).attr('disabled', false);
              swal(
                  'Something Wrong!',
                  'Error',
                  'error'
              ).then(function () {

              });
            }
        });
    }


    function saveEditLiveDate(id, event) {
      event.preventDefault();
      let new_live_date = $('#live_date'+id).val();

      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editLiveDate",
          data: {
              idProject : id,
              newLiveDate : new_live_date
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_live_date_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
              if(data.status){
                $('#editLiveDate'+id).modal('toggle');
                $('#dataLiveDate'+id).parent().remove();
                status_ssl = $('#dataStatusSSL'+id).parent();
                $(data.live_date_html).insertAfter(status_ssl);
                $('#edit_live_date_button'+id).attr('disabled', false);
                swal(
                  'Good job!',
                  data.message,
                  'success'
                ).then(function () {
                });
              }else{
                $('#edit_live_date_button'+id).attr('disabled', false);
                swal(
                  'Something Wrong!',
                  data.message,
                  'error'
                ).then(function () {

                });
              }
          },
          error: function (xhr, type) {
            $('#edit_live_date_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
      });
    }

    function saveEditEndDate(id, event) {
      event.preventDefault();
      let new_end_date = $('#end_date'+id).val();

      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editEndDate",
          data: {
              idProject : id,
              newEndDate : new_end_date
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_end_date_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
            if(data.status){
              $('#editEndDate'+id).modal('toggle');
              $('#dataEndDate'+id).parent().remove();
              let live_date = $('#dataLiveDate'+id).parent();
              $(data.end_date_html).insertAfter(live_date);
              $('#edit_end_date_button'+id).attr('disabled', false);
              swal(
                  'Good job!',
                  data.message,
                  'success'
              ).then(function () {

              });
            }else{
              $('#edit_end_date_button'+id).attr('disabled', false);
              swal(
                  'Something Wrong!',
                  data.message,
                  'error'
              ).then(function () {

              });
            }
          },
          error: function (xhr, type) {
            $('#edit_end_date_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
      });
    }


    function saveEditServerDB(id, event) {
      event.preventDefault();
      let new_server_db = $('#DB'+id).val();
      $.ajax({
          type: 'POST',
          url: "{{ env('APP_URL') }}/admin/editServerDB",
          data: {
              idProject : id,
              newServerDB : new_server_db
          },
          dataType: 'json',
          beforeSend: function(){
            $('#edit_db_button'+id).attr('disabled', 'disabled');
          },
          success: function (data) {
            if(data.status){
              $('#editServerDB'+id).modal('toggle');
              $('#dataDB'+id).parent().remove();
              let end_date = $('#dataEndDate'+id).parent();
              $(data.db_html).insertAfter(end_date);
              $('#edit_db_button'+id).attr('disabled', false);
              swal(
                'Good job!',
                data.message,
                'success'
              ).then(function () {

              });
            }else{
              $('#edit_db_button'+id).attr('disabled', false);
              swal(
                'Something Wrong!',
                data.message,
                'error'
              ).then(function () {

              });
            }
          },
          error: function (xhr, type) {
            $('#edit_db_button'+id).attr('disabled', false);
            swal(
                'Something Wrong!',
                'Error',
                'error'
            ).then(function () {

            });
          }
        });
    }
    
  </script>
  <script src="{{Config::get('app.url').'/js/tickets.js'}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ Config::get('app.url').'/assets/js/stisla.js' }}"></script>
  
  
  <!-- JS Libraies -->
  <script src="{{ Config::get('app.url').'/node_modules/jquery-sparkline/jquery.sparkline.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/chart.js/dist/Chart.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/owl.carousel/dist/owl.carousel.min.js' }}"></script>
  <script src="{{ Config::get('app.url').'/node_modules/summernote/dist/summernote-bs4.js' }}"></script>
  <script src="https://cdn.jsdelivr.net/sweetalert2/5.3.5/sweetalert2.min.js"></script>
  <!-- Template JS File -->
  <script src="{{ Config::get('app.url').'/assets/js/scripts.js' }}"></script>
  <script src="{{ Config::get('app.url').'/assets/js/custom.js' }}"></script>

  <!-- Page Specific JS File -->
</body>
</html>