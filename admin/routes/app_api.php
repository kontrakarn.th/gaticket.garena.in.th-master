<?php

$routes = function () {

        Route::group(['prefix' => 'api'], function (){
            

            /*Route::get('/', function () {
                return view('welcome');
            });*/

            Route::group(['namespace' => 'Auth', 'middleware' => 'database'], function() {
                Route::get('/login', 'LoginController@redirectToGoogle');
                Route::get('/login/google/callback', 'LoginController@handleGoogleCallback');
                Route::get('/logout', 'LoginController@logout');
            });

            Route::group(['namespace' => 'api', 'middleware' => ['auth', 'token', 'admin', 'database']], function() {
                Route::get('/ga-tickets', 'TicketsController@getAllGATickets');
                Route::get('/domain-tickets', 'DTicketsController@getAllDomainTickets');
                Route::get('/users', 'UsersController@getAllUsers');
            });
            
            Route::group(['middleware' => ['auth', 'token', 'database'], 'namespace' => 'api'], function () {

                Route::get('/get-user-info', 'UsersController@getCurrentUserInfo');

                Route::get('/get-all-tickets-of-this-user', 'TicketsController@getAllTicketsOfUser');
                Route::post('/create-ticket-send-email', 'TicketsController@createTicketSendEmail');
                
                Route::group(['middleware' => 'ticket_owner'], function() {
                    Route::get('/read-ticket', 'TicketsController@readTicket');
                    Route::get('/read-domain-ticket', 'DTicketsController@readDTicket');
                });

                Route::get('/get-all-domain-tickets-of-user', 'DTicketsController@getAllDTicketsOfUser');
                Route::post('/create-domain-ticket-send-email', 'DTicketsController@createDTicketSendEmail');
                Route::get('/get-website-type', 'TicketsController@getWebsiteType');
                Route::get('/get-region', 'TicketsController@getRegion');

                Route::post('/create-comment-send-email', 'CommentsController@createCommentSendEmail');
                Route::get('/get-all-comments-of-ticket', 'CommentsController@getAllCommentsOfTicket');

                Route::post('/create-domain-comment-send-email', 'DCommentsController@createDCommentSendEmail');
                Route::get('/get-all-comments-of-domain-ticket', 'DCommentsController@getAllCommentsOfDTicket');
            });
        });
};


Route::group(['domain' => 'test.api.gaticket.garena.in.th'], $routes);

//Route::group(['domain' => 'api.gaticket.garena.in.th'], $routes);
