<?php

$routes = function () {
        
    Route::group(['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => ['auth', 'database']], function(){

        Route::group(['middleware' => 'admin'], function() {

            Route::get('/', 'HomeController@showDashboard')->name('index');
            Route::get('/dashboard', 'HomeController@showDashboard')->name('dashboard');
            
            Route::get('/ga-tickets', 'GATicketController@getAllGATickets');
            Route::get('/domain-tickets', 'DomainTicketController@getAllDomainTickets');

            Route::get('/ga-tickets/{ticket_id}', 'GATicketController@viewGATicket');

            Route::put('/ga-tickets/{ticket_id}/update-status', 'GATicketController@updateGATicketStatusSendEmail');
            Route::post('/ga-tickets/{ticket_id}/create-comment', 'GATicketController@createGACommentSendEmail');
            Route::put('/ga-tickets/{ticket_id}/submit-gacode', 'GATicketController@submitGACode');

            Route::get('/domain-tickets/{dticket_id}', 'DomainTicketController@viewDomainTicket');
            Route::put('/domain-tickets/{dticket_id}/update-status', 'DomainTicketController@updateDomainTicketStatusSendEmail');
            Route::post('/domain-tickets/{dticket_id}/create-comment', 'DomainTicketController@createDomainCommentSendEmail');
        
            Route::get('/redirect-to-user-page', 'HomeController@redirectToUserPage');
            
            Route::put('/ga-tickets/{ticket_id}/assign-admin', 'GATicketController@assignAdmin');
            Route::put('/domain-tickets/{dticket_id}/assign-admin', 'DomainTicketController@assignAdmin');

            Route::group(['middleware' => ['auth', 'token', 'admin', 'database']], function() {
                Route::get('/gcloud-list/table', 'IndexController@index2');
            });

            Route::get('/gcloud-list', 'IndexController@index')->name('dashboard2');
            Route::post('/sendApi', 'IndexController@sendApi')->name('sendApi');
            Route::delete('/delete-project', 'IndexController@deleteProject');
            Route::post('/editProjectName', 'IndexController@editProjectName')->name('editProjectName');
            Route::post('/editRegion', 'IndexController@editRegion')->name('editRegion');
            Route::post('/editStatusDomain', 'IndexController@editStatusDomain')->name('editStatusDomain');
            Route::post('/editExpireDateSSL', 'IndexController@editExpireDateSSL')->name('editExpireDateSSL');
            Route::post('/editStatusSSL', 'IndexController@editStatusSSL')->name('editStatusSSL');
            Route::post('/editLiveDate', 'IndexController@editLiveDate')->name('editLiveDate');
            Route::post('/editEndDate', 'IndexController@editEndDate')->name('editEndDate');
            Route::post('/editServerDB', 'IndexController@editServerDB')->name('editServerDB');
        });

        Route::group(['middleware' => 'super_admin'], function() {
            
            Route::get('/users', 'UserController@showUsers')->name('users');
            Route::get('/users/new-user', 'UserController@showNewUser')->name('new-user');
            Route::get('/users/{user_id}', 'UserController@showViewUser')->name('view-user');

            Route::post('/users', 'UserController@createUser')->name('create-user');
            Route::post('/users/{user_id}', 'UserController@reactivateUser')->name('reactivate-user');
            Route::put('/users/{user_id}', 'UserController@updateUser')->name('update-user');
            Route::delete('/users/{user_id}', 'UserController@deleteUser')->name('delete-user');
                      
        });
    });
};


Route::group(['domain' => 'test.admin.gaticket.garena.in.th'], $routes);

Route::group(['domain' => 'admin.gaticket.garena.in.th'], $routes);