<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SelfAssign extends Mailable
{
    use Queueable, SerializesModels;
    public $markdown_info2;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($markdown_info2)
    {
        $this->markdown_info2 = $markdown_info2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.SelfAssign');
    }
}
