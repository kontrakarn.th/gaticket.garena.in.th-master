<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteType extends Model
{
    protected $table = "website_type";
}
