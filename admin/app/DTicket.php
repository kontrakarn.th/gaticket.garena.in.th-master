<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DTicket extends Model
{
    use SoftDeletes;

    protected $table = "dtickets" ;
    protected $primaryKey = 'dticket_id';

    public function requestor()
    {
        return $this->belongsTo('App\User', 'requestor_id', 'user_id')->withTrashed();
    }

    public function assignee()
    {
        return $this->belongsTo('App\User', 'assignee_id', 'user_id')->withTrashed();
    }
}
