<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Ticket;
use App\DTicket;

class CheckTicketOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = Auth::user()->user_id;
        $dticket = DTicket::find(request('dticket_id'));
        $ticket = Ticket::find(request('ticket_id'));
        if ($dticket) {
            if ($dticket->requestor_id === $user_id) {
                return $next($request);
            } else {
                return json_encode(['is_pass' => false, 'msg' => 'User does not own this ticket']);
            }
        }
        if ($ticket) {
            if ($ticket->requestor_id === $user_id) {
                return $next($request);
            } else {
                return json_encode(['is_pass' => false, 'msg' => 'User does not own this ticket']);
            }
        }
        return json_encode(['is_pass' => false, 'msg' => 'Ticket does not exist']);
    }
}
