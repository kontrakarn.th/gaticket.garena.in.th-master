<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Ticket;
use App\DTicket;
use App\User;

class CheckAssignee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $dticket = DTicket::find(request('dticket_id'));
        $ticket = Ticket::find(request('ticket_id'));
        
        if ($user->role === 'super_admin') {
            if (! $dticket and ! $ticket) {
                return redirect(env('APP_URL').'/admin/dashboard')->with(['message' => 'Ticket does not exist', 'alert' => 'alert-danger']);
            }
            return $next($request);
        } 
        if ($user->role === 'admin') {
            
            if ($dticket) {
                if ($dticket->assignee_id === $user->user_id) {
                    return $next($request);
                } else {
                    return redirect(env('APP_URL').'/admin/domain-tickets')->with(['message' => 'You do not have access to this ticket', 'alert' => 'alert-danger']);
                }
            }
            if ($ticket) {
                if ($ticket->assignee_id === $user->user_id) {
                    return $next($request);
                } else {
                    return redirect(env('APP_URL').'/admin/ga-tickets')->with(['message' => 'You do not have access to this ticket', 'alert' => 'alert-danger']);
                }
            }
        }
        return redirect(env('APP_URL').'/admin/dashboard')->with(['message' => 'Ticket does not exist', 'alert' => 'alert-danger']);
    }
}
