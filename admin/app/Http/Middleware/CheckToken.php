<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;
use \Firebase\JWT\JWT;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = session('Authorization');
        $provided_token = $request->header('Authorization');
        if ($provided_token === null) {
            return json_encode(['is_pass' => false, 'msg' => 'Token not provided']);
        }
        $decoded_token = JWT::decode($provided_token, env('JWT_ENCRYPTION_KEY'), array('HS256'));  
        if ($decoded_token === $token) { 
            return $next($request);
        }
        return json_encode(['is_pass' => false, 'msg' => 'Token mismatched']);
    }
}
