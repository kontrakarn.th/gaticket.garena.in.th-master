<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Session;
use \Illuminate\Support\Facades\Cookie;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            // Session::put('url.intended', '/'.$request->path());
            Cookie::queue(Cookie::forget('redirect'));
            Cookie::queue(Cookie::forget('intended'));
            Cookie::queue(Cookie::make('intended', '/'.$request->path(), 1));
            return url(env('APP_URL'));
        }
    }
}
