<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckDatabaseConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (DB::connection()->getPdo()) {
            return $next($request);
        } else {
            return json_encode(['is_pass' => false, 'msg' => 'Error: Cannot connect to the database']);
        }
    }
}
