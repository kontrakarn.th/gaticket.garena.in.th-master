<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use \Illuminate\Support\Facades\Cookie;

class IsAdminOrSuperAdmin

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role;
        if ($role === "admin" or $role === "super_admin") {
            return $next($request);
        }
        Cookie::queue(Cookie::forget('intended'));
        return redirect(env('APP_URL'));
    }
}
