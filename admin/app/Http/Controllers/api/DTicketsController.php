<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\DTicket;
use App\DBlacklist;
use App\Mail\NewTicket;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\StatusChange;
use App\User;
use Illuminate\Validation\Rule;
use App\Project;


class DTicketsController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function getAllDomainTickets(Request $request) {
        return datatables(DTicket::query()->with(['requestor', 'assignee']))->toJson();
    }

    public function getAllDTicketsOfUser() {
        /*
            Return all dtickets requested by the currently logged-in user

            input: header: Authorization => token value

            return:
                success:
                    json:
                        is_pass => true
                        result  => array of dtickets in json format, each with the following fields:
                            - dticket_id
                            - name
                            - requestor_id
                            - website_url
                            - website_type
                            - region
                            - status
                            - completed_at
                            - updated_at
                            - deleted_at
                fail:
                    is_pass => false
                    msg     => error message
        */
        $user = Auth::user();
        $dtickets = DTicket::where([
            ['requestor_id', $user->user_id],
            ['deleted_at', null]
        ])->orderBy('created_at', 'DESC')->get();
        return json_encode(['is_pass' => true, 'result' => $dtickets]);
    }

    public function createDTicketSendEmail(Request $request) {
        $domain = request('domain_name');
        if (substr($domain, 0, 8) === 'https://') {
            $domain = substr($domain, 8);
        }
        if (substr($domain, 0, 7) === 'http://') {
            $domain = substr($domain, 7);
        }
        if (substr($domain, 0, 4) === 'www.') {
            $domain = substr($domain, 4);
        }

        if (DTicket::where('domain_name', $domain)->first()) {
            return json_encode(['is_pass' => false, 'message' => 'Domain name already exists']);
        }
        if (DBlacklist::where('domain_name', $domain)->first()) {
            return json_encode(['is_pass' => false, 'message' => 'Domain name is blacklisted']);
        }

        request()->validate([
            'domain_name'   => ['required','string', 'regex:/^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+$/', "max:255"],
        ]);
        $new_dticket = new DTicket;
        $user = Auth::user();
        $new_dticket['requestor_id'] = $user->user_id;
        $fields = ['domain_name'];
        if ($user->role === 'admin' or $user->role === 'super_admin') {
            array_push($fields, 'remarks');
            request()->validate([
                'remarks' => ['string', 'max:1000', 'nullable'],
            ]);
        }
        foreach ($fields as $field) {
            if ($field === 'domain_name') {
                $new_dticket['domain_name'] = $domain;
            } else {
                $new_dticket[$field] = request($field);
            }
        }
        $new_dticket->save();

        $new_gcloud = new Project;
        $new_gcloud->project_name = $domain;
        $new_gcloud->dticket_id = $new_dticket->dticket_id;
        $new_gcloud->server_test = 'test'.$domain;
        $new_gcloud->domain_name = $domain;
        $new_gcloud->save();

        //email
        $admins = User::where('role', 'admin')->orWhere('role', 'super_admin')->get();
        $admin_emails = '';
        $counter = 0;
        foreach ($admins as $admin) {
            if ($counter === 0) {
                $first_admin_email = $admin->email;
                $counter++;
            } else {
                $admin_emails .= $admin->email.',';
            }
        }
        $markdown_info = [
            'name'          => $user->name,
            'domain_or_ga'  => 'Domain',
            'url_or_domain' => 'Domain',
            'website_url'   => request('domain_name'),
            'button'        => env('APP_URL').'/admin/domain-tickets/'.$new_dticket->dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $first_admin_email,
            'cc'            => $admin_emails,
            'subject'       => 'Domain Ticket - '.$new_dticket->dticket_id,
            'content'       => (new NewTicket($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return json_encode(['is_pass' => true]);
    }

    public function readDTicket(Request $request) {
        /*
        Retrieve dticket by id

        input: 
            header: - Authorization => token
            query_string = dticket_id    => value
        return:
            success:
                json:
                    is_pass => true
                    result  => json:    - dticket_id
                                        - name
                                        - requestor_id
                                        - website_url
                                        - website_type
                                        - website_region
                                        - status
                                        - created_at
                                        - completed_at
                                        - deleted_at
            fail:
                json:
                    is_pass => false
                    msg => error msg
        */
        request()->validate([
            'dticket_id' => ['required', 'integer', 'exists:dtickets,dticket_id'],
        ]);
        $dticket = DTicket::where([
            ['dticket_id', request('dticket_id')],
            ['deleted_at', null],
        ])->first();
        if (!$dticket) {
            return redirect(env('APP_URL').'/domain_request');
        }
        $dticket['assignee'] = User::find($dticket->assignee_id);
        return json_encode(['is_pass' => true, 'result' => $dticket]);
    }

    public function deleteDTicket(Request $Request) {
        /*
            Delete a dticket

            input: 
                header: Authorization => token
                body:   - dticket_id => dticket_id

            return:
                success:
                    is_pass => true
                fail:
                    is_pass => false
                    msg   => error message
        */
        request()->validate([
            'dticket_id' => ['required', 'integer', 'exists:dtickets,dticket_id'],

        ]);
        $dticket = DTicket::find(request('dticket_id'));
        $dticket->delete();
        return json_encode(['is_pass' => true]);
    }
}