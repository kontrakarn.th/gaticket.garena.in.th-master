<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DTicket;
use App\DComment;
use App\Mail\NewUserComment;
use Illuminate\Support\Facades\Mail;
use App\User;
use Auth;

class DCommentsController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }
    
    public function createDCommentSendEmail(Request $request) {
        /*
            Create a new comment for the currently logged-in user. Users can only comment on their own domain. Admins can comment on any ticket.

            Automatically sends an email to user if admin makes the comments. If a user make a comment then an email is sent to all Admins.

            Inputs are validated. If fail redirect back to previous page with laravel's error variable

            input:
                header: Authorization => token
                body:
                    json:
                            - dticket_id            => value
                            - body                  => value
            
            return:
                success:
                    json:
                        is_pass => true
                fail:
                    json:
                        is_pass => false
                        msg   => error message
        */
        request()->validate([
            'dticket_id' => ['required', 'integer', 'exists:dtickets,dticket_id'],
            'body' => ['required', 'string', 'max:1000'],
        ]);
        $user = Auth::user();
        $user_name = $user->name;
        $role = $user->role;
        $dticket = DTicket::find(request('dticket_id'));

        $fields = ['dticket_id', 'body'];
        $new_dcomment = new DComment;
        $new_dcomment['user_id'] = $user->user_id;
        foreach ($fields as $field) {
            $new_dcomment[$field] = request($field);
        }
        $new_dcomment->save();

        // email
        $all_admins = User::where('role', 'super_admin')->orWhere('user_id', $dticket->assignee_id)->get();
        $admin_emails = '';
        $counter = 0;
        foreach ($all_admins as $admin) {
            if ($counter === 0) {
                $first_admin_email = $admin->email;
                $counter++;
            } else {
                $admin_emails .= $admin->email.',';
            }
        }
        $markdown_info = [
            'name'          => $user_name,
            'domain_or_ga'  => 'Domain',
            'url_or_domain' => 'Domain',
            'website_url'   => $dticket->domain_name,
            'comment_msg'   => request('body'),
            'button'        => env('APP_URL').'/admin/domain-tickets/'.$dticket->dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $first_admin_email,
            'cc'            => $admin_emails,
            'subject'       => 'Domain Ticket - '.$dticket->dticket_id,
            'content'       => (new NewUserComment($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return json_encode(['is_pass' => true]);
    }

    public function getAllCommentsOfDTicket(Request $request) {
        /*
            Return all comments of a dticket

            input:
                header: Authorization => token
                query_string: dticket_id => value

            return:
                success:
                    json:
                        is_pass => true
                        result  => array of jsons of comments with fields:  - comment_id
                                                                            - name
                                                                            - dticket_id
                                                                            - body
                                                                            - created_at
                                                                            - updated_at
                fail:
                    json:
                        is_pass => fail
                        msg => error message
        */
        request()->validate([
            'dticket_id' => ['required', 'integer', 'exists:dtickets,dticket_id']
        ]);
        $dcomments = DComment::where([['deleted_at', null], ['dticket_id', request('dticket_id')]])->orderBy('created_at')->get();
        foreach ($dcomments as $dcomment) {
            $dcomment['name'] = User::find($dcomment->user_id)->name;
        }
        return json_encode(['is_pass' => true, 'result' => $dcomments]);
    }
}