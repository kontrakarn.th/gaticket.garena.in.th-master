<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Auth;

class UsersController extends Controller
{
    // public function checkToken($token) {
    //     $user_id = Auth::id();
    //     $db_token = User::find($user_id)->remember_token;
    //     return ($db_token === $token);
    // }
    public function getCurrentUserInfo() {
        $user = Auth::user();
        return json_encode(['is_pass' => true, 'result' => $user]);
    }

    public function getAllUsers(Request $request) {
        return datatables(User::query()->withTrashed()->with('user_group'))->toJson();
    }
}
