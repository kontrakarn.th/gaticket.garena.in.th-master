<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\User;
use App\Ticket;
use App\Mail\NewTicket;
use App\GABlacklist;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\WebsiteType;
use App\Region;
use \Firebase\JWT\JWT;
use Illuminate\Validation\Rule;
use GuzzleHttp\Client;
use SSP;
use Datatables;

class TicketsController extends Controller
{   
    public function getAllGATickets(Request $request) {
        return datatables(Ticket::query()->with(['requestor', 'assignee']))->toJson();
    }

    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function getAllTicketsOfUser() {
        /*
            Return all tickets requested by the currently logged-in user

            input: header: Authorization => token value

            return:
                success:
                    json:
                        is_pass => true
                        result  => array of tickets in json format, each with the following fields:
                            - ticket_id
                            - name
                            - requestor_email
                            - emails_for_permission
                            - website_url
                            - website_type
                            - region
                            - status
                            - completed_at
                            - created_at
                            - deleted_at
                fail:
                    is_pass => false
                    msg     => error message
        */
        $user = Auth::user();
        $tickets = Ticket::where([
            ['requestor_id', $user->user_id],
            ['deleted_at', null]
        ])->orderBy('created_at', 'DESC')->get();
        foreach ($tickets as $ticket) {
            $ticket['website_type'] = WebsiteType::find($ticket->website_type)->website_type;
            $ticket['website_region'] = Region::find($ticket->website_region)->region;
        }
        return json_encode(['is_pass' => true, 'result' => $tickets]);
    }

    public function createTicketSendEmail(Request $request) {
        /*
            Create a new ticket for the currently logged-in user. Automatically emails all admins.

            Inputs are validated. If fail redirect back to previous page with laravel's error variable

            input:
                header: Authorization
                body:
                    json:
                            - emails_for_permission => value
                            - website_url           => value
                            - website_type          => value
                            - website_region        => value
            
            return:
                success:
                    is_pass => true
                fail:
                    is_pass => false
                    msg   => error message
        */
        $region_arr = [];
        foreach (Region::get()->all() as $region) {
            array_push($region_arr, $region->region);
        }
        $website_type_arr = [];
        foreach (WebsiteType::get()->all() as $website_type) {
            array_push($website_type_arr, $website_type->website_type);
        }

        $url = request('website_url');
        if (substr($url, 0, 8) === 'https://') {
            $url = substr($url, 8);
        }
        if (substr($url, 0, 7) === 'http://') {
            $url = substr($url, 7);
        }
        if (substr($url, 0, 4) === 'www.') {
            $url = substr($url, 4);
        }
        if (Ticket::where('website_url', $url)->first()) {
            return json_encode(['is_pass' => false, 'message' => 'website url exists in DB']);
        }
        if (GABlacklist::where('website_url', $url)->first()) {
            return json_encode(['is_pass' => false, 'message' => 'website url is blacklisted']);
        }


        request()->validate([
            'emails_for_permission' => ['nullable', 'string', 'max:65535'],
            'website_url'           => ['required', 'string', 'regex:/^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+([a-zA-Z0-9&\/#?=\-_%.+:]{0,})$/', 'max:255'],
            'website_region'        => ['required', Rule::in($region_arr)],
            'website_type'          => ['required', Rule::in($website_type_arr)],
        ]);
        $new_ticket = new Ticket;
        $user = Auth::user();
        $new_ticket['requestor_id'] = $user->user_id;

        $fields = ['emails_for_permission', 'website_url'];
        if ($user->role === 'admin' or $user->role === 'super_admin') {
            array_push($fields, 'remarks');
            request()->validate([
                'remarks' => ['string', 'max:1000', 'nullable'],
            ]);
        }

        $new_ticket['website_type'] = WebsiteType::where('website_type', request('website_type'))->first()->id;
        $new_ticket['website_region'] = Region::where('region', request('website_region'))->first()->id;
        foreach ($fields as $field) {
            if ($field === 'website_url') {
                $new_ticket['website_url'] = $url;
            } else {
                $new_ticket[$field] = request($field);
            }
        }
        $new_ticket->save();

        //email
        $admins = User::where('role', 'admin')->orWhere('role', 'super_admin')->get();
        $admin_emails = '';
        $counter = 0;
        foreach ($admins as $admin) {
            if ($counter === 0) {
                $first_admin_email = $admin->email;
                $counter++;
            } else {
                $admin_emails .= $admin->email.',';
            }
        }
        $markdown_info = [
            'name'          => $user->name,
            'domain_or_ga'  => 'GA',
            'url_or_domain' => 'URL',
            'website_url'   => request('website_url'),
            'button'        => env('APP_URL').'/admin/ga-tickets/'.$new_ticket->ticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $first_admin_email,
            'cc'            => $admin_emails,
            'subject'       => 'GA Ticket - '.$new_ticket->ticket_id,
            'content'       => (new NewTicket($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return json_encode(['is_pass' => true]);
    }

    public function readTicket(Request $request) {
        /*
        Retrieve ticket by id

        input: 
            header: - Authorization     => value
            query_string: ticket_id => value
        return:
            success:
                json:
                    is_pass => true
                    result  => json:    - ticket_id
                                        - name
                                        - requestor_id
                                        - emails_for_permission
                                        - website_url
                                        - website_type
                                        - website_region
                                        - status
                                        - created_at
                                        - completed_at
            fail:
                json:
                    is_pass => false
                    msg => error msg
        */
        request()->validate([
            'ticket_id' => ['required', 'integer', 'exists:tickets,ticket_id'],
        ]);
        $ticket = Ticket::where([['ticket_id', request('ticket_id')], ['deleted_at', null],])->first();
        if (!$ticket) {
            return redirect(env('APP_URL').'/ga_request');
        }
        $ticket['website_type'] = WebsiteType::find($ticket->website_type)->website_type;
        $ticket['website_region'] = Region::find($ticket->website_region)->region;
        $ticket['assignee'] = User::find($ticket->assignee_id);
        return json_encode(['is_pass' => true, 'result' => $ticket]);
    }

    public function deleteTicket(Request $Request) {
        /*
            Delete a ticket

            input: 
                header: Authorization => token
                body:
                    json: 
                        - ticket_id => ticket_id

            return:
                success:
                    is_pass => true
                fail:
                    is_pass => false
                    msg   => error message
        */
        request()->validate([
            'ticket_id' => ['required', 'integer', 'exists:tickets,ticket_id'],
        ]);
        $ticket = Ticket::find(request('ticket_id'));
        $ticket->delete();
        return json_encode(['is_pass' => true]);
    }

    public function getWebsiteType() {
        return json_encode(['is_pass' => true, 'result' => WebsiteType::get()]);
    }
    
    public function getRegion() {
        return json_encode(['is_pass' => true, 'result' => Region::get()]);
    }
}
