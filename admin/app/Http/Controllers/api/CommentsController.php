<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Mail\NewUserComment;
use Auth;
use App\Ticket;
use Illuminate\Support\Facades\Mail;
use App\User;

class CommentsController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function createCommentSendEmail(Request $request) {
        /*
            Create a new comment for the currently logged-in user. Users can only comment on their own ticket. Admins can comment on any ticket

            Inputs are validated. If fail redirect back to previous page with laravel's error variable

            Automatically emails the all admins if user makes the comment, or the ticket owner otherwise.
            
            input:
                header: Authorization => token
                body:
                    json:
                            - ticket_id             => value
                            - body                  => value
            
            return:
                success:
                    json:
                        is_pass => true
                fail:
                    json:
                        is_pass => false
                        msg   => error message
        */
        request()->validate([
            'ticket_id' => ['required', 'integer', 'exists:tickets,ticket_id'],
            'body' => ['required', 'string', 'max:1000'],
        ]);
        $user = Auth::user();
        $user_name = $user->name;
        $role = $user->role;
        $ticket = Ticket::find(request('ticket_id'));

        $fields = ['ticket_id', 'body'];
        $new_comment = new Comment;
        $new_comment['user_id'] = $user->user_id;
        foreach ($fields as $field) {
            $new_comment[$field] = request($field);
        }
        $new_comment->save();

        //email
        $all_admins = User::where('role', 'super_admin')->orWhere('user_id', $ticket->assignee_id)->get();
        $admin_emails = '';
        $counter = 0;
        foreach ($all_admins as $admin) {
            if ($counter === 0) {
                $first_admin_email = $admin->email;
                $counter++;
            } else {
                $admin_emails .= $admin->email.',';
            }
        }
        $markdown_info = [
            'name'          => $user_name,
            'domain_or_ga'  => 'GA',
            'url_or_domain' => 'URL',
            'website_url'   => $ticket->website_url,
            'comment_msg'   => request('body'),
            'button'        => env('APP_URL').'/admin/ga-tickets/'.$ticket->ticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $first_admin_email,
            'cc'            => $admin_emails,
            'subject'       => 'GA Ticket - '.$ticket->ticket_id,
            'content'       => (new NewUserComment($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return json_encode(['is_pass' => true]);
    }

    public function getAllCommentsOfTicket(Request $request) {
        /*
            Return all comments of a ticket

            input:
                header: Authorization => token
                query_string: ticket_id => value

            return:
                success:
                    json:
                        is_pass => true
                        result  => array of jsons of comments with fields:  - comment_id
                                                                            - name
                                                                            - ticket_id
                                                                            - body
                                                                            - created_at
                                                                            - updated_at
                fail:
                    json:
                        is_pass => fail
                        msg => error message
        */
        request()->validate([
            'ticket_id' => ['required', 'integer', 'exists:tickets,ticket_id'],
        ]);
        $comments = Comment::where([['deleted_at', null], ['ticket_id', request('ticket_id')]])->orderBy('created_at')->get();
        foreach ($comments as $comment) {
            $comment['name'] = User::find($comment->user_id)->name;
        }
        return json_encode([
            'is_pass'   => true,
            'result'    => $comments
        ]);
    }
}
