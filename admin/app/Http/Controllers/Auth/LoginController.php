<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use \Firebase\JWT\JWT;
use \Illuminate\Support\Facades\Cookie;


class LoginController extends Controller
{
    //

    use Authenticatable;
    public function __construct()
    {
        
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
            $finduser = User::where([
                ['email', $user->email],
                ['deleted_at', null],
            ])->first();

            if($finduser){
                // update name and avatar
                $finduser->avatar = $user->avatar;
                $finduser->save();
                Auth::login($finduser);

                //token
                $token = $user->token;
                session(['Authorization' => $token]);
                $key = env('JWT_ENCRYPTION_KEY');
                $jwt = JWT::encode($token, $key);
                //redirect
                $role = $finduser->role;
                $url = '';
                if (Cookie::has('redirect')) {
                    $url = Cookie::get('redirect');
                } else if (Cookie::has('intended')) {
                    $url = Cookie::get('intended');
                }
                if ($role === 'admin' or $role === 'super_admin') {
                    if ($url) {
                        if (substr($url, 12, 5) === 'admin') {
                            $url = substr($url, 39);
                        }
                        return redirect(env('APP_URL').$url.'?token='.$jwt);
                    } else {
                        return redirect(env('APP_URL').'/admin/dashboard');
                    }
                } else if ($url) {
                    return redirect(env('APP_URL').$url.'?token='.$jwt);
                } else {
                    return redirect(env('APP_URL').'/ga_request?token='.$jwt);
                }
            } else {
                    return redirect(env('APP_URL'))->with(['message' => 'Sorry, you are not registered. Please contact IT dev.', 'alert' => 'alert-danger']);
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect(env('APP_URL').'/api/auth/google');
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        Cookie::queue(Cookie::forget('intended'));
        Cookie::queue(Cookie::forget('redirect'));
        $request->session()->flush();
        return redirect(env('APP_URL'));
    }
}
