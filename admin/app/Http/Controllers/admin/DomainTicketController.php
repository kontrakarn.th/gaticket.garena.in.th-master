<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\User;
use App\DTicket;
use App\DComment;
use App\UserGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Mail\StatusChange;
use App\Mail\NewComment;
use App\Mail\AssignAdmin;
use App\Mail\SelfAssign;
use App\Project;
use Config;

use Illuminate\Support\Facades\URL;

class DomainTicketController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function getAllDomainTickets() {
        $user = Auth::user();

        $table_columns = [
            'Requestor',
            'Requestor Email',
            'Assignee',
            'Assignee Email',
            'Domain Name',
            'Status',
            'Created At'
        ];

        return view('/dticket/dtickets', ['table_columns' => $table_columns, 'user' => Auth::user()]);
    }

    public function updateDomainTicketStatusSendEmail(Request $request, $dticket_id) {
        /*
            Updates ticket data. If status is closed, completed_at column of request is updated to the current time.
            Validate inputs. If failed, redirect to previous page with laravel's errors variable
            Automatically email the ticket's owner.

            inputs:
                    - ticket_id
                    - status
        */
        request()->validate([
            'status'  => ['required', 'string', Rule::in(['Open', 'Closed', 'Processing', 'Rejected'])],
        ]);
        $new_status = request('status');
        $dticket = DTicket::find($dticket_id);

        if ($new_status === "Rejected") {
            $request->request->add(['body' => request('reject_msg')]);
            $this->createDomainCommentSendEmail($request, $dticket_id);
            $gcloud_project = Project::where('dticket_id', $dticket->dticket_id)->first();
            if ($gcloud_project) {
                $gcloud_project->delete();
            }
        }

        $dticket->status = $new_status;
        if ($new_status === 'Closed') {
            $dticket->completed_at = Carbon::now();
        }
        $dticket_user = User::withTrashed()->find($dticket->requestor_id);
        $dticket->save();

        //email
        $markdown_info = [
            'name'          => $dticket_user->name,
            'admin_name'    => Auth::user()->name,
            'url_or_domain' => 'Domain',
            'domain_or_ga'  => 'Domain',
            'website_url'   => $dticket->domain_name,
            'status'        => $new_status,
            'button'        => env('APP_URL').'/domain_request/'.$dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $dticket_user->email,
            'cc'            => '',
            'subject'       => 'Domain Ticket - '.$dticket_id,
            'content'       => (new StatusChange($markdown_info))->render(),
            'layout'        => 'custom'
        ];

        $this->sendEmail($info);
        if ($new_status === 'Processing') {
            return redirect(env('APP_URL').'/admin/domain-tickets/'.$dticket_id)->with(['message' => "Ticket status changed to Processing.", "alert" => "alert-warning"]);
        } else if ($new_status === 'Rejected') {
            return redirect(env('APP_URL').'/admin/domain-tickets/'.$dticket_id)->with(['message' => "Ticket status changed to Rejected.", "alert" => "alert-success"]);
        } else if ($new_status === 'Closed') {
            return redirect(env('APP_URL').'/admin/domain-tickets/'.$dticket_id)->with(['message' => "Ticket status changed to Closed! The ticket-owner has been notified.", "alert" => "alert-success"]);
        }
    }

    public function viewDomainTicket($dticket_id) {
        /*
        Retrieve domain ticket by id

        input: 
            Route Wildcard: {dticket_id}
        */
        $dticket = DTicket::where([['dticket_id', $dticket_id], ['deleted_at', null]])->first();
        if ($dticket === null) {
            return redirect(env('APP_URL').'/admin/domain-tickets')->with(['message' => "The domain ticket you are trying to access does not exist.", "alert" => "alert-warning"]);
        }

        $dcomments = DComment::where('dticket_id', $dticket_id)->orderBy('created_at')->get();
        foreach ($dcomments as $dcomment) {
            $user = User::withTrashed()->find($dcomment->user_id);
            $dcomment_avatar = $user->avatar;
            if ($dcomment_avatar == null) {
                $dcomment->avatar = Config::get('app.url') . '/img/default_avatar3.png';
            } else {
                $dcomment->avatar = $dcomment_avatar;
            }
            $dcomment->name = $user->name;
            $dcomment->created_at = $dcomment->created_at->addHours(7);
        }
        $requestor = User::withTrashed()->find($dticket->requestor_id);
        $avatar = $requestor->avatar;
        $dticket["requestor_email"] = $requestor->email;
        $dticket["name"] = $requestor->name;
        $dticket['created_at'] =  $dticket['created_at']->addHours(7);
        $group = UserGroup::find($requestor->group);
        $group_name = null;
        if ($group !== null) {
            $group_name = $group->user_group;
        } else if ($requestor->role === 'admin') {
            $group_name = 'Admin';
        } else if ($requestor->role === 'super_admin') {
            $group_name = 'Super Admin';
        }

        $admins = User::where('role', 'admin')->orWhere('role', 'super_admin')->get();
        if ($dticket->assignee_id) {
            $assignee = User::find($dticket->assignee_id);
            if ($assignee->role === 'admin') {
                $assignee->group = 'Admin';
            } else if ($assignee->role === 'super_admin') {
                $assignee->group = 'Super Admin';
            }
        } else {
            $assignee = null;
        }

        return view('/dticket/view_dticket', [
            'dticket'    => $dticket,
            'dcomments'  => $dcomments,
            'user'      => Auth::user(),
            'avatar'    => $avatar,
            'group'     => $group_name,
            'admins'    => $admins,
            'assignee'  => $assignee,
        ]);
    }

    public function createDomainCommentSendEmail(Request $request, $dticket_id) {
        /*
            Create a new comment for the currently logged-in user. Users can only comment on their own ticket. Admins can comment on any ticket

            Inputs are validated. If fail redirect back to previous page with laravel's error variable

            Automatically emails the all admins if user makes the comment, or the ticket owner otherwise.
            
            input:
                    - dticket_id    => value
                    - body          => value
        */
        request()->validate(['body' => ['required', 'string', 'max:1000']]);
        
        $user = Auth::user();
        $user_name = $user->name;
        $dticket = DTicket::find($dticket_id);
        $new_dcomment = new DComment;
        $new_dcomment['user_id'] = $user->user_id;
        $new_dcomment['dticket_id'] = $dticket_id;
        $new_dcomment['body'] = request('body');
        $new_dcomment->save();
        $dticket_user = User::withTrashed()->find($dticket->requestor_id);

        //email
        $markdown_info = [
            'name'          => $dticket_user->name,
            'admin_name'    => $user->name,
            'domain_or_ga'  => 'Domain',
            'comment_msg'   => request('body'),
            'website_url'   => $dticket->domain_name,
            'url_or_domain' => 'Domain',
            'button'        => env('APP_URL').'/domain_request/'.$dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $dticket_user->email,
            'cc'            => '',
            'subject'       => 'Domain Ticket - '.$dticket_id,
            'content'       => (new NewComment($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return redirect(env('APP_URL')."/admin/domain-tickets/".$dticket_id)->with(['message' => "Comment Created", "alert" => "alert-secondary"]);
    }

    public function assignAdmin(Request $request, $dticket_id) {
        request()->validate([
            'assignee_id'   => ['required', 'integer', 'exists:users,user_id'],
        ]);
        $user = Auth::user();
        if ($user->role == 'admin') {
            request()->validate([
                'assignee_id'   => [Rule::in([$user->user_id])],
            ]);
        }
        $dticket = DTicket::find($dticket_id);
        $dticket->assignee_id = request('assignee_id');
        $dticket->save();
        $assignee = User::find(request('assignee_id'));

        //email
        $markdown_info = [
            'name'          => $assignee->name,
            'admin_name'    => Auth::user()->name,
            'domain_or_ga'  => 'domain',
            'url_or_domain' => 'Domain',
            'website_url'   => $dticket->domain_name,
            'button'        => env('APP_URL').'/admin/ga-tickets/'.$dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $assignee->email,
            'cc'            => '',
            'subject'       => 'Domain Ticket - '.$dticket_id,
            'content'       => (new AssignAdmin($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $request->request->add(['status' => 'Open']);
        $this->updateDomainTicketStatusSendEmail($request, $dticket_id);
        if ($user->role === 'super_admin' and request('assignee_id') === $user->user_id) {
            return redirect(env('APP_URL')."/admin/domain-tickets/".$dticket_id)->with(['message' => 'Ticket Accepted!', "alert" => "alert-success"]);
        }
        if ($user->role === 'admin') {
            $super_admins = User::where('role', 'super_admin')->get();
            $counter = 0;
            $emails = '';
            foreach ($super_admins as $super_admin) {
                if ($counter === 0) {
                    $first_email = $super_admin->email;
                    $counter++;
                } else {
                    $emails .= $super_admin->email.',';
                }
            }
            $markdown_info2 = [
                'admin_name'    => Auth::user()->name,
                'domain_or_ga'  => 'domain',
                'url_or_domain' => 'Domain',
                'website_url'   => $dticket->domain_name,
                'button'        => env('APP_URL').'/admin/ga-tickets/'.$dticket_id,
            ];
            $info2 = [
                'from'          => 'GaTicket Garena',
                'from_mail'	    => 'no-reply@garena.co.th',
                'to'            => $first_email,
                'cc'            => $emails,
                'subject'       => 'Domain Ticket - '.$dticket_id,
                'content'       => (new SelfAssign($markdown_info2))->render(),
                'layout'        => 'custom'
            ];
            $this->sendEmail($info2);
            return redirect(env('APP_URL')."/admin/domain-tickets/".$dticket_id)->with(['message' => 'Ticket Accepted!', "alert" => "alert-success"]);
        }
        $this->sendEmail($info);
        return redirect(env('APP_URL')."/admin/domain-tickets/".$dticket_id)->with(['message' => 'Ticket assigned! Admin '.$assignee->name.' has been notified.', "alert" => "alert-success"]);
    }
}
