<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Mail\Test;
use \Firebase\JWT\JWT;

class HomeController extends Controller
{
    function showDashboard(Request $request) {
        $user = Auth::User();
        return view('/home/dashboard', ["user"=>$user]);
    }

    function redirectToUserPage(Request $request) {
        $key = env("JWT_ENCRYPTION_KEY");
        $jwt = JWT::encode(session("Authorization"), $key);
        return redirect(env('APP_URL')."/ga_request?token=".$jwt);
    }
}
