<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Project;
use App\Region;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Alert;
use Auth;
use Illuminate\Validation\Rule;
use App\DTicket;
use App\DBlacklist;
use App\User;
use App\Mail\NewTicket;


class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function index()
    {
        $regions = Region::get();
        $projects = Project::get()->all();
        foreach ($projects as $project) {
            if ($project->expired_ssl) {
                $project->expired_ssl = Carbon::parse($project->expired_ssl);
            }
            if ($project->live_date) {
                $project->live_date = Carbon::parse($project->live_date);
            }
            if ($project->end_date) {
                $project->end_date = Carbon::parse($project->end_date);
            }
        }

        return view('gcloud',[
            'regions'   => $regions,
            'projects'  => $projects,
            'user'      => Auth::user(),
        ]);
    }

    public function index2(Request $request) {
        return datatables(Project::query()->with('getRegion'))->toJson();
    }


    public function sendApi(Request $request)
    {   

        request()->validate([
            'project_name'  => ['required', 'string', 'regex:/^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+$/'],
            'region'        => ['required', 'exists:regions,id', 'integer'],
            'ip_server_test'=> ['nullable', 'ip'],
            'status_domain' => ['required', Rule::in(['done', 'not_done'])],
            'expired_ssl'   => ['required', 'date'],
            'status_ssl'    => ['required', Rule::in(['done', 'not_done'])],
            'live_date'     => ['required', 'date'],
            'end_date'      => ['required', 'date'],
            'db'            => ['required', Rule::in(['our_server', 'gcloud'])],
            'remarks'       => ['nullable', 'string', 'max:1000'],
        ]);
        $project_name = request('project_name');
        if (substr($project_name, 0, 8) === 'https://') {
            $project_name = substr($project_name, 8);
        }
        if (substr($project_name, 0, 7) === 'http://') {
            $project_name = substr($project_name, 7);
        }
        if (substr($project_name, 0, 4) === 'www.') {
            $project_name = substr($project_name, 4);
        }
        if (DTicket::where('domain_name', $project_name)->first() or Project::where('project_name', $project_name)->first()) {
            return response()->json([
                'status'    => false,
                'message'   => 'Domain name already exists'
            ]);
        }

        if (DBlacklist::where('domain_name', $project_name)->first()) {
            return response()->json([
                'status'    => false,
                'message'   => 'This domain is blacklisted',
            ]);
        }
        
        
        $new_dticket = new DTicket;
        $new_dticket->requestor_id = Auth::user()->user_id;
        $new_dticket->domain_name = $project_name;
        if (request('remarks')) {
            $new_dticket->remarks = request('remarks');
        }
        try {
            $new_dticket->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
        $project = new Project();
        $project->dticket_id = $new_dticket->dticket_id;
        $project->project_name = $project_name ;
        $project->server_test = 'test.'.$project_name ;
        $project->domain_name = $project_name ;
        $project->region = $request->region ;
        $project->ip_server_test = $request->ip_server_test ;
        $project->status_domain = $request->status_domain === "done" ? 1 : 0 ;
        $project->expired_ssl = Carbon::parse($request->expired_ssl) ;
        $project->status_ssl = $request->status_ssl === "done" ? 1 : 0 ;
        $project->live_date = Carbon::parse($request->live_date) ;
        $project->end_date = Carbon::parse($request->end_date) ;
        $project->db = $request->db ;
        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        $admins = User::where('role', 'admin')->orWhere('role', 'super_admin')->get();
        $admin_emails = '';
        $counter = 0;
        foreach ($admins as $admin) {
            if ($counter === 0) {
                $first_admin_email = $admin->email;
                $counter++;
            } else {
                $admin_emails .= $admin->email.',';
            }
        }
        $markdown_info = [
            'name'          => Auth::user()->name,
            'domain_or_ga'  => 'Domain',
            'url_or_domain' => 'Domain',
            'website_url'   => $project->domain_name,
            'button'        => env('APP_URL').'/admin/domain-tickets/'.$new_dticket->dticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $first_admin_email,
            'cc'            => $admin_emails,
            'subject'       => 'Domain Ticket - '.$new_dticket->dticket_id,
            'content'       => (new NewTicket($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);

        if ($project->expired_ssl < Carbon::now()) {
            $expired_ssl_html = '<td><div id="dataExpiredSSL'.$project->id.'" data-toggle="modal" data-target="#editExpiredSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" style="background-color: \'#F44336\'; cursor:pointer">'.Carbon::parse($request->expired_ssl)->format("d M y").'</div></td>';
        } else {
            $expired_ssl_html = '<td><div id="dataExpiredSSL'.$project->id.'" data-toggle="modal" data-target="#editExpiredSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor:pointer">'.Carbon::parse($request->expired_ssl)->format("d M y").'</div></td>';
        }

        if ($project->status_ssl === 1) {
            $status_ssl_background = '#4CAF50';
            $status_ssl_class = "bg-success";
            $status_ssl_value = "Done";
        } else {
            $status_ssl_background = '\'#F44336\'';
            $status_ssl_class = "bg-danger";
            $status_ssl_value = "Not Done";
        }
        $status_ssl_html = '<td><div id="dataStatusSSL'.$project->id.'" data-toggle="modal" data-target="#editStatusSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcefz\'" onMouseOut="this.style.backgroundColor=\''.$status_ssl_background.'\'" class=\''.$status_ssl_class.'\' style="cursor:pointer">'.$status_ssl_value.'</div></td>';

        if ($project->status_domain === 1) {
            $status_domain_background = '#4CAF50';
            $status_domain_class = 'bg-success';
            $status_domain_value = 'Done';
        } else {
            $status_domain_background = '\'#F44336\'';
            $status_domain_class = 'bg-danger';
            $status_domain_value = 'Not Done';
        }
        $status_domain_html = '<td><div id="statusDomain'.$project->id.'" data-toggle="modal" data-target="#editStatusDomain'.$project->id.'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\''.$status_domain_background.'\'" class="'.$status_domain_class.'" style="cursor:pointer">'.$status_domain_value.'</div></td>';

        if ($project->db === 'our_server') {
            $db_mouseout = 'rgb(223,157,155)';
            $db_background = 'rgb(223,157,155)';
            $db_value = 'OUR SERVER';
        } else {
            $db_mouseout = 'rgb(177,168,211)';
            $db_background = 'rgb(177,168,211)';
            $db_value = 'GCLOUD';
        }
        $live_date_html = Carbon::parse($request->live_date)->format("d M y");
        $end_date_html = Carbon::parse($request->end_date)->format("d M y");
        if ($project->region === '1') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#cbe2b0\'" style="background-color: #cbe2b0; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '2') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f1d1d1\'" style="background-color: #f1d1d1; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '3') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f3e1e1\'" style="background-color: #f3e1e1; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '4') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#faf2f2\'" style="background-color: #faf2f2; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '5') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f6d186\'" style="background-color: #f6d186; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '6') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f19292\'" style="background-color: #f19292; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '7') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ccffff\'" style="background-color: #ccffff; cursor:pointer" >'.$project->getRegion->region.'</div></td>';
        }

        $domain_name_html = env('APP_URL')."/admin/domain-tickets/".$project->dticket_id;

        $new_project_html = <<<HTML
<tr>
    <td>
        <div id="project$project->id" onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'"  data-toggle="modal" data-target="#editProjectName$project->id" id="projectName$project->id">
            <a href="https://$project->project_name" target="_blank">$project->project_name</a>
        </div>
    </td>
    <td>
        <div onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'" id="serverTest$project->id">
            <a href="http://$project->server_test" target="_blank" >$project->server_test</a>
        </div>
    </td>
    <td>
        <div onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'" id="domainName$project->id">
            <a href="$domain_name_html">$project->domain_name</a>
        </div>
    </td>
    $region_html
    <td>
        <div id="ipServerTest$project->id" onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'" >$project->ip_server_test</div>
    </td>
    $status_domain_html
    $expired_ssl_html
    $status_ssl_html
    <td>
        <div id="dataLiveDate$project->id" data-toggle="modal" data-target="#editLiveDate$project->id" onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'" style="cursor:pointer">$live_date_html</div>
    </td>
    <td>
        <div id="dataEndDate$project->id" data-toggle="modal" data-target="#editEndDate$project->id" onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='#ffffff'" style="cursor:pointer">$end_date_html</div>
    </td>
    <td>
        <div id="dataDB$project->id" data-toggle="modal" data-target="#editServerDB$project->id" onmouseover="this.style.backgroundColor='#a6dcef'" onMouseOut="this.style.backgroundColor='$db_mouseout'" style="background-color: $db_background; cursor:pointer">$db_value</div>
    </td>
    <td>
        <form onSubmit="deleteProject($project->id, event)">
            <button type="submit" id="delete_project_button$project->id" class="btn-sm btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
        </form>
    </td>
</tr>
HTML;

        $regions = Region::get()->all();
        $modal_regions_html = '';
        foreach ($regions as $region) {
            if ($project->region == $region->id) {
                if ($region->ip_test) {
                    $modal_regions_html .= '<option value="'.$region->id.'" data-id="'.$region->ip_test.'" selected>'.$region->region.'</option>';
                } else {
                    $modal_regions_html .= '<option value="'.$region->id.'" data-id="" selected>'.$region->region.'</option>';
                }
            } else {
                if ($region->ip_test) {
                    $modal_regions_html .= '<option value="'.$region->id.'" data-id="'.$region->ip_test.'" "">'.$region->region.'</option>';
                } else {
                    $modal_regions_html .= '<option value="'.$region->id.'" data-id="" "">'.$region->region.'</option>';
                }
            }
        }
        
        if ($project->status_domain === 1) {
            $modal_status_domain_html = '<option value="done" selected>Done</option>';
            $modal_status_domain_html .= '<option value="not_done" "">Not Done</option>';
        } else {
            $modal_status_domain_html = '<option value="done" "">Done</option>';
            $modal_status_domain_html .= '<option value="not_done" selected>Not Done</option>';
        }

        if ($project->status_ssl === 1) {
            $modal_status_ssl_html = '<option value="done" selected>Done</option>';
            $modal_status_ssl_html .= '<option value="not_done" "">Not Done</option>';
        } else {
            $modal_status_ssl_html = '<option value="done" "">Done</option>';
            $modal_status_ssl_html .= '<option value="not_done" selected>Not Done</option>';
        }

        if ($project->db === 'our_server') {
            $modal_db_html = '<option value="our_server" selected>OUR SERVER</option>';
            $modal_db_html .= '<option value="gcloud" "">GCLOUD</option>';
        } else {
            $modal_db_html = '<option value="our_server" "">OUR SERVER</option>';
            $modal_db_html .= '<option value="gcloud" selected>GCLOUD</option>';
        }
        $modal_expired_ssl_html = Carbon::parse($project->expired_ssl)->format('Y-m-d');
        $live_date_html = Carbon::parse($request->live_date)->format("Y-m-d");
        $end_date_html = Carbon::parse($request->end_date)->format("Y-m-d");

        $modal_html = <<<HTML
<div class="modal fade" id="editRegion$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="needs-validation" onSubmit="saveEditRegion($project->id, event)" novalidate>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Region</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="region$project->id" >Region</label>
                    <select name="region$project->id" id="region$project->id" class="form-control" required>
                        <option value="" disabled>- Choose Region -</option>
                        $modal_regions_html
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="edit_region_button$project->id" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editStatusDomain$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form class="needs-validation" onSubmit="saveEditStatusDomain($project->id, event)" novalidate>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Status Domain</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="status_domain$project->id" >Status Domain</label>
                <select name="status_domain$project->id" id="status_domain$project->id" class="form-control" required>
                    <option value="" disabled selected>- Choose Status Domain -</option>
                    $modal_status_domain_html
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_status_domain_button$project->id" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </form>
    </div>
    </div>

    <div class="modal fade" id="editExpiredSSL$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Expired SSL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="expired_ssl$project->id" >Expired SSL</label>
                <input type="date" class="form-control" name="expired_ssl$project->id" id="expired_ssl$project->id" value="$modal_expired_ssl_html">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_expired_ssl_button$project->id" type="button" class="btn btn-primary" onclick="saveEditExpiredSSL($project->id, event)">Save changes</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="editStatusSSL$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Status SSL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="status_ssl$project->id" >Status SSL</label>
                <select name="status_ssl$project->id" id="status_ssl$project->id" class="form-control">
                    <option disabled>- Choose Status SSL -</option>
                    $modal_status_ssl_html
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_status_ssl_button$project->id" type="button" class="btn btn-primary" onclick="saveEditStatusSSL($project->id, event)">Save changes</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="editLiveDate$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Live Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="live_date$project->id" >Live Date</label>
                <input type="date" class="form-control" name="live_date$project->id" id="live_date$project->id" value="$live_date_html">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_live_date_button$project->id" type="button" class="btn btn-primary" onclick="saveEditLiveDate($project->id, event)">Save changes</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="editEndDate$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit End Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="end_date$project->id" >End Date</label>
                <input type="date" class="form-control" name="end_date$project->id" id="end_date$project->id" value="$end_date_html">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_end_date_button$project->id" type="button" class="btn btn-primary" onclick="saveEditEndDate($project->id, event)">Save changes</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="editServerDB$project->id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Server DB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="DB$project->id" >DB</label>
                <select name="DB$project->id" id="DB$project->id" class="form-control" required>
                    <option value="" disabled>- Choose DB -</option>
                    $modal_db_html
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="edit_db_button$project->id" type="button" class="btn btn-primary" onclick="saveEditServerDB($project->id, event)">Save changes</button>
            </div>
        </div>
    </div>
</div>
HTML;
        
        $form_region_html = '';
        foreach ($regions as $region) {
            $form_region_html .= '<option value="'.$region->id.'" data-id="'.$region->ip_test.'">'.$region->region.'</option>';
        }
        $form = <<<HTML
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body" id="form-modal-body">
        <div class="form-group">
        <label for="project_name">Project Name</label>
        <input type="text" class="form-control" name="project_name" id="project_name" required pattern="^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+$">
        <div class="invalid-feedback">
            Invalid Domain
        </div>
        </div>
        <div class="form-group">
        <label for="region" >Region</label>
        <select name="region" id="region" class="form-control" required>
            <option value="" disabled selected>- Choose Region -</option>
            $form_region_html
        </select>
        <div class="invalid-feedback">
            Required
        </div>
        </div>
        <div class="form-group">
        <label for="ip_server_test" >IP Server test</label>
        <input type="text" class="form-control" name="ip_server_test" id="ip_server_test" pattern="^((\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$" nullable>
        <div class="invalid-feedback">
            Invalid IP Address
        </div>
        </div>

        <div class="form-group">
        <label for="status_domain" >Status Domain</label>
        <select name="status_domain" id="status_domain" class="form-control" required>
            <option value="" disabled selected>- Choose Status Domain -</option>
            <option value="done">Done</option>
            <option value="not_done">Not Done</option>
        </select>
        <div class="invalid-feedback">
            Required
        </div>
        </div>

        <div class="form-group">
        <label for="expired_ssl" >Expired SSL</label>
        <input type="date" class="form-control" name="expired_ssl" id="expired_ssl" required>
        <div class="invalid-feedback">
            Required
        </div>
        </div>

        <div class="form-group">
        <label for="status_ssl" >Status SSL</label>
        <select name="status_ssl" id="status_ssl" class="form-control" required>
            <option value="" disabled selected>- Choose Status SSL -</option>
            <option value="done">Done</option>
            <option value="not_done">Not Done</option>
        </select>
        <div class="invalid-feedback">
            Required
        </div>
        </div>

        <div class="form-group">
        <label for="live_date" >Live Date</label>
        <input type="date" class="form-control" name="live_date" id="live_date" required>
        <div class="invalid-feedback">
            Required
        </div>
        </div>

        <div class="form-group">
        <label for="end_date" >End Date</label>
        <input type="date" class="form-control" name="end_date" id="end_date" required>
        <div class="invalid-feedback">
            Required
        </div>
        </div>

        <div class="form-group">
            <label for="db" >DB</label>
            <select name="db" id="db" class="form-control" required>
                <option value="" disabled selected>- Choose DB -</option>
                <option value="our_server">OUR SERVER</option>
                <option value="gcloud">GCLOUD</option>
            </select>
            <div class="invalid-feedback">
                Required
            </div>
        </div>
        <div class="form-group" id="remark_form_group">
            <label for="remarks">Remarks</label>
            <textarea maxlength="10000"style="resize: none;height:150px" class="form-control" name="remarks" id="remarks" cols="30" rows="10"></textarea>
        </div>
        <p class="text-danger" style="margin-left:10px">Note: Domain ticket will be created automatically</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="addProject_button" type="submit" class="btn btn-primary">Save changes</button>
    </div>
</div>
HTML;
        return response()->json([
            'status'            => true,
            'message'           => "Success. Domain Ticket has been created.",
            'new_project_html'  => $new_project_html,
            'modal_html'        => $modal_html,
            'form'              => $form,
        ]);
    }

    public function deleteProject(Request $request) {
        request()->validate([
            'idProject' => ['required', 'exists:projects,id'],
        ]);
        $project = Project::find(request('idProject'));
        $project->delete();

        return response()->json([
            'status' => true,
            'message'=> "success",
        ]);
    }

    public function editRegion(Request $request)
    {
        request()->validate([
            'idProject'     => ['required', 'integer', 'exists:projects,id'],
            'newRegion'     => ['required', 'exists:regions,id'],
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $region = Region::where('id',$request->newRegion)->first();

        $project->region = $request->newRegion ;
        $project->ip_server_test = $region->ip_test ;

        if ($project->region === '1') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#cbe2b0\'" style="background-color: #cbe2b0" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '2') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f1d1d1\'" style="background-color: #f1d1d1" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '3') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f3e1e1\'" style="background-color: #f3e1e1" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '4') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#faf2f2\'" style="background-color: #faf2f2" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '5') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f6d186\'" style="background-color: #f6d186" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '6') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#f19292\'" style="background-color: #f19292" >'.$project->getRegion->region.'</div></td>';
        } else if ($project->region === '7') {
            $region_html = '<td><div id="dataRegion'.$project->id.'" data-toggle="modal" data-target="#editRegion'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ccffff\'" style="background-color: #ccffff" >'.$project->getRegion->region.'</div></td>';
        }

        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "success",
            'region_html' => $region_html,
            'ip_test' => $project->getRegion->ip_test,
        ]);
    }

    public function editStatusDomain(Request $request)
    {
        request()->validate([
            'idProject'         =>  ['required', 'integer', 'exists:projects,id'],
            'newStatusDomain'   =>  ['required', 'string', Rule::in(['done', 'not_done'])],
        ]);

        $project = Project::where('id',$request->idProject)->first();
        $project->status_domain = $request->newStatusDomain === "done" ? 1 : 0 ;
        
        if ($project->status_domain === 1) {
            $status_domain_html = '<td><div id="statusDomain'.$project->id.'" data-toggle="modal" data-target="#editStatusDomain'.$project->id.'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#4CAF50\'" class="bg-success" style="cursor:pointer">Done</div></div>';
        } else {
            $status_domain_html = '<td><div id="statusDomain'.$project->id.'" data-toggle="modal" data-target="#editStatusDomain'.$project->id.'"  onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" class="bg-danger" style="cursor:pointer">Not Done</div></div>';
        }


        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'                => true,
            'message'               => "success",
            'status_domain_html'    => $status_domain_html,
        ]);
    }

    public function editExpireDateSSL(Request $request)
    {
        request()->validate([
            'idProject'         => ['required', 'integer', 'exists:projects,id'],
            'newExpiredDateSSL' => ['required', 'date'],
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $project->expired_ssl = Carbon::parse($request->newExpiredDateSSL) ;

        if ($project->expired_ssl < Carbon::now()) {
            $expired_ssl_html = '<td><div id="dataExpiredSSL'.$project->id.'" data-toggle="modal" data-target="#editExpiredSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#F44336\'" style="background-color: #F44336; cursor:pointer">'.Carbon::parse($request->newExpiredDateSSL)->format("d M y").'</div></td>';
        } else {
            $expired_ssl_html = '<td><div id="dataExpiredSSL'.$project->id.'" data-toggle="modal" data-target="#editExpiredSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="background-color: #ffffff; cursor:pointer">'.Carbon::parse($request->newExpiredDateSSL)->format("d M y").'</div></td>';
        }

        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'            => true,
            'message'           => "success",
            'expired_ssl_html'  => $expired_ssl_html,
        ]);
    }

    public function editStatusSSL(Request $request)
    {
        request()->validate([
            'idProject'         => ['required', 'integer', 'exists:projects,id'],
            'newStatusSSL'      => ['required', 'string', Rule::in(['done', 'not_done'])],
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $project->status_ssl = $request->newStatusSSL == "done" ? 1 : 0 ;

        if ($project->status_ssl === 1) {
            $status_ssl_background = '#4CAF50';
            $status_ssl_class = "bg-success";
            $status_ssl_value = "Done";
        } else {
            $status_ssl_background = '\'#F44336\'';
            $status_ssl_class = "bg-danger";
            $status_ssl_value = "Not Done";
        }
        $status_ssl_html = '<td><div id="dataStatusSSL'.$project->id.'" data-toggle="modal" data-target="#editStatusSSL'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcefz\'" onMouseOut="this.style.backgroundColor=\''.$status_ssl_background.'\'" class=\''.$status_ssl_class.'\' style="cursor:pointer">'.$status_ssl_value.'</div></td>';


        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'            => true,
            'message'           => "success",
            'status_ssl_html'   => $status_ssl_html,
        ]);
    }

    public function editLiveDate(Request $request)
    {
        request()->validate([
            'idProject'         => ['required', 'integer', 'exists:projects,id'],
            'newLiveDate'       => ['required', 'date'],
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $project->live_date = Carbon::parse($request->newLiveDate) ;

        $live_date_html = '<td><div id="dataLiveDate'.$project->id.'" data-toggle="modal" data-target="#editLiveDate'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor:pointer">'.Carbon::parse($project->live_date)->format("d M y").'</div></td>';

        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'            => true,
            'message'           => "success",
            'live_date_html'    => $live_date_html,
        ]);
    }


    public function editEndDate(Request $request)
    {
        request()->validate([
            'idProject'     => ['required', 'integer', 'exists:projects,id'],
            'newEndDate'    => ['required', 'date'],
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $project->end_date = Carbon::parse($request->newEndDate) ;

        $end_date_html = '<td><div id="dataEndDate'.$project->id.'" data-toggle="modal" data-target="#editEndDate'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\'#ffffff\'" style="cursor:pointer">'.Carbon::parse($project->end_date)->format("d M y").'</div></td>';

        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'        => true,
            'message'       => "success",
            'end_date_html' => $end_date_html,
        ]);
    }


    public function editServerDB(Request $request)
    {
        request()->validate([
            'idProject' => ['required', 'integer', 'exists:projects,id'],
            'newServerDB'   => ['required', 'string', Rule::in(['our_server', 'gcloud'])]
        ]);
        $project = Project::where('id',$request->idProject)->first();
        $project->db = $request->newServerDB ;

        if ($project->db === 'our_server') {
            $db_mouseout = 'rgb(223,157,155)';
            $db_background = 'rgb(223,157,155)';
            $db_value = 'OUR SERVER';
        } else {
            $db_mouseout = 'rgb(177,168,211)';
            $db_background = 'rgb(177,168,211)';
            $db_value = 'GCLOUD';
        }
        $db_html = '<td><div id="dataDB'.$project->id.'" data-toggle="modal" data-target="#editServerDB'.$project->id.'" onmouseover="this.style.backgroundColor=\'#a6dcef\'" onMouseOut="this.style.backgroundColor=\''.$db_mouseout.'\'" style="background-color: '.$db_background.'; cursor:pointer">'.$db_value.'</div></td>';

        try {
            $project->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

        return response()->json([
            'status'    => true,
            'message'   => "success",
            'db_html'   => $db_html,
        ]);

    }
}
