<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\User;
use App\Ticket;
use App\Comment;
use App\WebsiteType;
use App\Region;
use App\UserGroup;
use Carbon\Carbon;
use App\Mail\StatusChange;
use App\Mail\SubmitGACode;
use App\Mail\NewComment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Throwable;
use App\Mail\AssignAdmin;
use Config;
use App\Mail\SelfAssign;

class GATicketController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    public function getAllGATickets(Request $request) {
        $user = Auth::user();

        $table_columns = [
            'Requestor',
            'Requestor Email',
            'Assignee',
            'Assignee Email',
            'Website URL',
            'Status',
            'Created At'
        ];

        return view('/ticket/tickets', ['table_columns' => $table_columns, 'user' => Auth::user()]);
    }

    public function submitGACode(Request $request, $ticket_id) {
        try {
            request()->validate([
                'gacode' => 'required', 'string', 'max:255',
            ]);
        } catch (Throwable $e) {
            request()->validate([
                'gatag' => 'required', 'string', 'max:10000',
            ]);
        }
        
        
        $ticket = Ticket::find($ticket_id);
        if ($ticket['status'] !== 'Processing') {
            return redirect(env('APP_URL').'/admin/ga-tickets/'.$ticket_id)->with(['message' => "Cannot change status from " . $ticket['status'] . " to Closed.", "alert" => "alert-danger"]);
        }
        $ticket['gacode'] = request('gacode');
        $ticket['gatag'] = request('gatag');
        $ticket['status'] = "Closed";
        $ticket['completed_at'] = Carbon::now();

        $ticket_user = User::withTrashed()->find($ticket->requestor_id);

        $ticket->save();
        //email

        
        $markdown_info = [
            'name'          => $ticket_user->name,
            'admin_name'    => Auth::user()->name,
            'website_url'   => $ticket->website_url,
            'button'        => env('APP_URL').'/ga_request/'.$ticket_id,
            'gacode'        => request('gacode'),
            'gatag'         => request('gatag'),
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $ticket_user->email,
            'cc'            => '',
            'subject'       => 'GA Ticket - '.$ticket_id,
            'content'       => (new SubmitGACode($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);
        return redirect(env('APP_URL').'/admin/ga-tickets/'.$ticket_id)->with(['message' => "GA Code/Tag Submitted! The ticket-owner has been notified.", "alert" => "alert-success"]);
    }

    public function updateGATicketStatusSendEmail(Request $request, $ticket_id) {
        /*
            Updates ticket data. If status is closed, completed_at column of request is updated to the current time.
            Validate inputs. If failed, redirect to previous page with laravel's errors variable
            Automatically email the ticket's owner.

            inputs:
                    - ticket_id
                    - status
        */
        request()->validate([
            'status'  => ['required', 'string', Rule::in(['Open', 'Processing', 'Rejected'])],
        ]);

        $ticket = Ticket::find($ticket_id);

        if (request('status') === "Rejected") {
            $request->request->add(['body' => request('reject_msg')]);
            $this->createGACommentSendEmail($request, $ticket_id);
        }

        $ticket->status = request('status');
        $ticket_user = User::withTrashed()->find($ticket->requestor_id);
        $ticket->save();

        
        //email
        $markdown_info = [
            'name'          => $ticket_user->name,
            'admin_name'    => Auth::user()->name,
            'domain_or_ga'  => 'GA',
            'url_or_domain' => 'URL',
            'website_url'   => $ticket->website_url,
            'status'        => request('status'),
            'button'        => env('APP_URL').'/ga_request/'.$ticket_id,
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $ticket_user->email,
            'cc'            => '',
            'subject'       => 'GA Ticket - '.$ticket_id,
            'content'       => (new StatusChange($markdown_info))->render(),
            'layout'        => 'custom',
        ];
        $this->sendEmail($info);

        
        if (request('status') === "Processing") {
            return redirect(env('APP_URL').'/admin/ga-tickets/'.$ticket_id)->with(['message' => "Ticket status changed to Processing.", "alert" => "alert-warning"]);
        } else if (request('status') === "Rejected") {
            return redirect(env('APP_URL').'/admin/ga-tickets/'.$ticket_id)->with(['message' => "Ticket status changed to Rejected.", "alert" => "alert-success"]);
        }
    }

    public function viewGATicket($ticket_id) {
        /*
        Retrieve ticket by id

        input: 
            Route Wildcard: {ticket_id}
        */
        $ticket = Ticket::where([['ticket_id', $ticket_id], ['deleted_at', null]])->first();
        if ($ticket === null) {
            return redirect(env('APP_URL').'/admin/ga-tickets')->with(['message' => "The ga ticket you are trying to access does not exist.", "alert" => "alert-warning"]);
        }
        $comments = Comment::where('ticket_id', $ticket_id)->orderBy('created_at')->get();
        foreach ($comments as $comment) {
            $user = User::withTrashed()->find($comment->user_id);
            $comment_avatar = $user->avatar;
            if ($comment_avatar == null) {
                $comment->avatar = Config::get('app.url') . '/img/default_avatar3.png';
            } else {
                $comment->avatar = $comment_avatar;
            }
            $comment->name = $user->name;
            $comment->created_at = $comment->created_at->addHours(7);
        }
        $avatar = null;
        $group_name = null;
        $requestor = User::withTrashed()->find($ticket->requestor_id);
        $avatar = $requestor->avatar;
        $ticket["requestor_email"] = $requestor->email;
        $ticket["name"] = $requestor->name;
        $ticket["website_type"] = WebsiteType::find($ticket["website_type"])->website_type;
        $ticket["website_region"] = Region::find($ticket["website_region"])->region;
        $ticket['created_at'] =  $ticket['created_at']->addHours(7);
        $group = UserGroup::find($requestor->group);
        if ($group !== null) {
            $group_name = $group->user_group;
        } else if ($requestor->role === 'admin') {
            $group_name = 'Admin';
        } else if ($requestor->role === 'super_admin') {
            $group_name = 'Super Admin';
        }

        $admins = User::where('role', 'admin')->orWhere('role', 'super_admin')->get();

        if ($ticket->assignee_id) {
            $assignee = User::find($ticket->assignee_id);
            if ($assignee->role === 'admin') {
                $assignee->group = 'Admin';
            }
            if ($assignee->role === 'super_admin') {
                $assignee->group = 'Super Admin';
            }
        } else {
            $assignee = null;
        }

        return view('/ticket/view_ticket', [
            'ticket'    => $ticket,
            'comments'  => $comments,
            'user'      => Auth::user(),
            'avatar'    => $avatar,
            'group'     => $group_name,
            'admins'    => $admins,
            'assignee'  => $assignee,
        ]);
    }

    public function createGACommentSendEmail(Request $request, $ticket_id) {
        /*
            Create a new comment for the currently logged-in user. Users can only comment on their own ticket. Admins can comment on any ticket

            Inputs are validated. If fail redirect back to previous page with laravel's error variable

            Automatically emails the all admins if user makes the comment, or the ticket owner otherwise.
            
            input:
                - ticket_id             => value
                - body                  => value
        */

        request()->validate(['body' => ['required', 'string', 'max:1000']]);

 
        $user = Auth::user();
        $user_name = $user->name;
        $ticket = Ticket::find($ticket_id);
        $new_comment = new Comment;
        $new_comment['user_id'] = $user->user_id;
        $new_comment['ticket_id'] = $ticket_id;
        $new_comment['body'] = request('body');
        $new_comment->save();
        $ticket_user = User::withTrashed()->find($ticket->requestor_id);

        //email
        $markdown_info = [
            'name'          => $ticket_user->name,
            'admin_name'    => $user->name,
            'comment_msg'   => request('body'),
            'domain_or_ga'  => 'GA',
            'url_or_domain' => 'URL',
            'website_url'   => $ticket->website_url,
            'button'        => env('APP_URL').'/ga_request/'.$ticket_id
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $ticket_user->email,
            'cc'            => '',
            'subject'       => 'GA Ticket - '.$ticket_id,
            'content'       => (new NewComment($markdown_info))->render(),
            'layout'        => 'custom',
        ];
        $this->sendEmail($info);
        return redirect(env('APP_URL')."/admin/ga-tickets/".$ticket_id)->with(['message' => "Comment Created", "alert" => "alert-secondary"]);
    }

    public function assignAdmin(Request $request, $ticket_id) {
        request()->validate([
            'assignee_id'   => ['required', 'integer', 'exists:users,user_id'],
        ]);
        $user = Auth::user();
        if ($user->role == 'admin') {
            request()->validate([
                'assignee_id'   => [Rule::in([$user->user_id])],
            ]);
        }
        $ticket = Ticket::find($ticket_id);
        $ticket->assignee_id = request('assignee_id');
        $ticket->save();
        $assignee = User::find(request('assignee_id'));
        //email
        $markdown_info = [
            'name'          => $assignee->name,
            'admin_name'    => Auth::user()->name,
            'domain_or_ga'  => 'GA',
            'url_or_domain' => 'URL',
            'website_url'   => $ticket->website_url,
            'button'        => env('APP_URL').'/admin/ga-tickets/'.$ticket_id
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $assignee->email,
            'cc'            => '',
            'subject'       => 'GA Ticket - '.$ticket_id,
            'content'       => (new AssignAdmin($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $request->request->add(['status' => 'Open']);
        $this->updateGATicketStatusSendEmail($request, $ticket_id);
        if ($user->role === 'super_admin' and request('assignee_id') === $user->user_id) {
            return redirect(env('APP_URL')."/admin/domain-tickets/".$ticket_id)->with(['message' => 'Ticket Accepted!', "alert" => "alert-success"]);
        }
        if ($user->role === 'admin') {
            $super_admins = User::where('role', 'super_admin')->get();
            $counter = 0;
            $emails = '';
            foreach ($super_admins as $super_admin) {
                if ($counter === 0) {
                    $first_email = $super_admin->email;
                    $counter++;
                } else {
                    $emails .= $super_admin->email.',';
                }
            }
            $markdown_info2 = [
                'admin_name'    => Auth::user()->name,
                'domain_or_ga'  => 'domain',
                'url_or_domain' => 'Domain',
                'website_url'   => $ticket->domain_name,
                'button'        => env('APP_URL').'/admin/ga-tickets/'.$ticket_id,
            ];
            $info2 = [
                'from'          => 'GaTicket Garena',
                'from_mail'	    => 'no-reply@garena.co.th',
                'to'            => $first_email,
                'cc'            => $emails,
                'subject'       => 'GA Ticket - '.$ticket_id,
                'content'       => (new SelfAssign($markdown_info2))->render(),
                'layout'        => 'custom'
            ];
            $this->sendEmail($info2);
            return redirect(env('APP_URL')."/admin/ga-tickets/".$ticket_id)->with(['message' => 'Ticket Accepted!', "alert" => "alert-success"]);
        }
        $this->sendEmail($info);
        return redirect(env('APP_URL')."/admin/ga-tickets/".$ticket_id)->with(['message' => 'Ticket assigned! Admin '.$assignee->name.' has been notified.', "alert" => "alert-success"]);
    }
}
