<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\UserGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Auth;
use Error;
use Exception;
use Throwable;
use App\Mail\NewUser;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function sendEmail($data)
    {
        ob_start();
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, 'http://api.apps.garena.in.th/sendmail');
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 30);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec ($cURL);
        curl_close ($cURL);
        ob_end_clean();

        return $result;
    }

    function showUsers(Request $request) {
        $table_columns = [
            ["email","Email"],
            ["role","Role"],
            ["group","Group"],
            ["deleted_at","Status"],
            ["created_at","Created At"],
        ];
        $user = Auth::User();
        $listed_users = User::withTrashed()->get();
        $user_groups = UserGroup::get();
        $user_groups_assoc_arr = array();
        foreach ($user_groups as $user_group) {
            $user_groups_assoc_arr[$user_group['id']] = $user_group['name'];
        }
        return view('/user/users', ["listed_users"=>$listed_users, "user"=>$user, "table_columns"=>$table_columns, "user_groups"=>$user_groups_assoc_arr, "deleted_user"=>$request['deleted_user']]);
    }

    function showNewUser(Request $request) {
        $user = Auth::User();
        $user_groups = UserGroup::get();
        $user_groups_assoc_arr = array();
        foreach ($user_groups as $user_group) {
            $user_groups_assoc_arr[$user_group['id']] = $user_group['user_group'];
        }
        return view('/user/new_user', ["user"=>$user, "user_groups"=>$user_groups_assoc_arr]);
    }

    function showViewUser(Request $request, $listed_user_id) {
        $user = Auth::User();
        $listed_user = User::withTrashed()->find($listed_user_id);
        if ($listed_user === null) {
            return redirect(env('APP_URL').'/admin/users')->with(['message' => "The user you are trying to access does not exist.", "alert" => "alert-warning"]);
        }
        $user_groups = UserGroup::get();
        $user_groups_assoc_arr = array();
        foreach ($user_groups as $user_group) {
            $user_groups_assoc_arr[$user_group['id']] = $user_group['user_group'];
        }
        if ($listed_user == null) {
            return view('/user/view_user', ["listed_user"=>$listed_user, "user_groups"=>$user_groups_assoc_arr,"user"=>$user, 'message' => "This user does not exist.", "alert" => "alert-danger"]);
        } else {
            return view('/user/view_user', ["listed_user"=>$listed_user, "user_groups"=>$user_groups_assoc_arr, "user"=>$user]);
        }
        
    }

    function reactivateUser(Request $request, $listed_user_id) {
        $listed_user = User::onlyTrashed()->where("user_id", $listed_user_id)->first();
        if ($listed_user === null) {
            return redirect()->back()->with(['message' => "User was not sucessfully reactivated. User not found.", "alert" => "alert-danger"]);
        } else {
            if ($listed_user['role'] == "super_admin") {
                return redirect()->back()->with(['message' => "User was not sucessfully reactivated. You do not have permission to modify a Super Admin account.", "alert" => "alert-danger"]);
            } else {
                if (User::where("email", $listed_user['email'])->first() !== null) {
                    return redirect()->back()->with(['message' => "User was not sucessfully reactivated. An active user with this email already exists.", "alert" => "alert-danger"]);
                } else {
                    $listed_user->restore();
                    $listed_user['user_status'] = "Active";
                    $listed_user->save();
                    return redirect()->back()->with(['message' => "User was sucessfully reactivated.", "alert" => "alert-success"]);
                }
            }
        }
    }

    function deleteUser(Request $request, $listed_user_id) {
        $listed_user = User::find($listed_user_id);
        if ($listed_user === null) {
            return redirect()->back()->with(['message' => "User was not sucessfully deactivated. User not found.", "alert" => "alert-danger"]);
        } else {
            if ($listed_user['role'] == "super_admin") {
                return redirect()->back()->with(['message' => "User was not sucessfully deactivated. You do not have permission to modify a Super Admin account.", "alert" => "alert-danger"]);
            } else {
                $listed_user['user_status'] = "Inactive";
                $listed_user->save();
                $listed_user->delete();
                return redirect()->back()->with(['message' => "User was sucessfully deactivated.", "alert" => "alert-success"]);
            }
        }
    }

    function updateUser(Request $request, $listed_user_id) {
        $listed_user = User::withTrashed()->find($listed_user_id);
        if ($listed_user === null) {
            return redirect(env('APP_URL')."/admin/users")->with(['message' => "User data was not sucessfully updated. User not found.", "alert" => "alert-danger"]);
        } else {
            if ($listed_user['role'] === "super_admin") {
                return redirect(env('APP_URL')."/admin/users")->with(['message' => "User data was not sucessfully updated. You do not have permission to modify a super admin account.", "alert" => "alert-danger"]);
            } else {
                request()->validate([
                    'name'  => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'max:255', 'email'],
                    'role'  => ['required', 'string', 'max:255', Rule::in(['admin', 'user'])],
                ]);

                if ($listed_user['email'] !== request('email') and $listed_user['deleted_at'] === null) {
                    $emails = DB::table('users')->where('deleted_at', null)->pluck('email');
                    request()->validate([
                        'email' => [Rule::notIn($emails)],
                    ]);
                }
                
                $user_groups = UserGroup::get();
                $user_group_ids = [];
                foreach ($user_groups as $user_group) {
                    array_push($user_group_ids, $user_group['id']);
                }

                if ($request['role'] === 'user') {
                    request()->validate([
                        'group'  => ['required', 'Integer', Rule::in($user_group_ids)],
                    ]);
                } else if ($request['role'] === 'admin'){
                    request()->validate([
                        'group'  => [Rule::in([''])],
                    ]);
                }

                $listed_user['name'] = $request['name'];
                $listed_user['email'] = $request['email'];
                $listed_user['role'] = $request['role'];
                $listed_user['group'] = $request['group'];
                $listed_user->save();

                return redirect(env('APP_URL')."/admin/users")->with(['message' => "User data sucessfully updated.", "alert" => "alert-success"]);
            }
        }
    }

    function createUser(Request $request) {
        $emails = DB::table('users')->where('deleted_at', null)->pluck('email');
        request()->validate([
            'name'  => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'email', Rule::notIn($emails)],
            'role'  => ['required', 'string', 'max:255', Rule::in(['admin', 'user'])],
        ]);

        $user_groups = UserGroup::get();
        $user_group_ids = [];
        foreach ($user_groups as $user_group) {
            array_push($user_group_ids, $user_group['id']);
        }

        if ($request['role'] === 'user') {
            request()->validate([
                'group'  => ['required', 'Integer', Rule::in($user_group_ids)],
            ]);
        } else if ($request['role'] === 'admin'){
            request()->validate([
                'group'  => [Rule::in([''])],
            ]);
        }

        $new_user = new User;
        $new_user['name'] = $request['name'];
        $new_user["email"] = $request['email'];
        $new_user["role"] = $request['role'];
        $new_user["group"] = $request['group'];
        $new_user->save();

        //email
        $markdown_info = [
            'name'          => request('name'),
            'admin_name'    => Auth::user()->name,
            'button'        => env('APP_URL'),
        ];
        $info = [
            'from'          => 'GaTicket Garena',
            'from_mail'	    => 'no-reply@garena.co.th',
            'to'            => $new_user->email,
            'cc'            => '',
            'subject'       => 'Account Registered!',
            'content'       => (new NewUser($markdown_info))->render(),
            'layout'        => 'custom'
        ];
        $this->sendEmail($info);

        return redirect(env('APP_URL').'/admin/users')->with(['message' => "User sucessfully created.", "alert" => "alert-success"]);

    
        
    }
    
}