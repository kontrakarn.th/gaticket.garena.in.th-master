<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{

    use SoftDeletes;

    protected $table = "tickets" ;
    protected $primaryKey = 'ticket_id';

    public function requestor()
    {
        return $this->belongsTo('App\User', 'requestor_id', 'user_id')->withTrashed();
    }

    public function assignee()
    {
        return $this->belongsTo('App\User', 'assignee_id', 'user_id')->withTrashed();
    }
}
