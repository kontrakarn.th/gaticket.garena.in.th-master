<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DComment extends Model
{
    use SoftDeletes;

    protected $table = "dcomments" ;
    protected $primaryKey = 'dcomment_id';
}
