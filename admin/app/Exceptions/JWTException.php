<?php

namespace App\Exceptions;

use Exception;

class JWTException extends Exception
{

    public function render($request, Exception $exception)
    {
        if($exception instanceof CustomException) {
            return $this->showCustomErrorPage();
        }

        return parent::render($request, $exception);

        
    }
}
