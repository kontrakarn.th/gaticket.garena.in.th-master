export const GET_USER_INFO = 'http://test.gaticket.garena.in.th/api/get-user-info'

export const GET_ALL_GA_TICKETS = 'http://test.gaticket.garena.in.th/api/get-all-tickets-of-this-user'
export const CREATE_GA_TICKET = 'http://test.gaticket.garena.in.th/api/create-ticket-send-email'
export const READ_GA_TICKET = 'http://test.gaticket.garena.in.th/api/read-ticket'
export const GET_ALL_GA_COMMENTS = 'http://test.gaticket.garena.in.th/api/get-all-comments-of-ticket'
export const CREATE_GA_COMMENT = 'http://test.gaticket.garena.in.th/api/create-comment-send-email'

export const GET_ALL_DOMAIN_TICKETS = 'http://test.gaticket.garena.in.th/api/get-all-domain-tickets-of-user'
export const CREATE_DOMAIN_TICKET = 'http://test.gaticket.garena.in.th/api/create-domain-ticket-send-email'
export const READ_DOMAIN_TICKET = 'http://test.gaticket.garena.in.th/api/read-domain-ticket'
export const GET_ALL_DOMAIN_COMMENTS = 'http://test.gaticket.garena.in.th/api/get-all-comments-of-domain-ticket'
export const CREATE_DOMAIN_COMMENT = 'http://test.gaticket.garena.in.th/api/create-domain-comment-send-email'
