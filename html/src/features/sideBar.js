import React, {useRef,useState} from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {useCookies} from 'react-cookie';
import useOutsideClick from "./useOutsideClick";

const SideBar = (props) => {
    const {onClickToggle, data, displaySideBar, onClickOutside} = props
    const ref = useRef();

    useOutsideClick(ref, () => {
        if (displaySideBar) onClickOutside();
    });

    return (
        <Container displaySideBar={displaySideBar} ref={ref}>
            <HeaderWrapper>
                <HeaderText>GA Ticket User</HeaderText>
            </HeaderWrapper>
            <UL>
                <MenuHeader>My Tickets</MenuHeader>
                <LI>
                    <Link to='/ga_request'>
                        <Item redirectBack={false} onClick={()=>onClickToggle()}>
                            <Text>GA Ticket Requests</Text>
                        </Item>
                    </Link>
                </LI>
                <LI>
                    <Link to='/domain_request'>
                        <Item redirectBack={false} onClick={()=>onClickToggle()}>
                            <Text>Domain Ticket Requests</Text>
                        </Item>
                    </Link>
                </LI>
                {data.role!=='user' && 
                <>
                    <MenuHeader notTop={true}>Admin Dashboard</MenuHeader>
                    <LI>
                        <Item redirectBack={true}>
                            <Text2 href='/admin/dashboard'>Back to Admin Page</Text2>
                        </Item>
                    </LI>
                </>}
            </UL>
        </Container>
    )
}

export default SideBar;

const Container = styled.aside`
    background-color: ${props=>props.displayGa? '#f8fafb' : 'white'};
    height: 100%;
    border-right: 1px solid rgb(221, 221, 221);
    bottom: 0px;
    left: 0px;
    overflow: auto;
    position: fixed;
    top: 50px;
    z-index: 5;
    width: 270px;
    transition: transform 0.3s ease-in-out;
    transform: ${props => props.displaySideBar ? 'translateX(0)' : 'translateX(-100%)'};
`

const UL = styled.ul`
    padding: 0;
    margin: 0;
    line-height: 28px;
`

const LI = styled.li`
    display: block;
    position: relative;
    box-sizing: border-box;
`
const Item = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    height: 50px;
    padding: 0 20px;
    width: 100%;
    letter-spacing: .3px;
    color: #78828a;
    text-decoration: none;
    font-weight: 700;
    cursor: pointer;
    :before{
        content: ${p => p.redirectBack? '"\\f141"' : '"\\f0ce"'} ;
        font-family: 'FontAwesome';
        width: 28px;
        margin-right: 10px;
        text-align: center;
        margin-left: 4px;
        margin-top: 6px;
    }
`
const Text = styled.span`
    margin-top: 3px;
    width: 100%;
`
const Text2 = styled.a`
    margin-top: 3px;
    width: 100%;
    text-decoration: none;
    color: inherit;
    :visited {
        color: inherit;
    }
    :hover {
        color: inherit;
    }
`
const HeaderWrapper = styled.div`
    display: inline-block;
    width: 100%;
    text-align: center;
    height: 60px;
    line-height: 60px;
    box-sizing: border-box;
`
const HeaderText = styled.p`
    text-decoration: none;
    text-transform: uppercase;
    letter-spacing: 1.5px;
    font-weight: 700;
    color: #000;
`
const MenuHeader = styled.div`
    padding: 3px 15px;
    color: #a1a8ae;
    font-size: 10px;
    text-transform: uppercase;
    letter-spacing: 1.3px;
    font-weight: 600;
    display: box;
    box-sizing: border-box;
    margin-top: ${props=>props.notTop? '10px' : '0px' };
`



