import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

const NavLeft = (props) => {
    const {onClickToggle} = props;
    return(
        <>
            <DataToggle onClick={onClickToggle}/>
            <ButtonDivider/>
            <Link to='/fill_domain_request'>
                <CreateTicket>
                    Create Domain Ticket
                </CreateTicket>
            </Link>
            <ButtonDivider/>
            <Link to='/fill_ga_request'>
                <CreateTicket>
                    Create GA Ticket
                </CreateTicket>
            </Link>   
        </>
    )
}

export default NavLeft;

const DataToggle = styled.span`
    margin-right: 10px;
    font-size: 25px;
    padding-left: 20px;
    line-height: 46px;
    font-weight: 100;
    cursor: pointer;
    :before{
        padding-left: 0;
        margin-bottom: 0;
        content: "\f0c9"; 
        font-family: FontAwesome;
        color: white;
    }
`

const ButtonDivider = styled.div`
    margin: 10px 10px;
    height: 30px;
`
const CreateTicket = styled.button`
    box-shadow: inset 0 0 0 1px rgba(16,22,26,.4), inset 0 -1px 0 rgba(16,22,26,.2);
    background-color: rgb(15, 153, 96);
    background-image: linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0));
    color: #fff;
    display: inline-block;
    border: none;
    border-radius: 3px;
    cursor: pointer;
    padding: 0 10px;
    vertical-align: middle;
    font-size: 14px;
    min-width: 30px;
    min-height: 30px;
    line-height: 30px;
    margin-top: 10px;
    :before{
        content: "\f067";
        font-family: FontAwesome;
        font-size: 15px;
        margin-right: 5px;
    }
    :hover{
        background-color: rgb(15, 175, 96);
    }
`