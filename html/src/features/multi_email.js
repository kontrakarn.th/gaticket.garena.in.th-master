import React,{useState,useEffect} from 'react';
import styled from 'styled-components';
import { ReactMultiEmail, isEmail } from 'react-multi-email';
import 'react-multi-email/style.css';

const MultiEmail = (props) =>{
    const {getManyEmail} = props;
    const [emails, setEmails] = useState([]);

    useEffect(()=>{    
        getManyEmail(emails.join(','))
    }, [emails])
    
    return (
        <>
        <Label for="emails_for_permission">Emails Requested for Permission</Label>
        {/* <Email as={ReactMultiEmail}
            id="email"
            placeholder="placeholder"
            emails={emails}
            onChange={(_emails) => {
                setEmails(_emails);
            }}
            validateEmail={email => {
                return isEmail(email); // return boolean
            }}
            getLabel={(
                email,
                index,
                removeEmail: (index: number) => void,
            ) => {
                return (
                <div data-tag key={index}>
                    {email}
                    <span data-tag-handle onClick={() => removeEmail(index)}>
                    ×
                    </span>
                </div>
                );
            }}/> */}
        <ReactMultiEmail
            id="email"
            placeholder="insert email here"
            emails={emails}
            onChange={(_emails) => {
                setEmails(_emails);
            }}
            validateEmail={email => {
                return isEmail(email); // return boolean
            }}
            getLabel={(
                email,
                index,
                removeEmail: (index: number) => void,
            ) => {
                return (
                <DataTag key={index}>
                    {email}
                    <span data-tag-handle onClick={() => removeEmail(index)}>
                    ×
                    </span>
                </DataTag>
                );
            }}/>
            {/* <br />
            <h4>react-multi-email value</h4>
            <p>{emails.join(', ') || 'empty'}</p> */}
        </>        
    )
}

export default MultiEmail;

const DataTag = styled.div`
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
    text-align: left;
    line-height: 1;
    vertical-align: baseline;
    margin: 0.14285714em;
    background-color: #f3f3f3;
    background-image: none;
    padding: 0.5833em 0.833em;
    color: rgba(0, 0, 0, 0.6);
    text-transform: none;
    font-weight: 600;
    border: 0 solid transparent;
    border-radius: 0.28571429rem;
    transition: background 0.1s ease;
    font-size: 1.2rem;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    max-width: 100%;
`
const Label = styled.label`
    font-weight: 600;
    color: #34395e;
    font-size: 12px;
    letter-spacing: .5px;
    display: inline-block;
    margin-bottom: .5rem;
`
// const Email = styled.input`
//     margin: 0;
//     max-width: 100%;
//     flex: 1 0 auto;
//     outline: 0;
//     -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
//     text-align: left;
//     line-height: 1.21428571em;
//     padding: 0.4em 0.5em;
//     background: #fff;
//     border: 1px solid rgba(34, 36, 38, 0.15);
//     color: rgba(0, 0, 0, 0.87);
//     border-radius: 0.28571429rem;
//     transition: box-shadow 0.1s ease, border-color 0.1s ease;
//     font-size: 13px;
//     position: relative;
//     display: flex;
//     flex-wrap: wrap;
//     align-items: center;
//     align-content: flex-start;
// `

