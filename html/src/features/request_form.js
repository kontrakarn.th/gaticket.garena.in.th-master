import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

function RequestForm(props){
    const {is_Ga, data, user, assigneeData} = props;
    return(
        <>
        <Container topCard={true}>
            <Card>
                <CardHeader>
                    <HeaderText>{is_Ga? 'Request for GA Code - ID: ' + data.ticket_id : 'Request for Domain - ID: ' + data.dticket_id}</HeaderText>
                </CardHeader>
                <CardBody>
                    <FormGroup>
                        <Form>
                            <Label for="name">Name</Label>
                            <Input 
                                id="name" 
                                type="name" 
                                name="name" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled
                                isDisabled = {true}
                                value = {user.name}
                            />
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="requestor_email">Email</Label>
                            <Input 
                                id="requestor_email" 
                                type="requestor_email" 
                                name="requestor_email" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled 
                                isDisabled = {true}
                                value = {user.email}
                            />
                        </Form>
                    </FormGroup>
                    {is_Ga && data.emails_for_permission!==null && <FormGroup>
                        <Form>
                            <Label for="emails_for_permission">Emails Requested for Permission</Label>
                            <Input 
                                id="emails_for_permission" 
                                type="emails_for_permission" 
                                name="emails_for_permission" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled 
                                isDisabled = {true}
                                value = {data.emails_for_permission}
                            />
                        </Form>
                    </FormGroup>}
                    {is_Ga && <FormGroup>
                        <Form>
                            <Label for="website_url">Website URL</Label>
                            <Input 
                                id="website_url" 
                                type="website_url" 
                                name="website_url" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled
                                isDisabled = {true}
                                value = {data.website_url}
                            />
                        </Form>
                    </FormGroup>}
                    {is_Ga && <FormGroup>
                        <Form>
                            <Label for="website_type">Website Type</Label>
                            <Input 
                                id="website_type" 
                                name="website_type" 
                                tabIndex="1" 
                                required 
                                autoFocus
                                disabled 
                                isDisabled = {true}
                                value = {data.website_type}
                            /> 
                        </Form>
                    </FormGroup>}
                    {is_Ga && <FormGroup>
                        <Form>
                            <Label for="website_region">Region</Label>
                            <Input 
                                id="website_region" 
                                name="website_region" 
                                tabIndex="1" 
                                required 
                                autoFocus
                                disabled
                                isDisabled = {true} 
                                value = {data.website_region}
                            />
                        </Form>
                    </FormGroup>}
                    {!is_Ga && <FormGroup>
                        <Form>
                            <Label for="domain_name">Domain Name</Label>
                            <Input 
                                id="domain_name" 
                                type="domain_name" 
                                name="domain_name" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled
                                isDisabled = {true} 
                                value = {data.domain_name}
                            />
                        </Form>
                    </FormGroup>}
                    <FormGroup>
                        <Form>
                            <Label for="created_at">Created At</Label>
                            <Input 
                                id="created_at" 
                                type="created_at" 
                                name="created_at" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled
                                isDisabled = {true} 
                                value = {moment(data.created_at).local().format('LLL')}
                            />
                        </Form>
                    </FormGroup>
                    {user.role!=='user' && data.remarks!==null && <FormGroup>
                        <Form>
                            <Label for="remarks">Remarks</Label>
                            <TextArea 
                                id="remarks" 
                                type="remarks" 
                                name="remarks" 
                                tabIndex="1" 
                                autoFocus 
                                value = {data.remarks}
                                placeholder="Add a new remark"
                                disabled
                                isDisabled = {true} 
                            />
                        </Form>
                    </FormGroup>}
                </CardBody>
            </Card>
        </Container>

        {assigneeData!==null && <Container>
            <Card>
                <CardHeader>
                    <HeaderText>Assignee Information</HeaderText>
                </CardHeader>
                <CardBody>
                    <FormGroup>
                        <Form>
                            <Label for="name">Name</Label>
                            <Input 
                                id="name" 
                                type="name" 
                                name="name" 
                                tabIndex="1" 
                                autoFocus 
                                disabled
                                isDisabled = {true} 
                                value = {assigneeData.name}
                            />
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="email">Email</Label>
                            <Input 
                                id="email" 
                                type="email" 
                                name="email" 
                                tabIndex="1" 
                                required 
                                autoFocus 
                                disabled
                                isDisabled = {true} 
                                value = {assigneeData.email}
                                />
                            </Form>
                    </FormGroup>
                </CardBody>
            </Card>
        </Container>}

        {is_Ga && data.gacode!==null && <Container>
             <Card>
                <CardHeader>
                    <HeaderText>GA Code</HeaderText>
                </CardHeader>
                <CardBody>
                    <GA>{data.gacode}</GA>
                </CardBody>
            </Card>
        </Container>}
        {is_Ga && data.gatag!==null && <Container>
             <Card>
                <CardHeader>
                    <HeaderText>GA Tag</HeaderText>
                </CardHeader>
                <CardBody>
                    <Pre>{data.gatag}</Pre>
                </CardBody>
            </Card>
        </Container>}
        </>
    )
}

export default RequestForm;

const Container = styled.div` 
    box-sizing: border-box;
    display: block;
    min-height: 100%;
    height: 100%;
    margin-bottom: 15px;
    margin-top: ${p=>p.topCard? '91px' : '15px'};
    padding: 0;
`
const Card = styled.div`
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.03);
    background-color: #fff;
    border-radius: 3px;
    border: none;
    position: relative;
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    max-width: 50%;
    margin-left: 25%;
    @media (max-width: 768px) {
        min-width: 90%;
        width: 90%;
        margin-left: 5%
    }
`
const CardHeader = styled.div`
    border-bottom: 1px solid #f9f9f9;
    line-height: 30px;
    align-self: center;
    width: 100%;
    min-height: 30px;
    padding: 15px 25px;
    display: flex;
    align-items: center;
    background-color: transparent;
    margin-bottom: 0;
    box-sizing: border-box;
`
const HeaderText = styled.h4`
    font-size: 24px;
    line-height: 28px;
    color: #6777ef;
    padding-right: 10px;
    margin-bottom: 0;
    font-weight: 700;
    margin-top: 0;
    font-family: "Nunito", "Segoe UI", arial;
`
const CardBody = styled.form`
    background-color: transparent;
    padding: 20px 25px;
`
const Form = styled.div`
    display: block;
    margin-top: 0em;
`
const FormGroup = styled.div`
    margin-bottom: 25px;
    display: block;
`
const Input = styled.input`
    font-size: 14px;
    padding: 10px 15px;
    height: 42px;
    display: block; 
    width: 100%;
    background-color: ${props=>props.isDisabled? '#eee' : '#fdfdff' };
    border-color: #e4e6fc;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-sizing: border-box;
`
const Label = styled.label`
    font-weight: 600;
    color: #34395e;
    font-size: 12px;
    letter-spacing: .5px;
    display: inline-block;
    margin-bottom: .5rem;
`

const TextArea = styled.textarea`
    resize: none;
    height: 120px;
    width: 100%;
    max-width: 100%;
    padding: 10px;
    line-height: 1.28581;
    border-radius: 3px;
    border-color: rgb(206, 212, 218);
    background-color: #eee;
    vertical-align: middle;
    color: #495057;
    font-size: 14px;
    font-weight: 400;
    transition: box-shadow .1s cubic-bezier(.4,1,.75,.9);
    box-sizing: border-box;
`
const Pre = styled.pre`
    color: #47c363 !important;
`
const GA = styled.div`
    font-size: 30px;
    color: #47c363 !important;
`