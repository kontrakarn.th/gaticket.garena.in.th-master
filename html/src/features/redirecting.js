import React from 'react';
import {useCookies} from 'react-cookie';
import {useHistory, useLocation, } from 'react-router-dom';

const Redirecting = (props) => {

    const history = useHistory();

    var query = useLocation().search;
    var token = query.slice(7);

    const [cookie, setCookie] = useCookies(['token']);

    if(query!==''){
        setCookie('token',token,{ path: '/' , maxAge: 7200});
        history.push(props.location.pathname)
    } else{
        if(typeof cookie.token!== 'undefined'){
            setCookie('token', cookie.token,{ path: '/' , maxAge: 7200})
        }
    }
    return (
        <></>
    )
}

export default Redirecting;