import React from 'react';
import styled from 'styled-components';

const Overlay = () => {
    return(
        <Div/>
    )

}

export default Overlay;

const Div = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgb(0, 0, 0, 0.5);
    z-index: 2;
`