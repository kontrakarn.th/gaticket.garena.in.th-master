import React from 'react';
import styled from 'styled-components';

const StatusBar = (props) => {
    const {data} = props
    return(
        <StatusContainer isCurrentStatus={data.status==='Rejected'}>
            {data.status ==='Rejected'? <Rejected>TICKET REJECTED</Rejected> :
            <>
                <Status isCurrentStatus={data.status==='Unassigned'}>UNASSIGNED</Status>
                <Arrow/>
                <Status isCurrentStatus={data.status==='Open'}>OPEN</Status>
                <Arrow/>
                <Status isCurrentStatus={data.status==='Processing'}>PROCESSING</Status>
                <Arrow/>
                <Status isCurrentStatus={data.status==='Closed'}>CLOSED</Status>
            </> }
        </StatusContainer>
    )
}

export default StatusBar;


const Status = styled.div`
    border: 2px solid rgb(15, 153, 96);
    color: ${props=>props.isCurrentStatus? 'rgb(255, 255, 255)' : 'rgb(138, 155, 168)'};
    margin: 0px;
    padding: 5px 3px;
    font-size: 14px;
    font-weight: 500;
    line-height: 1.1;
    background-color: ${props=>props.isCurrentStatus? 'rgb(15, 153, 96)' : 'white'};
`
const StatusContainer = styled.div`
    margin-top: 0;
    top: 70px;
    position: relative;
    align-items: center;
    display: flex;
    justify-content: ${props=>props.isCurrentStatus? 'center' : 'space-between'};
    padding: 5px;
    margin-bottom: 15px;
    margin-left: 25%;
    margin-right: 25%;
    @media (max-width: 768px) {
        margin-left: 5%;
        margin-right: 5%;
    }
`
const Arrow = styled.div`
    font-size: 35px;
    :before{
        content: "\f101";
        font-family: FontAwesome;
    }
`

const Rejected = styled.div`
    border: 2px solid rgb(255, 0, 0);
    color: rgb(255, 255, 255);
    margin: 0px;
    padding: 5px 3px;
    font-size: 20px;
    font-weight: 500;
    line-height: 1.1;
    background-color: rgb(255, 0, 0);
`