import React,{useState} from 'react';
import styled from 'styled-components';

const CommentAdd = (props) => {
    const {handleCommentSubmit, disabled} = props;
    const[comment,setComment] = useState('');
    return(
        <CommentWrapper>
            <CommentHeader>
                <div>New Comment</div>
            </CommentHeader>
            <CommentBody>
                <CommentSubBody>
                    <CommentText 
                        placeholder="Add a new comment"
                        onFocus={(e) => e.target.placeholder = ''} 
                        onBlur={(e) => e.target.placeholder = 'Add a new comment'}
                        onChange={e => setComment(e.target.value)}
                        value = {comment}
                        maxLength = '1000'
                    />
                    <CommentButton disabled={disabled}
                        onClick={e => {
                            handleCommentSubmit(comment);
                            setComment('');
                            }}>Comment</CommentButton>
                </CommentSubBody>
            </CommentBody>
        </CommentWrapper>
    )
}

export default CommentAdd;

const CommentWrapper = styled.div`
    border-radius: 3px;
    border: 1px solid rgba(16, 22, 26, 0.15);
    display: flex;
    flex-direction: column;
    margin-top: 15px;
    margin-bottom: 15px;
    width: 50%;
    margin-left: 25%;
    @media (max-width: 768px) {
        width: 90%;
        margin-left: 5%
    }
`
const CommentHeader = styled.div`
    align-items: center;
    background-color: rgb(247, 247, 247);
    display: flex;
    flex-direction: row;
    font-size: 14px;
    justify-content: space-between;
    padding: 10px;
    border-bottom: 1px solid rgba(16, 22, 26, 0.15);
`
const CommentBody = styled.div`
    overflow: hidden;
    padding: 10px;
    background-color: white;
`
const CommentSubBody = styled.div`
    display: flex;
    flex-direction: column; 
    align-items: center;
    min-height: 160px;
`
const CommentText = styled.textarea`
    resize: none;
    height: 120px;
    width: 100%;
    max-width: 100%;
    padding: 10px;
    line-height: 1.28581;
    outline: none;
    border: none;
    border-radius: 3px;
    box-shadow: 0 0 0 0 rgba(19,124,189,0), 0 0 0 0 rgba(19,124,189,0), inset 0 0 0 1px rgba(16,22,26,.15), inset 0 1px 1px rgba(16,22,26,.2);
    background: #fff;
    vertical-align: middle;
    color: #182026;
    font-size: 14px;
    font-weight: 400;
    transition: box-shadow .1s cubic-bezier(.4,1,.75,.9);
    box-sizing: border-box;
    ::-webkit-input-placeholder {
        color: #ccc;
    }
    :-ms-input-placeholder {
    color: #ccc;
    }
`
const CommentButton = styled.button`
    box-shadow: inset 0 0 0 1px rgba(16,22,26,.4), inset 0 -1px 0 rgba(16,22,26,.2);
    background-color: #0f9960;
    background-image: linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0));
    color: #fff;
    display: inline-block;
    border: none;
    border-radius: 3px;
    cursor: pointer;
    padding: 0 10px;
    vertical-align: middle;
    font-size: 14px;
    min-width: 30px;
    min-height: 30px;
    line-height: 30px;
    margin-top: 10px;
    :hover{
        background-color: #0F993D;
    }
`