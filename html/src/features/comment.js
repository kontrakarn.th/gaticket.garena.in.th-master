import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

const Comment = (props) => {
    const{comments} = props;
    console.log("comments", comments)
    return(
        <>
            {comments && comments.map((item,index)=>{
                return(
                    <Container>
                        <Header>
                            <b>{item.name}</b>
                            &nbsp;commented at&nbsp;
                            <Date>{moment(item.created_at).local().format('LLL')}</Date>
                        </Header>
                        <Body>
                            <Text>{item.body}</Text>
                        </Body>
                    </Container>
                )
            })}
        </>
    )
}

export default Comment;

const Container = styled.div`
    border-radius: 3px;
    border: 1px solid rgba(16, 22, 26, 0.15);
    display: flex;
    flex-direction: column;
    margin-top: 15px;
    margin-bottom: 15px;
    width: 50%;
    margin-left: 25%;
    @media (max-width: 768px) {
        width: 90%;
        margin-left: 5%
    }
`

const Header = styled.div`
    align-items: center;
    background-color: rgb(247, 247, 247);
    font-size: 14px;
    justify-content: space-between;
    padding: 10px;
    border-bottom: 1px solid rgba(16, 22, 26, 0.15);
`

const Date = styled.span`
    color: rgb(115, 134, 148);
`

const Body = styled.div`
    font-size: 14px;
    display: block;
    background-color: #fff;
    box-sizing: border-box;
    overflow-wrap: normal;
    white-space: pre-line;
`

const Text = styled.p`
    margin: 10px 10px 10px 10px;
    box-sizing: border-box;
    word-wrap: break-word;
`