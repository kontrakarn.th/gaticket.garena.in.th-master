import React, {useState, useEffect, useRef} from 'react';
import styled from 'styled-components';
import GarenaLogo from '../static/images/logo-garena.png'
import Logout from '../static/images/logout.png'
import Profile from '../static/images/profile_icon.png'
import NavLeft from '../features/navLeft'
import SideBar from '../features/sideBar'
import axios from 'axios'
import {useCookies} from 'react-cookie';
import {Link, useHistory, useLocation} from 'react-router-dom'
import Spinner from '../features/spinner'
import useOutsideClick from "./useOutsideClick";

const NavBar = (props) => {
    const ref = useRef();
    const history = useHistory();

    const {isLoginPage,toggleOverlay} = props;
    const [displayMenu, setDisplayMenu] = useState(false);
    const [displaySideBar, setDisplaySideBar] = useState(false);
    const [loading, setLoading] = useState(true)

    const [data, setData] = useState([]);

    const [cookie, setCookie, removeCookie] = useCookies(['token', 'redirect']);
    
    const onClickToggle = () => {
        setDisplaySideBar(!displaySideBar)
        toggleOverlay()
    }

    const onClickOutside = () => {
        setDisplaySideBar(false)
        toggleOverlay()
    }

    useOutsideClick(ref, () => {
        if (displayMenu) setDisplayMenu(false)
    });

    useEffect(()=>{
        // if(typeof cookie.token === 'undefined' || cookie.token === ''){
        //     history.push('/')
        // }

        if(!isLoginPage){
            axios.get(process.env.REACT_APP_GET_USER_INFO,{
                headers : {
                    'Authorization' : cookie.token 
                }
            })
            .then((response) => {
                setLoading(false)
                setData(response.data.result);
            })
            .catch((error) => {
                console.log(error);
                removeCookie('token',{path:'/'})
                history.push('/')
            });
        }    
    }, [])

    const onClickLogOut = () => {
        axios.get(process.env.REACT_APP_LOGOUT).then((response) => {
            removeCookie('token',{path:'/'})
            removeCookie('redirect',{path:'/'})
            console.log(response)
            history.push('/')
        })
        .catch((error) => {
            console.log(error);
        });
    }

    return(
        <>
            <Nav>
                {isLoginPage && <Logo src={GarenaLogo} alt="logo"/>}
                {!isLoginPage && <NavLeft onClickToggle= {onClickToggle}/>}    
                <NavBarRight>
                    {loading? !isLoginPage && <Spinner/> : !isLoginPage && <DropDownShow onClick={()=>setDisplayMenu(!displayMenu)}>
                        <Image src={data.avatar} alt="avatar"></Image> 
                        <Name>{data.name}</Name>
                        {displayMenu && <DropDownMenu>
                            <DropDownItem ref={ref}>
                                <Icon isLogout={false}>
                                    <Person src={Profile} alt="profile"/>
                                    {data.email}
                                </Icon>
                            </DropDownItem>
                            <Divider/>
                                <DropDownItem onClick={()=>onClickLogOut()}>
                                    <Icon isLogout={true}>
                                        <LogoutImage src={Logout} alt="logout"/>
                                        Logout
                                    </Icon>
                                </DropDownItem>
                        </DropDownMenu>}
                    </DropDownShow>}
                </NavBarRight>
            </Nav>
            <SideBar onClickToggle={onClickToggle} data={data} displaySideBar={displaySideBar} onClickOutside={onClickOutside}/>
        </>
    )
}

export default NavBar;

const Nav = styled.div`
    display: flex; 
    position: fixed;
    width: 100%;
    z-index: 1000;
    margin-top: 0px;
    min-height: 50px;
    background-color: ${props => props.isLoginPage? '#3F51B5' : '#3F51B5' };
    border-color: #3F51B5;
`
const Logo = styled.img`
    margin-top: 0px;
    width: 135px;
    height: 50px;
    vertical-align: middle;
    padding: 10px 20px;
`
const NavBarRight = styled.div`
    position: absolute;
    right: 0px;
    align-items: center;
    height: 50px;
    display: flex;
`
const DropDownShow = styled.span`
    display: inline-block;
    position: relative;
    vertical-align: top;
    cursor: pointer;
`
const Image = styled.img`
    width: 30px;
    margin-right: .25rem!important;
    border-radius: 50%!important;
    vertical-align: middle;
    border-style: none;
`
const Name = styled.div`
    margin-top: 3px;
    display: inline-block!important;
    color: #fff;
    font-weight: 600;
    font-size: 14px;
    font-family: "Nunito", "Segoe UI", arial;
    margin-right: 10px;
    padding: 0 5px;
    :after {
        display: inline-block;
        margin-left: .255em;
        vertical-align: .255em;
        content: "";
        border-top: .3em solid;
        border-right: .3em solid transparent;
        border-bottom: 0;
        border-left: .3em solid transparent;
    }
    @media (max-width: 768px) {
        font-size: 0px;
        :after{
            font-size: 14px;
        }
    }
`
const DropDownMenu = styled.div`
    position: absolute;
    display: block !important;
    box-shadow: 0 10px 40px 0 rgba(51, 73, 94, 0.15);
    border: none;
    width: 230px;
    top: 100%;
    right: 20px;
    left: auto;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border-radius: .25rem;
    z-index: 10;
`
const DropDownItem = styled.div`
    display: block;
    width: 100%;
    clear: both;
    color: #212529;
    text-align: center;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
    cursor: pointer;
    font-size: 13px;
    padding: 15px 10px;
    font-weight: 500;
    line-height: 1.2;
`
const Icon = styled.div`
    margin-top: -5px;
    font-size: 13px;
    text-align: center;
    width: 15px;
    float: left;
    margin-right: 0px;
    display: inline-block;
    font-style: normal;
    font-variant: normal;
    text-rendering: auto;
    line-height: 1;
    color: ${props => props.isLogout? 'red' : 'black' };
    cursor: pointer;
}
`
const Divider = styled.div`
    border-top: 1px solid rgba(16,22,26,.15);
    height: 0;
    margin: .5rem 0;
    overflow: hidden;
`

const LogoutImage = styled.img`
    width: 100%;
    height: 14px;
    margin-top: -7px;
    margin-right: 5px;
`
const Person = styled.img`
    width: 100%;
    margin-top: -2px;
    margin-right: 5px;
`


