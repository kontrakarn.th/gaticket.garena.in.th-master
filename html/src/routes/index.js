import React from 'react'
import {BrowserRouter,Route} from 'react-router-dom'
import Login from '../pages/loginGoogle'
import GaFillRequest from '../pages/gaticket_fill_request'
import GaRequest from '../pages/ga_request'
import DomainFillRequest from '../pages/dticket_fill_request'
import DomainRequest from '../pages/d_request'
import ViewGaRequest from '../pages/view_ga_request'
import ViewDRequest from '../pages/view_d_request'
import Redirecting from '../features/redirecting'
import {CookiesProvider} from 'react-cookie'
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

function Routes() {
    
    const options = {
        timeout: 5000,
        position: positions.TOP_CENTER,
        containerStyle: {
            zIndex: 10000000
          }
    }

    return(
        <CookiesProvider>
            <Provider template={AlertTemplate} {...options}>
                <BrowserRouter>
                    <>
                        <Route path= '/' component={Redirecting}/>
                        <Route path='/' exact component={Login}/>
                        <Route path='/fill_ga_request' component={GaFillRequest}/>
                        <Route path='/fill_domain_request' component={DomainFillRequest}/>
                        <Route path='/ga_request' exact component={GaRequest}/>
                        <Route path='/domain_request' exact component={DomainRequest}/>
                        <Route path='/ga_request/:ticket_id' component={ViewGaRequest}/>
                        <Route path='/domain_request/:dticket_id' component={ViewDRequest}/>
                    </>
                </BrowserRouter>
            </Provider>
        </CookiesProvider>
    )
}

export default Routes;