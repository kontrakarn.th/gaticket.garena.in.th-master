import React, {useState, useEffect} from 'react';
import styled from 'styled-components'
import Nav from '../features/navBar'
import Overlay from '../features/overlay'
import AddComment from '../features/commentAdd'
import RequestForm from '../features/request_form'
import StatusBar from '../features/statusBar'
import Comment from '../features/comment'
import axios from 'axios'
import {useCookies} from 'react-cookie'
import { TitleComponent } from '../constant/TitleComponent.jsx'
import {useAlert} from 'react-alert';
import {useHistory, useLocation} from 'react-router-dom'
import Spinner from '../features/spinner';

const ViewGaRequest = ({match}) => {
    const location = useLocation();
    
    const [cookie, setCookie, removeCookie] = useCookies(['token', 'redirect']);
    setCookie('redirect',location.pathname,{path:'/',maxAge: 600})

    const dticket_id = match.params.dticket_id; //Ticket ID to display ////

    const history = useHistory();

    const [data, setData] = useState([]);
    const [assigneeData, setAssigneeData] = useState([]);
    const [comments,setComments] = useState([]);
    const [user, setUser] = useState([]);
    const [submit, setSubmit] = useState(false); //just to call useEffect 
    const [loading, setLoading] = useState(true);  
    const [disabled, setDisabled] = useState(false); 

    const alert = useAlert();

    const [displayOverlay, setDisplayOverlay] = useState(false);

    const toggleOverlay = () => {
        setDisplayOverlay(!displayOverlay)
    }
    useEffect(()=>{    
        axios.get(process.env.REACT_APP_READ_DOMAIN_TICKET + "?dticket_id=" + dticket_id, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setData(response.data.result);
            setAssigneeData(response.data.result.assignee);  
        })
        .catch((error) => {
            if(typeof cookie.token==='undefined'){
                history.push('/')
            } else {
                alert.error("You do not have permission to view this ticket!")
                history.push('/domain_request')
            }
            // removeCookie('token',{path:'/'})
            console.log(error);
        });

        axios.get(process.env.REACT_APP_GET_USER_INFO, {
            headers : {
                'Authorization' : cookie.token
            }
        })
        .then((response) => {
            setUser(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });
    }, [])

    useEffect(()=>{    
        axios.get(process.env.REACT_APP_GET_ALL_DOMAIN_COMMENTS + "?dticket_id=" + dticket_id, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setComments(response.data.result);
            setLoading(false)
            setDisabled(false)
        })
        .catch((error) => {
            console.log(error);
        });
    }, [submit])

    function handleCommentSubmit(comment){
        if(comment==='' || comment===null || comment.trim()===''){
            alert.error('Please write a comment!')
        } else {
            setDisabled(true);
            const body = {
                'name' : data.name,
                'dticket_id' : dticket_id,
                'body' : comment
            };
            axios.post(process.env.REACT_APP_CREATE_DOMAIN_COMMENT, body, {
                headers : {
                    'Authorization' : cookie.token 
                }
            }).then(function (response) {
                setSubmit(!submit) //just to call useEffect 
                alert.success('Comment Submitted Successfully!')
            })
            .catch(function (error) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    alert.error(error.response.data.errors.body);
                  } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    alert.error(error.request);
                  } else {
                    // Something happened in setting up the request that triggered an Error
                    alert.error(error.message);
                  }
                setDisabled(false)
            })
        }
    };

    return (
        <>
            <TitleComponent title="View Domain Request · GA Ticket" />
            <Nav toggleOverlay={toggleOverlay}/>
            {displayOverlay && <Overlay/>}
            {loading? <SpinnerWrapper><Spinner/></SpinnerWrapper>:
            <>
                <StatusBar data={data}/>
                <RequestForm is_Ga={false} data={data} user={user} assigneeData={assigneeData}/>
                <Comment comments={comments}/>
                <AddComment handleCommentSubmit={handleCommentSubmit}/>
            </>}
        </>
    )
}

export default ViewGaRequest;

const SpinnerWrapper = styled.div`
    text-align: center;
    position: relative;
    margin-top: 0;
    top: 110px;
`