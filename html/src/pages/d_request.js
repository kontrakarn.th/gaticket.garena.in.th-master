import React,{useState,useEffect} from 'react';
import styled from 'styled-components'
import Nav from '../features/navBar'
import Overlay from '../features/overlay'
import axios from 'axios';
import {useHistory, useLocation} from 'react-router-dom';
import {useCookies} from 'react-cookie';
import moment from 'moment';
import { TitleComponent } from '../constant/TitleComponent.jsx';
import Spinner from '../features/spinner';

function MyRequest() {
    const headerList = ['Ticket ID', 'Name','Requestor Email',
    'Domain Name','Status','Created At'];
    
    const location = useLocation();
    
    const [cookie, setCookie, removeCookie] = useCookies(['token', 'redirect']);
    setCookie('redirect',location.pathname,{path:'/',maxAge: 600})

    const [data, setData] = useState([]);
    const [user, setUser] = useState([])
    const [sort, setSort] = useState(
        {dticket_id:true,domain_name:true,status:true,created_at:true}
    );

    const [search, setSearch] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const [loading, setLoading] = useState(true);  
    const [displayOverlay, setDisplayOverlay] = useState(false);

    const toggleOverlay = () => {
        setDisplayOverlay(!displayOverlay)
    }

    useEffect(()=>{
        axios.get(process.env.REACT_APP_GET_ALL_DOMAIN_TICKETS, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setData(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(process.env.REACT_APP_GET_USER_INFO, {
            headers : {
                'Authorization' : cookie.token
            }
        })
        .then((response) => {
            setLoading(false);
            setUser(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });
    }, [])

    useEffect(() => {
        setFilteredData(
          data.filter(item =>
            item.dticket_id.toString().includes(search) ||
            item.status.toLowerCase().includes(search.toLowerCase()) || 
            item.domain_name.toLowerCase().includes(search.toLowerCase())||
            moment(item.created_at).local().format('LLL').includes(search)
        )
        );
      }, [search, data]);

      function onSort (sortKey) {
        var temp = [...data]
        if(sortKey ==='Ticket ID'){
            if(sort.dticket_id){
                setSort({...sort, dticket_id:false})
                temp = temp.sort(function(a, b) {
                return a.dticket_id - b.dticket_id
            });
            } else {
                setSort({...sort, dticket_id:true})
                temp = temp.sort(function(a, b) {
                    return b.dticket_id - a.dticket_id
                });
            }
        }
 
        if(sortKey ==='Domain Name'){
            if(sort.domain_name){
                setSort({...sort, domain_name:false})
                temp = temp.sort((a, b) => a.domain_name.localeCompare(b.domain_name))
            }else{
                setSort({...sort, domain_name:true})
                temp = temp.sort((a, b) => b.domain_name.localeCompare(a.domain_name))       
            }
        }

        if(sortKey ==='Status'){
            if(sort.status){
                setSort({...sort, status:false})
                temp = temp.sort((a, b) => a.status.localeCompare(b.status))
            }else{
                setSort({...sort, status:true})
                temp = temp.sort((a, b) => b.status.localeCompare(a.status))       
            }
        }
        
        if(sortKey ==='Created At'){
            if(sort.created_at){
                setSort({...sort, created_at:false})
                temp = temp.sort(function(a, b) {
                    var dateA = new Date(a.created_at), dateB = new Date(b.created_at);
                    return dateA - dateB;
                });
            }else{
                setSort({...sort, created_at:true})
                temp = temp.sort(function(a, b) {
                    var dateA = new Date(a.created_at), dateB = new Date(b.created_at);
                    return dateB - dateA;
                });       
            }
        }      
        setData(temp)
    }

    const history = useHistory();

    return(
        <>
        <TitleComponent title="My Domain Requests · GA Ticket" />
        <Nav toggleOverlay={toggleOverlay}/>
        {displayOverlay && <Overlay/>}
        <HeaderWrapper>
        <HeaderText>My Domain Requests</HeaderText>
            <Filter>
            <SearchLabel>Search : </SearchLabel>
            <input
                type="text"
                placeholder="search"
                onChange={e => setSearch(e.target.value)}
            /></Filter>
        </HeaderWrapper>
        {loading? <SpinnerWrapper><Spinner/></SpinnerWrapper>:<TableContainer>
            <Table>
                <Thr>
                    <TR>
                        {headerList.map((item,index)=>{   //When connect change to --> data.result.map
                            return (
                                <TH onClick={()=>onSort(item)}>
                                    <TableHeader>{item}</TableHeader>
                                </TH>
                            )
                        })}
                    </TR>
                </Thr>
                <Body>
                    {filteredData.map((item,index)=>{
                        return(
                            <TR onClick={()=>{
                                history.push(`/domain_request/${item.dticket_id}`)}}>
                                <TD>{item.dticket_id}</TD>
                                <TD>{user.name}</TD>
                                <TD>{user.email}</TD>
                                <TD>{item.domain_name}</TD>
                                <TD>{item.status}</TD>
                                <TD>{moment(item.created_at).local().format('LLL')}</TD>
                            </TR>
                        )
                    })}
                </Body>
            </Table>
        </TableContainer>}
        </>
    )
}

export default MyRequest;

const TableContainer = styled.div`
    width: 100%;
    overflow: auto;
    display: inline-block;
    box-sizing: border-box;
`

const Table = styled.table`
    border-spacing: 0;
    font-size: 14px;
    background-color: transparent;
    border-collapse: collapse;
    display: table;
    border-color: grey;
    width: 100%;
    :selection {
        background: rgba(125,188,255,.6);
    }
`

const TH = styled.th`
    resize: horizontal;
    border-color: grey;
    height: 40px;
    // background-color: #3498db;
    box-shadow: inset 1px 0 0 0 rgba(16,22,26,.15);
    :hover{
        background-color: rgba(191,204,214,0.8);
    }
`

const TableHeader = styled.div` 
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-left: 10px;
    text-align: center;
    ::after{
        margin-left: 10px;
        // margin-right: 10px;
        content:"\f0dc";
        font-family: FontAwesome;
        color: black;
    }
`

const TR = styled.tr`
    display: table-row;
    cursor: pointer;
    border-color: grey;
    box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    background: white;
    :nth-child(odd){
        background: rgba(191,204,214,.2);
    }
    :hover{
        background-color: rgba(191,204,214,0.8);
    }
`

const TD = styled.td`
    max-width: 250px;
    min-width: 150px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    width: 160px;
    color: #182026;
    padding: 11px;
    vertical-align: top;
    text-align: center;
    display: table-cell;
    background: rgba(191,204,214,.2);
    :not(:first-child) {
        box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    }
`
const Thr = styled.thead`
    display: table-header-group;
    vertical-align: middle;
    border-color: grey;
`
const Body = styled.tbody`
    display: table-row-group;
    vertical-align: middle;
    border-color: grey;
    box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    background: rgba(191,204,214,.2);
`

const HeaderText = styled.h2`
    margin-left: 10px;
    margin-top: 65px;
`

const Filter = styled.div`
    float: left;
    text-align: left;
    margin-top: 70px;
    margin-right: 5px;
`

const SearchLabel = styled.label`
    font-size: 14px;
    padding: 5px;
`

const HeaderWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;
`

const SpinnerWrapper = styled.div`
    text-align: center;
`