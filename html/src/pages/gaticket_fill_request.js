import React,{useState, useEffect} from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import styled from 'styled-components';
import axios from 'axios';
import MultiEmail from '../features/multi_email'
import Nav from '../features/navBar'
import Overlay from '../features/overlay'
import {useCookies} from 'react-cookie';
import {useAlert} from 'react-alert';
import Spinner from '../features/spinner';


function FillRequest(){
    const location = useLocation();
    
    const [cookie, setCookie, removeCookie] = useCookies(['token', 'redirect']);
    setCookie('redirect',location.pathname,{path:'/',maxAge: 600})

    const history = useHistory();

    const [websiteTypeList, setWebsiteTypeList] = useState([]);
    const [regionList, setRegionList] = useState([]);

    const [data, setData] = useState([]);
    const [ticketRequest, setTicketRequest] = useState(
        {name:'',requestor_email:'',emails_for_permission:'', website_url:'',website_type:'',website_region:'', remarks:''}
    );

    const [disabled, setDisabled] = useState(false)

    const [loading, setLoading] = useState(true)  

    const alert = useAlert();

    const [displayOverlay, setDisplayOverlay] = useState(false);

    const toggleOverlay = () => {
        setDisplayOverlay(!displayOverlay)
    }

    useEffect(()=>{
        axios.get(process.env.REACT_APP_GET_USER_INFO, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setData(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(process.env.REACT_APP_GET_WEBSITE_TYPE, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setWebsiteTypeList(response.data.result)
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(process.env.REACT_APP_GET_REGION, {
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then((response) => {
            setRegionList(response.data.result)
            setLoading(false);
        })
        .catch((error) => {
            console.log(error);
        });
    }, [])

    const getManyEmail = (data) => {
        ticketRequest.emails_for_permission = data;
        console.log(ticketRequest.emails_for_permission)
    }

    const handleChange = (e) => {
        if(e.target.name!=='remarks'){
            setTicketRequest({...ticketRequest,[e.target.name]:e.target.value.trim()})
        }else{
            setTicketRequest({...ticketRequest,[e.target.name]:e.target.value})
        }
    }

    const handleSubmit= (e) => {
        e.preventDefault()
        setDisabled(true)
        axios.post(process.env.REACT_APP_CREATE_GA_TICKET, ticketRequest,{
            headers : {
                'Authorization' : cookie.token 
            }
        })
        .then(function (response) {
            console.log(response.data)
            if(response.data.is_pass){
                alert.success("Ticket Submitted Successfully!")
                history.push('/ga_request')
            } else {
                setDisabled(false)
                alert.error("Website URL already exists in the database")
            }
          })
        .catch(function (error) {
            setDisabled(false)
            if (error.response) {
                // The request was made and the server responded with a status code
                alert.error(error.response.data.message);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                alert.error(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                alert.error(error.message);
              }
        })
    }

    return(
        <Container>
            <Nav toggleOverlay={toggleOverlay}/>
            {displayOverlay && <Overlay/>}
            {loading? <SpinnerWrapper><Spinner/></SpinnerWrapper>:<Card>
                <CardHeader>
                    <HeaderText>New GA Ticket</HeaderText>
                </CardHeader>
                <CardBody onSubmit={handleSubmit}>
                    <FormGroup>
                        <Form>
                            <Label for="name">Name</Label>
                            <Input 
                                id="name" 
                                type="name" 
                                name="name" 
                                tabIndex="1" 
                                autoFocus 
                                value = {data.name}
                                onChange={handleChange}
                                disabled
                                isDisabled = {true}
                            />
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="requestor_email">Email</Label>
                            <Input 
                                id="requestor_email" 
                                type="requestor_email" 
                                name="requestor_email" 
                                tabIndex="1" 
                                autoFocus 
                                value = {data.email}
                                onChange={handleChange}
                                disabled 
                                isDisabled = {true}
                            />
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <MultiEmail
                        value = {ticketRequest.emails_for_permission}
                        onChange={handleChange}
                        getManyEmail={getManyEmail}/>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="website_url">Website URL</Label>
                            <Input 
                                id="website_url" 
                                type="website_url" 
                                name="website_url" 
                                tabIndex="1" 
                                autoFocus 
                                required
                                value = {ticketRequest.website_url}
                                onChange={handleChange}
                                pattern="^(http:\/\/|https:\/\/|)(www\.|)([a-zA-Z][a-zA-Z0-9-_]{0,62})(\.[a-zA-Z]{2,})+([a-zA-Z0-9&\/#?=\-_%.+:]{0,})$"
                                placeholder = 'e.g. test.garena.in.th'
                                onFocus={(e) => e.target.placeholder = ''} 
                                onBlur={(e) => e.target.placeholder = 'eg. test.garena.in.th'}
                            />
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="website_type">Website Type</Label>
                            <Select 
                                id="website_type" 
                                name="website_type" 
                                tabIndex="1" 
                                autoFocus 
                                required
                                value = {ticketRequest.website_type}
                                onChange={handleChange}>
                                <Option disabled selected value="">---Select a website type---</Option>
                                {websiteTypeList.map((item)=>{
                                    return(
                                        <Option>{item.website_type}</Option>
                                    )
                                }
                                )}
                            </Select>
                        </Form>
                    </FormGroup>
                    <FormGroup>
                        <Form>
                            <Label for="website_region">Region</Label>
                            <Select 
                                id="website_region" 
                                name="website_region" 
                                tabIndex="1"  
                                autoFocus 
                                required
                                value = {ticketRequest.website_region}
                                onChange={handleChange} >
                                <Option disabled selected value="">---Select a website region---</Option>
                                {regionList.map((item)=>{
                                    return(
                                        <Option>{item.region}</Option>
                                    )
                                }
                                )}
                            </Select>
                        </Form>
                    </FormGroup>
                    {data.role!=='user' && <FormGroup>
                        <Form>
                            <Label for="remarks">Remarks</Label>
                            <TextArea 
                                id="remarks" 
                                type="remarks" 
                                name="remarks" 
                                tabIndex="1" 
                                autoFocus 
                                value = {ticketRequest.remarks}
                                onChange={handleChange}
                                placeholder="Add a new remark"
                                onFocus={(e) => e.target.placeholder = ''} 
                                onBlur={(e) => e.target.placeholder = "Add a new remark"}
                                maxLength='1000'
                            />
                        </Form>
                    </FormGroup>}
                    <FormGroup>
                        <Button type="submit" tabIndex="4" disabled={disabled}>Submit</Button>
                    </FormGroup>
                </CardBody>
            </Card>}
        </Container>
    )
}

export default FillRequest;

const Option = styled.option`
    font-weight: normal;
    display: block;
    white-space: pre;
    min-height: 1.2em;
    padding: 0px 2px 1px;
    color: ${p=>p.value===''? '#ccc' : 'rgb(73, 80, 87)'};
`
const Button = styled.button`
    padding: 10px;
    font-size: 12px;
    box-shadow: 0 2px 6px #acb5f6;
    background-color: #6777ef;
    border-color: #6777ef;
    font-weight: 600;
    display: block;
    width: 20%;
    margin-left: 40%;
    line-height: 24px;
    border-radius: .3rem;
    color: #fff;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    :hover {
        background-color: #6777FF;
        border-color: #6777FF;
    }
`

const Container = styled.div` 
    box-sizing: border-box;
    display: block;
    min-height: 100%;
    height: 100%;
    padding: 0;
`
const Card = styled.div`
    position: relative;
    margin-top: 0;
    top: 110px;
    margin-bottom: 60px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.03);
    background-color: #fff;
    border-radius: 3px;
    border: none;
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    max-width: 50%;
    margin-left: 25%;
    @media (max-width: 768px) {
        min-width: 90%;
        width: 90%;
        margin-left: 5%
    }
`
const CardHeader = styled.div`
    border-bottom: 1px solid #f9f9f9;
    line-height: 30px;
    align-self: center;
    width: 100%;
    min-height: 30px;
    padding: 15px 25px;
    display: flex;
    align-items: center;
    background-color: transparent;
    margin-bottom: 0;
    box-sizing: border-box;
`
const HeaderText = styled.h4`
    font-size: 24px;
    line-height: 28px;
    color: #6777ef;
    padding-right: 10px;
    margin-bottom: 0;
    font-weight: 700;
    margin-top: 0;
    font-family: "Nunito", "Segoe UI", arial;
`
const CardBody = styled.form`
    background-color: transparent;
    padding: 20px 25px;
`
const Form = styled.div`
    display: block;
    margin-top: 0em;
`
const FormGroup = styled.div`
    margin-bottom: 25px;
    display: block;
`
const Input = styled.input`
    font-size: 14px;
    padding: 10px 15px;
    height: 42px;
    display: block; 
    width: 100%;
    background-color: ${props=>props.isDisabled? '#eee' : '#fdfdff' };
    border-color: #e4e6fc;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-sizing: border-box;
    ::placeholder,
    ::-webkit-input-placeholder {
    color: #ccc;
    }
    :-ms-input-placeholder {
    color: #ccc;;
    }
`
const Select = styled.select`
    font-size: 14px;
    padding: 10px 10px;
    height: 42px;
    display: block; 
    width: 100%;
    background-color: #fdfdff;
    border-color: #e4e6fc;
    font-weight: 400;
    line-height: 1.5;
    color: ${p=>p.value===''? '#ccc':'#495057'};
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-sizing: border-box;
    text-rendering: auto;
    letter-spacing: normal;
    word-spacing: normal;
    text-indent: 0px;
    text-shadow: none;
    text-align: start;
    -webkit-appearance: menulist;
    align-items: center;
    white-space: pre;
    -webkit-rtl-ordering: logical;
    font: 400 13.3333px Arial;
`
const Label = styled.label`
    font-weight: 600;
    color: #34395e;
    font-size: 12px;
    letter-spacing: .5px;
    display: inline-block;
    margin-bottom: .5rem;
`
const SpinnerWrapper = styled.div`
    text-align: center;
    position: relative;
    margin-top: 0;
    top: 110px;
`

const TextArea = styled.textarea`
    resize: none;
    height: 120px;
    width: 100%;
    max-width: 100%;
    padding: 10px;
    line-height: 1.28581;
    outline: none;
    border: none;
    border-radius: 3px;
    box-shadow: 0 0 0 0 rgba(19,124,189,0), 0 0 0 0 rgba(19,124,189,0), inset 0 0 0 1px rgba(16,22,26,.15), inset 0 1px 1px rgba(16,22,26,.2);
    background: #fff;
    vertical-align: middle;
    color: #182026;
    font-size: 14px;
    font-weight: 400;
    transition: box-shadow .1s cubic-bezier(.4,1,.75,.9);
    box-sizing: border-box;
    ::placeholder,
    ::-webkit-input-placeholder {
    color: #ccc;
    }
    :-ms-input-placeholder {
    color: #ccc;;
    }
`