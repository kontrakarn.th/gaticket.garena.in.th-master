import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import Nav from '../features/navBar';
import GarenaImage from '../static/images/GARENA_LOGO.png';
import GoogleImage from '../static/images/logo-google.png';
import { TitleComponent } from '../constant/TitleComponent.jsx';

const Login = () =>{
    return (
        <>
            <TitleComponent title="Login · GA Ticket" />
            <Nav isLoginPage={true}/>
            <ContentContainer>
                <Content>
                    <Image src={GarenaImage} alt="logo"/>
                    <TextLogin>Login to Your Account</TextLogin>
                    <Divider>
                        <LoginText>sign in with</LoginText>
                    </Divider>
                    <a href='/api/login'><Button src={GoogleImage} alt="google"></Button></a>
                </Content>
            </ContentContainer>
        </>
    )
}

export default Login;

const ContentContainer = styled.body`
    font-family: Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;
    padding-top: 110px;
    position: static;
    width: 100%;
    display: table;
    text-align: center;
`
const Content = styled.div`
    width: 320px;
    position: relative;
    padding: 20px;
    background-color: #fff;
    margin: 0 auto 20px auto;
    border: 1px solid transparent;
    border-radius: 3px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`
const Image = styled.img`
    width: 128px;
    height: 128px;
`
const TextLogin = styled.h5`
    margin-top: 10px;
    margin-bottom: 20px !important;
    letter-spacing: -0.015em;
    font-size: 17px;
    font-weight: 400;
    line-height: 1.5384616;
`
const Divider = styled.div`
    margin-bottom: 20px;
    position: relative;
    text-align: center;
    z-index: 1;
    color: #999999;
`
const LoginText = styled.span`
    color: #999999;
    display: inline-block;
    padding-left: 16px;
    padding-right: 16px;
    text-align: center;
    background-color: #fff;
    font-size: 13px;
    :before {
        content: "";
        position: absolute;
        top: 50%;
        left: 0;
        height: 1px;
        background-color: #ddd;
        width: 100%;
        z-index: -1;
    }
`
const Button = styled.img`
   width: 15%;
   touch-action: manipulation;
   cursor: pointer;
`

