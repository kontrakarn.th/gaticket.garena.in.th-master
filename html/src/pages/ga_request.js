import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import Nav from '../features/navBar';
import Overlay from '../features/overlay'
import axios from 'axios';
import {useHistory, useLocation} from 'react-router-dom';
import {useCookies} from 'react-cookie';
import moment from 'moment';
import { TitleComponent } from '../constant/TitleComponent.jsx';
import Spinner from '../features/spinner';

function MyRequest(){
    const location = useLocation();

    const [cookie, setCookie] = useCookies(['token', 'redirect']);
    setCookie('redirect',location.pathname,{path:'/',maxAge: 600})

    const headerList = ['Ticket ID', 'Name','Requestor Email',
    'Website URL','Region','GA Code','GA Tag','Status','Created At'];
    
    const [data, setData] = useState([]);
    const [user, setUser] = useState([])
    const [sort, setSort] = useState(
        {ticket_id:true,requestor_email:true,website_url:true,website_type:true,website_region:true,status:true,created_at:true}
    );

    const [search, setSearch] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const [loading, setLoading] = useState(true)  
    const [displayOverlay, setDisplayOverlay] = useState(false);

    const toggleOverlay = () => {
        setDisplayOverlay(!displayOverlay)
    }
    
    useEffect(()=>{    
        axios.get(process.env.REACT_APP_GET_ALL_GA_TICKETS, {
            headers : {
                'Authorization' : cookie.token
            }
        })
        .then((response) => {
            setData(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(process.env.REACT_APP_GET_USER_INFO, {
            headers : {
                'Authorization' : cookie.token
            }
        })
        .then((response) => {
            setLoading(false);
            setUser(response.data.result);
        })
        .catch((error) => {
            console.log(error);
        });
    }, [])

    useEffect(() => {
        setFilteredData(
          data.filter(item =>
            item.ticket_id.toString().includes(search) ||
            item.status.toLowerCase().includes(search.toLowerCase()) || 
            item.website_region.toLowerCase().includes(search.toLowerCase()) ||
            item.website_url.includes(search) ||
            (item.gacode!==null && item.gacode.includes(search)) ||
            (item.gacode!==null && item.gacode.includes(search)) ||
            moment(item.created_at).local().format('LLL').includes(search)
        )
        );
      }, [search, data]);

    const history = useHistory();
 
    function onSort (sortKey) {
        var temp = [...data]
        if(sortKey ==='Ticket ID'){
            if(sort.ticket_id){
                setSort({...sort, ticket_id:false})
                temp = temp.sort(function(a, b) {
                return a.ticket_id - b.ticket_id
            });
            } else {
                setSort({...sort, ticket_id:true})
                temp = temp.sort(function(a, b) {
                    return b.ticket_id - a.ticket_id
                });
            }
        }
 
        if(sortKey ==='Website URL'){
            if(sort.website_url){
                setSort({...sort, website_url:false})
                temp = temp.sort((a, b) => a.website_url.localeCompare(b.website_url))
            }else{
                setSort({...sort, website_url:true})
                temp = temp.sort((a, b) => b.website_url.localeCompare(a.website_url))       
            }
        }
 
        if(sortKey ==='Region'){
            if(sort.website_region){
                setSort({...sort, website_region:false})
                temp = temp.sort((a, b) => a.website_region.localeCompare(b.website_region))
            }else{
                setSort({...sort, website_region:true})
                temp = temp.sort((a, b) => b.website_region.localeCompare(a.website_region))       
            }
        }
 
        if(sortKey ==='Status'){
            if(sort.status){
                setSort({...sort, status:false})
                temp = temp.sort((a, b) => a.status.localeCompare(b.status))
            }else{
                setSort({...sort, status:true})
                temp = temp.sort((a, b) => b.status.localeCompare(a.status))       
            }
        }
        
        if(sortKey ==='Created At'){
            if(sort.created_at){
                setSort({...sort, created_at:false})
                temp = temp.sort(function(a, b) {
                    var dateA = new Date(a.created_at), dateB = new Date(b.created_at);
                    return dateA - dateB;
                });
            }else{
                setSort({...sort, created_at:true})
                temp = temp.sort(function(a, b) {
                    var dateA = new Date(a.created_at), dateB = new Date(b.created_at);
                    return dateB - dateA;
                });       
            }
        }      
        setData(temp)
    }

    return(
        <>
        <TitleComponent title="My GA Requests · GA Ticket" />
        <Nav toggleOverlay={toggleOverlay}/>
        {displayOverlay && <Overlay/>}
        <HeaderWrapper>
        <HeaderText>My GA Requests</HeaderText>
            <Filter>
            <SearchLabel>Search : </SearchLabel>
            <input
                type="text"
                placeholder="search"
                onChange={e => setSearch(e.target.value)}
            /></Filter>
        </HeaderWrapper>
        {loading? <SpinnerWrapper><Spinner/></SpinnerWrapper>:<TableContainer>
            <Table>
                <Thr>
                    <TR>
                        {headerList.map((item,index)=>{
                            return (
                                <TH onClick={()=>onSort(item)}>
                                    <TableHeader notSort={item==='GA Code'||item==='GA Tag'}>{item}</TableHeader>
                                </TH>
                            )
                        })}
                    </TR>
                </Thr>
                    <Body>
                    {filteredData.map((item,index)=>{  //When connect change to --> data.result.map
                        return(
                            <TR onClick={()=>{
                                history.push(`/ga_request/${item.ticket_id}`)}}>
                                <TD>{item.ticket_id}</TD>
                                <TD>{user.name}</TD>
                                <TD>{user.email}</TD>
                                <TD>{item.website_url}</TD>                                    
                                <TD>{item.website_region}</TD>
                                {item.gacode===null? <TD>-</TD> : <TD>{item.gacode}</TD>}
                                {item.gatag===null? <TD>-</TD> : <TD>{item.gatag}</TD>}
                                <TD>{item.status}</TD>
                                <TD>{moment(item.created_at).local().format('LLL')}</TD>
                            </TR>
                        )
                    })}
                </Body>
            </Table>
        </TableContainer>}
        </>
    )
}

export default MyRequest;

const TableContainer = styled.div`
    width: 100%;
    overflow: auto;
    display: inline-block;
    box-sizing: border-box;
`

const Table = styled.table`
    border-spacing: 0;
    font-size: 14px;
    background-color: transparent;
    border-collapse: collapse;
    display: table;
    border-color: grey;
    width: 100%;
    :selection {
        background: rgba(125,188,255,.6);
    }
`

const TH = styled.th`
    resize: horizontal;
    border-color: grey;
    height: 40px;
    // background-color: #3498db;
    box-shadow: inset 1px 0 0 0 rgba(16,22,26,.15);
    :hover{
        background-color: rgba(191,204,214,0.8);
    }
`

const TableHeader = styled.div` 
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-left: 10px;
    text-align: center;
    ::after{
        font-size: ${p=>p.notSort? '0px':'inherit'};
        margin-left: 10px;
        content:"\f0dc";
        font-family: FontAwesome;
        color: black;
    }
`

const TR = styled.tr`
    display: table-row;
    cursor: pointer;
    border-color: grey;
    box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    background: white;
    :nth-child(odd){
        background: rgba(191,204,214,.2);
    }
    :hover{
        background-color: rgba(191,204,214,0.8);
    }
`

const TD = styled.td`
    max-width: 200px;
    min-width: 130px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    width: 130px;
    color: #182026;
    padding: 11px;
    vertical-align: top;
    text-align: center;
    display: table-cell;
    background: rgba(191,204,214,.2);
    :not(:first-child) {
        box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    }
`
const Thr = styled.thead`
    display: table-header-group;
    vertical-align: middle;
    border-color: grey;
`
const Body = styled.tbody`
    display: table-row-group;
    vertical-align: middle;
    border-color: grey;
    box-shadow: inset 1px 1px 0 0 rgba(16,22,26,.15);
    background: rgba(191,204,214,.2);
`
const HeaderText = styled.h2`
    margin-left: 10px;
    margin-top: 65px;
`

const Filter = styled.div`
    float: left;
    text-align: left;
    margin-top: 70px;
    margin-right: 5px;
`

const SearchLabel = styled.label`
    font-size: 14px;
    padding: 5px;
`

const HeaderWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;
`

const SpinnerWrapper = styled.div`
    text-align: center;
`





