#!/usr/bin/env bash

DIR=$(dirname "$0")
DOMAIN=$(basename "$DIR")

ln -sf /var/www/$DOMAIN/nginx.conf /etc/nginx/conf.d/site.conf
ln -sf $DIR/admin/composer.lock /tmp/composer.lock
ln -sf $DIR/admin/vendor /tmp/vendor

chmod -R 777 $DIR/admin/bootstrap/cache
chmod -R 777 $DIR/admin/storage
chmod -R 777 /var/log/nginx/$DOMAIN

# cp /var/www/gaticket.garena.in.th/html/.env /var/www/gaticket.garena.in.th/html/.env
cp /var/www/gaticket.garena.in.th/env/$DEPLOY/.env /var/www/gaticket.garena.in.th/admin/.env
echo "COPY /var/www/gaticket.garena.in.th/env/$DEPLOY/.env /var/www/gaticket.garena.in.th/api/.env"

nginx

echo "[$DEPLOY] $DOMAIN Nginx started"

/usr/sbin/php-fpm7.3 -F --fpm-config=/etc/php/7.3/fpm/pool.d/www.conf
